# Online Learning System

## Introduction

This project is aimed at developing an online learning system that supports organizations and experts in building and
selling online courses, and supports users in searching, registering, and accessing online courses for learning. The
system supports several types of users, including guests, customers, marketing members, sales members, experts, and
administrators.

SRS: <https://fptuniversity-my.sharepoint.com/:w:/g/personal/duonglhhe176069_fpt_edu_vn/EWhEAWibgpROsQ5eGvW729MBD-GF9QVKU37A-i6a8qtcOA?e=SfGXSz>

## Functional Requirements

The system includes 8 features with functions for each feature listed in the attached Excel file (OnlineLearn_Function
Details.xlsx). These features include:

1. **Common Feature:** Functions and screens used by all types of users, such as user login, user registration, reset
   password, user authorization, user profile, change password, enroll in free courses, learn and take quizzes.

2. **Premium Feature:** Functions and screens used by all types of users, such as user login, user registration, reset
   password, user authorization, user profile, change password, enroll in free courses and premium courses, learn and take quizzes.

3. **Course Content:** Functions that allow the role to add and edit subject contents and assign experts to subjects
   for preparing subject contents.

4. **Testing Content:** Functions that allow the role and expert to prepare testing-related contents such as questions
   bank and quizzes list.

5. **Admin Feature:** Screens used by the organization manager and system administrator to manage users and system
   settings.

## Other Requirements

- User-configured data such as user roles, system menu, post categories, subject categories, test types, question
  levels, lesson types, and subject dimension are defined and stored as system settings.
- The types and format of input data are suggested by the project team and must be agreed upon by the supervisor before
  implementing.
- Once agreed upon, input data must be implemented accordingly in software input validation, data showing, and in the
  system database.
