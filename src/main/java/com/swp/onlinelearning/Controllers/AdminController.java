package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Repository.UserRepository;
import com.swp.onlinelearning.Service.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * Controller class for handling admin-related actions.
 */
@Controller
@RequiredArgsConstructor
public class AdminController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserNotificationService notificationService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private CategoryService categoryService;

    /**
     * Displays the admin dashboard page.
     *
     * @param model the model object to add attributes to
     * @param auth  the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/admin/dashboard")
    public String viewAdminDashboard(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        List<Users> usersList = userService.getPremiumByWeek();
        int premiumByWeek = userService.countPremiumByWeek();
        int common = userService.getTotalCommon();
        int premium = userService.getTotalPremium();
        int category = categoryService.getCategory();
        int subject = subjectService.getSubject();
        Date currentTime = new Date();
        model.addAttribute("userList", usersList);
        model.addAttribute("category", category);
        model.addAttribute("subject", subject);
        model.addAttribute("common", common);
        model.addAttribute("premium", premium);
        model.addAttribute("premiumByWeek", premiumByWeek);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        return "dashboard/index";
    }

    /**
     * Displays the admin users list page.
     *
     * @param model the model object to add attributes to
     * @param auth  the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/admin/users")
    public String viewAdminUsers(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<Users> usersList = userService.getUsersList(0);
        int totalPage = userService.getTotalPage();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("userList", usersList);
        return "dashboard/user-list";
    }

    /**
     * Displays the add user form page.
     *
     * @param model the model object to add attributes to
     * @param auth  the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/admin/users/add")
    public String showAddUserForm(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("authenticatedUser", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", new Users());
        return "dashboard/add-user";
    }

    /**
     * Displays a paginated list of users on the admin users list page.
     *
     * @param model the model object to add attributes to
     * @param page  the page number to display
     * @param auth  the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/admin/users/page={page}")
    public String viewAdminUsers(Model model, @PathVariable(name = "page") int page, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        int totalPage = userService.getTotalPage();
        int position = (page - 1) * 15;
        List<Users> usersList = userService.getUsersList(position);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("userList", usersList);
        return "dashboard/user-list";
    }

    /**
     * Displays a list of users matching a search query on the admin users list page.
     *
     * @param model  the model object to add attributes to
     * @param search the search query string
     * @param auth   the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/admin/users/search={search}")
    public String viewAdminUsers(Model model, @PathVariable(name = "search") String search, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        String holder = "%" + search + "%";
        List<Users> usersList = userService.getUserListByName(holder, 0);
        int totalPage = userService.getTotalUserSearchPage(holder);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("userList", usersList);
        return "dashboard/user-list";
    }

    /**
     * Handles a search request on the admin users page.
     *
     * @param request the HttpServletRequest object containing request information
     * @return a redirect string to a URL containing search results
     */
    @PostMapping(value = "/admin/users/search")
    public String searching(HttpServletRequest request) {
        String rawSearch = request.getParameter("search");
        return "redirect:/admin/users/search=" + rawSearch;
    }

    /**
     * Displays a paginated list of users matching a search query on the admin users page.
     *
     * @param model  the model object to add attributes to
     * @param page   the page number to display
     * @param search the search query string
     * @param auth   the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/admin/users/search={search}/page={page}")
    public String viewAdminUsers(Model model, @PathVariable(name = "page") int page, @PathVariable(name = "search") String search, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        int position = (page - 1) * 15;
        String holder = "%" + search + "%";
        List<Users> usersList = userService.getUserListByName(holder, position);
        int totalPage = userService.getTotalUserSearchPage(holder);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("userList", usersList);
        return "dashboard/user-list";
    }

    /**
     * Handles adding a new user from an admin form submission.
     *
     * @param user          a Users object containing information about a new user from an admin form submission
     * @param bindingResult a BindingResult object containing validation errors for a new user from an admin form submission
     * @param model         a Model object containing information about an admin form submission for adding a new user
     * @param auth          an Authentication object representing information about an authenticated user
     * @return the name of the view to render
     */
    @PostMapping("/admin/users/add")
    public String addUser(@ModelAttribute("user") @Valid Users user, BindingResult bindingResult, Model model, Authentication auth) {
        if (bindingResult.hasErrors()) {
            Users authenticatedUser;
            try {
                authenticatedUser = userService.getAuthenticatedUser(auth);
            } catch (Exception NullPointerException) {
                return "redirect:/login";
            }
            List<UserNotifications> unreadNotifications = new ArrayList<>();
            List<UserNotifications> notifications = null;
            if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
                notifications = notificationService.getNotifications(authenticatedUser.getId());
                unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
            }
            Date currentTime = new Date();
            model.addAttribute("currentTime", currentTime);
            model.addAttribute("authenticatedUser", authenticatedUser);
            model.addAttribute("notification", notifications);
            model.addAttribute("unreadNotifications", unreadNotifications);
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "dashboard/add-user";
        }

        if (userService.emailExists(user.getEmail())) {
            model.addAttribute("error", "Email already exists!");
            return "dashboard/add-user";
        }
        userService.adminAddUser(user);
        return "redirect:/admin/users";
    }

    /**
     * Handles deleting a user by id from an admin request.
     *
     * @param id an int representing id of a user that is being deleted
     * @return a redirect string indicating whether deleting a user was successful or not
     */
    @RequestMapping("/admin/users/delete/{id}")
    public String adminDeleteUser(@PathVariable(value = "id") int id) {
        userService.delete(id);
        return "redirect:/admin/users";
    }

    /**
     * Handles displaying a form for editing a user by id from an admin request.
     *
     * @param model a Model object containing information about an admin form submission for editing a user
     * @param auth  an Authentication object representing information about an authenticated user
     * @param id    an int representing id of a user that is being edited
     * @return the name of the view to render
     */
    @GetMapping("/admin/users/edit/{id}")
    public String showUserUpdateForm(Model model, Authentication auth, @PathVariable(value = "id") int id) {
        Users editUser = userService.get(id);
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("editUser", editUser);
        return "dashboard/edit-user";
    }

    /**
     * Handles updating a user by id from an admin form submission.
     *
     * @param id          an int representing id of a user that is being edited
     * @param updatedUser a Users object containing information about a user from an admin form submission
     * @return a redirect string indicating whether updating a user was successful or not
     */
    @PostMapping("/admin/users/edit/{id}")
    public String updateUser(@PathVariable(value = "id") int id, @ModelAttribute("user") Users updatedUser) {
        Users user = userService.get(id);
        user.setFullName(updatedUser.getFullName());
        user.setGender(updatedUser.getGender());
        user.setMobile(updatedUser.getMobile());
        user.setRole(updatedUser.getRole());
        user.setEmail(updatedUser.getEmail());
        user.setStatus(updatedUser.getStatus());
        userRepository.save(user);
        return "redirect:/admin/users";
    }

    @GetMapping("/admin/form")
    public String showFormPage(Model model) {
        return "dashboard/form/form-element";
    }

    @GetMapping("/admin/icons")
    public String showIconsPage(Model model) {
        return "dashboard/icons/solid";
    }
}

