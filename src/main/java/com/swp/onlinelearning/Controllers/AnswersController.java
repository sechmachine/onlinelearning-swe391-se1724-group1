package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.AnswerOptions;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.*;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller class for handling actions related to answers.
 */
@Controller
public class AnswersController {

    @Autowired
    QuestionService questionService;

    @Autowired
    LessonsService lessonService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    AnswerService answerService;

    @Autowired
    UserService userService;

    @Autowired
    UserNotificationService notificationService;

    /**
     * Displays a list of answers for a given question on the test content creator page.
     *
     * @param model      the model object to add attributes to
     * @param questionId the id of the question to display answers for
     * @param auth       the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/questions/{questionId}/answers")
    public String viewQuestionsList(Model model, @PathVariable int questionId, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<AnswerOptions> listAnswers = answerService.listAnswerOptionsByQuestionId(questionId);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = notificationService.getNotifications(authenticatedUser.getId());
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listAnswers", listAnswers);
        model.addAttribute("question", questionService.get(questionId));
        return "answers-list";
    }

    /**
     * Displays an edit form for updating information about an existing answer by id.
     *
     * @param model      the model object to add attributes to
     * @param auth       the authentication object representing the currently authenticated user
     * @param questionId the id of the question associated with the answer being edited
     * @param answerId   the id of the answer being edited
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/questions/{questionId}/answers/edit/{answerId}")
    public String showEditAnswerForm(Model model, Authentication auth, @PathVariable int questionId, @PathVariable int answerId) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        AnswerOptions answer = answerService.get(answerId);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = notificationService.getNotifications(authenticatedUser.getId());
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("answer", answer);
        model.addAttribute("question", questionService.get(questionId));
        return "edit-answer";
    }

    /**
     * Handles updating information about an existing answer by id from a test content creator form submission.
     *
     * @param questionId    the id of the question associated with the answer being edited
     * @param auth          the authentication object representing the currently authenticated user
     * @param answerId      the id of the answer being edited
     * @param answer        an AnswerOptions object containing updated information about an existing answer from a test content creator form submission
     * @param bindingResult a BindingResult object containing validation errors for an existing answer from a test content creator form submission
     * @param model         a Model object containing information about a test content creator form submission for updating information about an existing answer
     * @return a redirect string indicating whether updating information about an existing answer was successful or not
     */
    @PostMapping("/test-content-creator/questions/{questionId}/answers/edit/{answerId}")
    public String editAnswer(@PathVariable int questionId, Authentication auth, @PathVariable int answerId, @Valid AnswerOptions answer, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            Users authenticatedUser;
            try {
                authenticatedUser = userService.getAuthenticatedUser(auth);
            } catch (Exception NullPointerException) {
                return "redirect:/login";
            }
            List<UserNotifications> unreadNotifications = new ArrayList<>();
            List<UserNotifications> notifications = notificationService.getNotifications(authenticatedUser.getId());
            if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
                notifications = notificationService.getNotifications(authenticatedUser.getId());
                unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
            }
            Date currentTime = new Date();
            model.addAttribute("currentTime", currentTime);
            model.addAttribute("user", authenticatedUser);
            model.addAttribute("notification", notifications);
            model.addAttribute("unreadNotifications", unreadNotifications);
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "edit-answer";
        }
        answer.setId(answerId);
        answer.setQuestions(questionService.get(questionId));
        answerService.save(answer);
        return "redirect:/test-content-creator/questions/{questionId}/answers";
    }

    /**
     * Displays an add form for adding a new answer to a given question on the test content creator page.
     *
     * @param model      the model object to add attributes to
     * @param auth       the authentication object representing the currently authenticated user
     * @param questionId the id of the question to add a new answer for
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/questions/{questionId}/answers/add")
    public String showAddAnswerForm(Model model, Authentication auth, @PathVariable int questionId) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        AnswerOptions answer = new AnswerOptions();
        answer.setQuestions(questionService.get(questionId));
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = notificationService.getNotifications(authenticatedUser.getId());
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("answer", answer);
        model.addAttribute("question", questionService.get(questionId));
        return "add-answer";
    }

    /**
     * Handles adding a new answer to a given question from a test content creator form submission.
     *
     * @param questionId    the id of the question to add a new answer for
     * @param auth          the authentication object representing information about an authenticated user
     * @param answer        an AnswerOptions object containing information about a new answer from a test content creator form submission
     * @param bindingResult a BindingResult object containing validation errors for a new answer from a test content creator form submission
     * @param model         a Model object containing information about a test content creator form submission for adding a new answer
     * @return a redirect string indicating whether adding a new answer was successful or not
     */
    @PostMapping("/test-content-creator/questions/{questionId}/answers/add")
    public String addAnswer(@PathVariable int questionId, Authentication auth, @Valid AnswerOptions answer, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            Users authenticatedUser;
            try {
                authenticatedUser = userService.getAuthenticatedUser(auth);
            } catch (Exception NullPointerException) {
                return "redirect:/login";
            }
            List<UserNotifications> unreadNotifications = new ArrayList<>();
            List<UserNotifications> notifications = notificationService.getNotifications(authenticatedUser.getId());
            if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
                notifications = notificationService.getNotifications(authenticatedUser.getId());
                unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
            }
            Date currentTime = new Date();
            model.addAttribute("currentTime", currentTime);
            model.addAttribute("user", authenticatedUser);
            model.addAttribute("notification", notifications);
            model.addAttribute("unreadNotifications", unreadNotifications);
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "add-answer";
        }
        answer.setQuestions(questionService.get(questionId));
        answerService.save(answer);
        return "redirect:/test-content-creator/questions/{questionId}/answers";
    }

    /**
     * Handles deleting an existing answer by id from a test content creator request.
     *
     * @param questionId an int representing id of a question associated with an existing answer that is being deleted
     * @param answerId   an int representing id of an existing answer that is being deleted
     * @return a redirect string indicating whether deleting an existing answer was successful or not
     */
    @RequestMapping("/test-content-creator/questions/{questionId}/answers/delete/{answerId}")
    public String deleteAnswer(@PathVariable int questionId, @PathVariable int answerId) {
        answerService.delete(answerId);
        return "redirect:/test-content-creator/questions/" + questionId + "/answers";
    }
}
