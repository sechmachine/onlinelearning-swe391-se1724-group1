package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.*;
import com.swp.onlinelearning.Security.UserRole;
import com.swp.onlinelearning.Service.*;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Time;
import java.util.*;

/**
 * Controller class for handling actions related to course details.
 */
@Controller
@RequiredArgsConstructor
public class CourseDetailController {

    @Autowired
    final SubjectService subjectService;

    @Autowired
    final HtmlService htmlService;


    @Autowired
    final UserService userService;

    @Autowired
    final LessonsService lessonService;


    @Autowired
    final QuizQuestionsService quizQuestionsService;


    @Autowired
    final QuestionService questionService;


    @Autowired
    final AnswerService answerService;


    @Autowired
    final QuizHistoryService quizHistoryService;


    @Autowired
    final QuizService quizService;


    @Autowired
    final AnsweredQuestionService answeredQuestionService;

    @Autowired
    final UserNotificationService notificationService;

    @Autowired
    final CourseRegistrationService registrationService;

    public Map<Integer, Boolean> getProcessMap(Subjects subjects, Users users) {
        Map<Integer, Boolean> process = new HashMap<>();
        for (Lessons lessons : subjects.getLessonsList()) {
            boolean status = false;
            if (quizHistoryService.getQuizHistoryByUser(users.getId(), lessons.getQuizzes().getId()) != null) {
                double highestScore = 0;
                for (QuizHistory quizHistory : quizHistoryService.getQuizHistoryByUser(users.getId(), lessons.getQuizzes().getId())) {
                    if (quizHistory.getScore() > highestScore) {
                        highestScore = quizHistory.getScore();
                    }
                }
                if (highestScore > lessons.getQuizzes().getPassCondition()) {
                    status = true;
                }
            }
            process.put(lessons.getId(), status);
        }

        return process;
    }

    public Map<Integer, Boolean> getProcessMap2(Subjects subjects, Users users) {
        Map<Integer, Boolean> process = new HashMap<>();
        for (Lessons lessons : subjects.getLessonsList()) {
            boolean status = false;
            if (quizHistoryService.getQuizHistoryByUser(users.getId(), lessons.getQuizzes().getId()) != null) {
                double highestScore = 0;
                for (QuizHistory quizHistory : quizHistoryService.getQuizHistoryByUser(users.getId(), lessons.getQuizzes().getId())) {
                    if (quizHistory.getScore() > highestScore) {
                        highestScore = quizHistory.getScore();
                    }
                }
                if (highestScore > lessons.getQuizzes().getPassCondition()) {
                    status = true;
                }
            }
            process.put(lessons.getId(), status);
        }
        return process;
    }

    /**
     * Handles a GET request for the course details page for a specific subject.
     *
     * @param id   the ID of the subject
     * @param auth the authentication object representing the currently authenticated user
     * @return a ModelAndView object for the course details page
     */
    @RequestMapping("/coursedetails/id={id}")
    public ModelAndView getCourseDetails(@PathVariable(name = "id") int id, Authentication auth) {
        ModelAndView mav = new ModelAndView("coursedetail");
        ModelAndView mav2 = new ModelAndView("redirect:/login");
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return mav2;
        }
        Subjects subject = subjectService.get(id);
        Lessons firstLesson = subject.getLessonsList().get(0);
        boolean enroll = registrationService.checkRegistration(authenticatedUser.getId(), id);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Map<Integer, Boolean> process = getProcessMap(subject, authenticatedUser);
        Date currentTime = new Date();
        String htmlContent = htmlService.markdownToHtml(firstLesson.getMarkdownContent());
        mav.addObject("enroll", enroll);
        mav.addObject("map", process);
        mav.addObject("notification", notifications);
        mav.addObject("currentTime", currentTime);
        mav.addObject("htmlContent", htmlContent);
        mav.addObject("unreadNotifications", unreadNotifications);
        mav.addObject("user", authenticatedUser);
        mav.addObject("subject", subject);
        mav.addObject("lesson", firstLesson);
        return mav;
    }

    /**
     * Handles enrolling a user in a specific subject.
     *
     * @param subjectId the ID of the subject to enroll in
     * @param auth      the authentication object representing the currently authenticated user
     * @return a ModelAndView object for the course details page with a message indicating whether enrollment was successful or not
     */
    @RequestMapping(value = "/coursedetails/id={subjectId}/enroll")
    public ModelAndView enroll(@PathVariable("subjectId") int subjectId, Authentication auth) {
        ModelAndView mav = new ModelAndView("coursedetail");
        ModelAndView mav2 = new ModelAndView("redirect:/login");
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return mav2;
        }
        Subjects subject = subjectService.get(subjectId);
        String message = "";
        if (subject.getTier().equals(Subjects.Tier.Premium) && !(authenticatedUser.getRole().equals(UserRole.Premium) || authenticatedUser.getRole().equals(UserRole.Admin) || authenticatedUser.getRole().equals(UserRole.CourseContentCreator) || authenticatedUser.getRole().equals(UserRole.TestContentCreator))) {
            message = "Sorry, only Premium users can enroll in Premium subjects.";
        } else {
            Registrations registration = new Registrations();
            registration.setUser(authenticatedUser);
            registration.setSubject(subject);
            registration.setStatus(Registrations.Status.valueOf("Enrolled"));
            registrationService.save(registration);
            message = "You have successfully enrolled in the subject!";
        }
        mav.addObject("message", message);

        Lessons firstLesson = subject.getLessonsList().get(0);
        boolean enroll = registrationService.checkRegistration(authenticatedUser.getId(), subjectId);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        String htmlContent = htmlService.markdownToHtml(firstLesson.getMarkdownContent());
        mav.addObject("notification", notifications);
        mav.addObject("enroll", enroll);
        mav.addObject("currentTime", currentTime);
        mav.addObject("htmlContent", htmlContent);
        mav.addObject("unreadNotifications", unreadNotifications);
        mav.addObject("user", authenticatedUser);
        mav.addObject("subject", subject);
        mav.addObject("lesson", firstLesson);

        return mav;
    }

    /**
     * Handles dropping out a user from a specific subject.
     *
     * @param subjectId the ID of the subject to drop out from
     * @param auth      the authentication object representing the currently authenticated user
     * @return a ModelAndView object for the course details page with a message indicating whether dropping out was successful or not
     */
    @RequestMapping(value = "/coursedetails/id={subjectId}/drop-out")
    public ModelAndView dropOut(@PathVariable("subjectId") int subjectId, Authentication auth) {
        ModelAndView mav = new ModelAndView("coursedetail");
        ModelAndView mav2 = new ModelAndView("redirect:/login");
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return mav2;
        }
        Subjects subject = subjectService.get(subjectId);
        String message = "";
        if (registrationService.checkRegistration(authenticatedUser.getId(), subjectId)) {
            Registrations registration = registrationService.getRegistration(authenticatedUser.getId(), subjectId);
            registrationService.delete(registration.getId());
            message = "You have successfully dropped out of the subject!";
        } else {
            message = "You are not enrolled in this subject!";
        }
        mav.addObject("message", message);

        Lessons firstLesson = subject.getLessonsList().get(0);
        boolean enroll = registrationService.checkRegistration(authenticatedUser.getId(), subjectId);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        String htmlContent = htmlService.markdownToHtml(firstLesson.getMarkdownContent());
        mav.addObject("notification", notifications);
        mav.addObject("enroll", enroll);
        mav.addObject("currentTime", currentTime);
        mav.addObject("htmlContent", htmlContent);
        mav.addObject("unreadNotifications", unreadNotifications);
        mav.addObject("user", authenticatedUser);
        mav.addObject("subject", subject);
        mav.addObject("lesson", firstLesson);

        return mav;
    }

    /**
     * Handles a GET request for the course details page for a specific subject and lesson.
     *
     * @param id        the ID of the subject
     * @param lessonNum the number of the lesson within the subject's lesson list (1-indexed)
     * @param auth      the authentication object representing the currently authenticated user
     * @return a ModelAndView object for the course details page
     */
    @RequestMapping("/coursedetails/id={id}/lessonnum={lessonNum}")
    public ModelAndView getSubjectsDetails(@PathVariable(name = "id") int id, @PathVariable(name = "lessonNum") int lessonNum, Authentication auth) {
        ModelAndView mav = new ModelAndView("coursedetail");
        ModelAndView mav2 = new ModelAndView("redirect:/login");
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return mav2;
        }
        Subjects subject = subjectService.get(id);
        Lessons lesson = subject.getLessonsList().get(lessonNum - 1);
        boolean enroll = registrationService.checkRegistration(authenticatedUser.getId(), id);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        String htmlContent = htmlService.markdownToHtml(lesson.getMarkdownContent());
        Map<Integer, Boolean> process = getProcessMap2(subject, authenticatedUser);
        mav.addObject("map", process);
        mav.addObject("notification", notifications);
        mav.addObject("htmlContent", htmlContent);
        mav.addObject("enroll", enroll);
        mav.addObject("currentTime", currentTime);
        mav.addObject("unreadNotifications", unreadNotifications);
        mav.addObject("user", authenticatedUser);
        mav.addObject("subject", subject);
        mav.addObject("lesson", lesson);
        return mav;
    }

    /**
     * Handles a GET request for displaying quiz history for a specific quiz.
     *
     * @param model  model object to add attributes to be used in view rendering
     * @param quizid ID of quiz to display history for
     * @param auth   authentication object representing currently authenticated user
     * @return name of view to render
     */
    @RequestMapping("/quizhistory/quizid={quizid}")
    public String getHistory(Model model, @PathVariable(name = "quizid") int quizid, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Quizzes quizzes = quizService.get(quizid);
        List<QuizHistory> quizHistoryList = quizHistoryService.getQuizHistoryByUser(authenticatedUser.getId(), quizid);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("history", quizHistoryList);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("quiz", quizzes);
        return "quizHistory";
    }


    /**
     * Handles a GET request for displaying testing page for a specific quiz.
     *
     * @param model  model object to add attributes to be used in view rendering
     * @param id  ID of quiz to display testing page for
     * @param auth  authentication object representing currently authenticated user
     * @return name of view to render
     */
    @RequestMapping("/testing/id={id}")
    public String getTesting(Model model, @PathVariable(name = "id") int id, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<QuizQuestions> quizQuestionsList = quizQuestionsService.listAllByQuizId(id);
        if (quizQuestionsList != null) {
            Collections.shuffle(quizQuestionsList);
        }
        model.addAttribute("listQuestions", quizQuestionsList);
        model.addAttribute("user", authenticatedUser);
        return "testing";
    }

    /**
     * Handles a GET request for displaying simulator page for a specific quiz.
     *
     * @param model model object to add attributes to be used in view rendering
     * @param id    ID of quiz to display simulator page for
     * @param auth  authentication object representing currently authenticated user
     * @return name of view to render
     */
    @RequestMapping("/simulator/id={id}")
    public String getSimulator(Model model, @PathVariable(name = "id") int id, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<QuizQuestions> quizQuestionsList = quizQuestionsService.listAllByQuizId(id);
        model.addAttribute("listQuestions", quizQuestionsList);
        model.addAttribute("user", authenticatedUser);
        return "simulator";
    }

    /**
     * Handles a GET request for displaying review testing page for a specific quiz history.
     *
     * @param model model object to add attributes to be used in view rendering
     * @param id    ID of quiz history to display review testing page for
     * @param auth  authentication object representing currently authenticated user
     * @return name of view to render
     */
    @RequestMapping("/reviewtesting/id={id}")
    public String getReviewTesting(Model model, @PathVariable(name = "id") int id, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<AnsweredQuestions> answeredQuestionsList = answeredQuestionService.getAnsweredQuestions(id);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("history", answeredQuestionsList.get(0).getQuizHistory());
        model.addAttribute("answered", answeredQuestionsList);
        return "reviewtesting";
    }

    /**
     * Saves score for a specific quiz history.
     *
     * @param id ID of quiz history to save score for
     */
    public void saveScore(int id) {
        List<AnsweredQuestions> answeredQuestions = answeredQuestionService.getAnsweredQuestions(id);
        double grade = grading(answeredQuestions);
        QuizHistory quizHistory = quizHistoryService.getQuizHistory(id);
        quizHistory.setScore(grade);
        quizHistoryService.save(quizHistory);
    }

    /**
     * Calculates grading for a list of answered questions.
     *
     * @param answeredQuestions list of answered questions to calculate grading for
     * @return calculated grading as a double
     */
    public double grading(List<AnsweredQuestions> answeredQuestions) {
        double correctAnswer = 0;
        for (AnsweredQuestions answeredQuestion : answeredQuestions) {
            List<AnswerOptions> answerOptions = answeredQuestion.getQuestions().getAnswerOptionsList();
            for (AnswerOptions answerOption : answerOptions) {
                if (answeredQuestion.getSelectedAnswerId().equals(answerOption.getId()) && (answerOption.getIsKey() == 1)) {
                    correctAnswer = correctAnswer + 1;

                }
            }
        }
        return (correctAnswer / answeredQuestions.size()) * 10;
    }

    /**
     * Handles a POST request for saving test results from a testing page.
     *
     * @param request HttpServletRequest object containing request information
     * @param auth    authentication object representing currently authenticated user
     * @return redirect string to review testing page for saved test results
     */
    @PostMapping(value = "/saveTest")
    public String saveTest(HttpServletRequest request, Authentication auth) {
        String rawQuizId = request.getParameter("quizId");
        QuizHistory newQuizHistory = new QuizHistory();
        String timeTaken = request.getParameter("timeTaken");
        Users user = userService.getAuthenticatedUser(auth);
        Date date = new Date();
        Time time = Time.valueOf(timeTaken);
        QuizQuestions quizQuestions = quizQuestionsService.get(Integer.parseInt(rawQuizId));
        newQuizHistory.setUsers(user);
        newQuizHistory.setQuizzes(quizQuestions.getQuizzes());
        newQuizHistory.setDateTaken(date);
        newQuizHistory.setScore(1.0);
        newQuizHistory.setTimeTaken(time);
        quizHistoryService.save(newQuizHistory);
        int i = 1;
        while (request.getParameter("question" + i) != null) {
            String rawAnswer = request.getParameter("answer" + i);
            String rawQuestionId = request.getParameter("question" + i);
            if (rawAnswer == null) {
                rawAnswer = "1";
            }
            AnsweredQuestions newAnsweredQuestions = new AnsweredQuestions();
            Questions questions = questionService.get(Integer.parseInt(rawQuestionId));
            newAnsweredQuestions.setQuestions(questions);
            newAnsweredQuestions.setQuizHistory(newQuizHistory);
            newAnsweredQuestions.setSelectedAnswerId(Integer.parseInt(rawAnswer));
            answeredQuestionService.save(newAnsweredQuestions);
            i++;
        }
        saveScore(newQuizHistory.getId());
        return "redirect:/reviewtesting/id=" + newQuizHistory.getId();
    }

}