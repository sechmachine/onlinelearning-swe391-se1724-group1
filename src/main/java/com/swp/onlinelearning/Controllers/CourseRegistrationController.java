package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.*;
import com.swp.onlinelearning.Service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

/**
 * Controller class for handling actions related to course registration.
 */
@Controller
@RequiredArgsConstructor
public class CourseRegistrationController {
    @Autowired
    final UserService userService;

    @Autowired
    CourseRegistrationService service;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    UserNotificationService notificationService;

    @Autowired
    LessonsService lessonsService;

    @Autowired
    QuizHistoryService quizHistoryService;

    public Map<Integer, Integer> getProcess(List<Registrations> registrationsList, Users users) {
        Map<Integer, Integer> process = new HashMap<>();
        for (Registrations registrations : registrationsList) {
            int count = 0;
            Subjects subjects = registrations.getSubject();
            for (Lessons lessons : subjects.getLessonsList()) {
                double highestScore = 0;
                if (quizHistoryService.getQuizHistoryByUser(users.getId(), lessons.getQuizzes().getId()) != null) {
                    for (QuizHistory quizHistory : quizHistoryService.getQuizHistoryByUser(users.getId(), lessons.getQuizzes().getId())) {
                        if (quizHistory.getScore() > highestScore) {
                            highestScore = quizHistory.getScore();
                        }
                    }
                }
                if (highestScore > lessons.getQuizzes().getPassCondition()) {
                    count++;
                }
            }
            process.put(registrations.getSubject().getId(), count);
        }
        return process;
    }

    /**
     * Handles a GET request for displaying the user's registered courses.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @RequestMapping("/mycourse")
    public String viewHomePage(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Map<Integer, Integer> lessonCounts = lessonsService.getLessonCounts();
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        List<Registrations> listRegistrations = service.listAllByUserId(authenticatedUser.getId());
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            if (!notifications.isEmpty()) {
                unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
            }
        }
        Map<Integer, Integer> process = null;
        if (!listRegistrations.isEmpty()) {
            process = getProcess(listRegistrations, authenticatedUser);
        }
        Date currentTime = new Date();
        model.addAttribute("process", process);
        model.addAttribute("lessonCounts", lessonCounts);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("listRegistrations", listRegistrations);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mycourse";
    }
}
