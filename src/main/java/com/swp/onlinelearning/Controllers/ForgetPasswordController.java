package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Repository.UserRepository;
import com.swp.onlinelearning.Service.UserService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller class for handling actions related to forgetting and resetting passwords.
 */
@Controller
@RequiredArgsConstructor
public class ForgetPasswordController {
    @Autowired
    private UserService userService;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private UserRepository userRepo;

    /**
     * Generates a site URL from a given HttpServletRequest object.
     *
     * @param request the HttpServletRequest object to generate a site URL from
     * @return the generated site URL as a String
     */
    public static String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }

    /**
     * Generates a random 32-character String.
     *
     * @return the generated random String
     */
    public static String randomString32() {
        String uuid = java.util.UUID.randomUUID().toString();
        return uuid.replace("-", "").substring(0, 32);
    }

    /**
     * Displays the forgot password form page.
     *
     * @return the name of the view to render
     */
    @GetMapping("/forgot")
    public String showForgotPasswordForm() {
        return "forgor";
    }

    /**
     * Processes a forgot password form submission.
     *
     * @param request the HttpServletRequest object containing request information
     * @param model   the model object to add attributes to be used in view rendering
     * @return the name of the view to render
     */
    @PostMapping("/forgot")
    public String processForgotPasswordForm(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String token = randomString32();

        try {
            userService.updatePasswordResetToken(token, email);
            String resetPasswordLink = getSiteURL(request) + "/reset-password?token=" + token;
            sendEmail(email, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");

        } catch (UsernameNotFoundException ex) {
            model.addAttribute("error", ex.getMessage());
        } catch (MessagingException e) {
            model.addAttribute("error", "Error while sending email");
        }
        return "forgor";
    }

    /**
     * Sends an email containing a reset password link to a given recipient email address.
     *
     * @param recipientEmail the recipient email address to send the email to
     * @param link           the reset password link to include in the email
     * @throws MessagingException if an error occurs while sending the email
     */
    public void sendEmail(String recipientEmail, String link) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);

        helper.setFrom("tenno696913112003@gmail.com");
        helper.setTo(recipientEmail);

        String subject = "Here's the link to reset your password";

        String content = "<h1>Hello,</h1>" + "<p>You have requested to reset your password.</p>" + "<p>Click the link below to change your password:</p>" + "<p><a href=\"" + link + "\">Change my password</a></p>" + "<br>" + "<p>Ignore this email if you do remember your password, " + "or you have not made the request.</p>";

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(mimeMessage);
    }

    /**
     * Displays the reset password form page for a given password reset token.
     *
     * @param token the password reset token to use for resetting a password
     * @param model the model object to add attributes to be used in view rendering
     * @return the name of the view to render
     */
    @GetMapping("/reset-password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
        Users user = userService.getByPasswordResetToken(token);
        if (user == null) {
            model.addAttribute("error", "Invalid Token");
            return "reset";
        }
        model.addAttribute("token", token);
        return "reset";
    }

    /**
     * Processes a reset password form submission.
     *
     * @param token              password reset token used for resetting a password
     * @param newPassword        new password entered by user
     * @param confirmNewPassword confirmation of new password entered by user
     * @param model              model object to add attributes to be used in view rendering
     * @return name of view to render
     */
    @PostMapping("/reset-password")
    public String processResetPassword(@RequestParam("token") String token,
                                       @RequestParam("newPassword") String newPassword,
                                       @RequestParam("confirmNewPassword") String confirmNewPassword,
                                       Model model) {
        Users user = userRepo.findByPasswordResetToken(token);

        if (user == null) {
            model.addAttribute("error", "Invalid token");
            return "reset";
        }

        if (!newPassword.equals(confirmNewPassword)) {
            model.addAttribute("error", "New password and confirm new password do not match");
            return "reset";
        }
        userService.updatePassword(newPassword, user);
        model.addAttribute("message", "Password reset successfully");
        return "reset";
    }
}


