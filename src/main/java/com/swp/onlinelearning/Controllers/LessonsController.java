package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.*;
import com.swp.onlinelearning.Service.*;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller class for handling actions related to lessons.
 */
@Controller
public class LessonsController {

    @Autowired
    LessonsService lessonService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    QuizService quizService;

    @Autowired
    UserService userService;

    @Autowired
    UserNotificationService notificationService;

    /**
     * Displays a list of lessons for a given subject on the course content creator page.
     *
     * @param subjectId the id of the subject to display lessons for
     * @param model     the model object to add attributes to be used in view rendering
     * @param auth      the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/course-content-creator/subjects/{subjectId}/lessons")
    public String viewLessonsListBySubject(@PathVariable("subjectId") int subjectId, Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Subjects subject = subjectService.get(subjectId);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("subject", subject);
        List<Lessons> listLessons = lessonService.listLessonsBySubjectId(subjectId);
        model.addAttribute("listLessons", listLessons);
        return "lessons-list";
    }

    /**
     * Displays an add form for adding a new lesson to a given subject on the course content creator page.
     *
     * @param subjectId the id of the subject to add a new lesson for
     * @param model     the model object to add attributes to be used in view rendering
     * @param auth      the authentication object representing the currently authenticated user
     * @return the name of the view to render
     */
    @GetMapping("/course-content-creator/subjects/{subjectId}/lessons/add")
    public String showNewLessonForm(@PathVariable("subjectId") int subjectId, Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Subjects subject = subjectService.get(subjectId);
        model.addAttribute("subject", subject);
        List<Quizzes> quizzesList = quizService.listAll();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("quizzesList", quizzesList);
        model.addAttribute("lesson", new Lessons());
        return "add-lesson";
    }

    /**
     * Handles adding a new lesson to a given subject from a course content creator form submission.
     *
     * @param subjectId     id of subject to add new lesson for
     * @param lesson        Lessons object containing information about new lesson from course content creator form submission
     * @param bindingResult BindingResult object containing validation errors for new lesson from course content creator form submission
     * @param model         Model object containing information about course content creator form submission for adding new lesson
     * @param auth          authentication object representing currently authenticated user
     * @return redirect string indicating whether adding new lesson was successful or not
     */
    @PostMapping("/course-content-creator/subjects/{subjectId}/lessons/add")
    public String addNewLesson(@PathVariable("subjectId") int subjectId, @ModelAttribute("lesson") @Valid Lessons lesson, BindingResult bindingResult, Model model, Authentication auth) {
        if (bindingResult.hasErrors()) {
            Users authenticatedUser;
            try {
                authenticatedUser = userService.getAuthenticatedUser(auth);
            } catch (Exception NullPointerException) {
                return "redirect:/login";
            }
            List<UserNotifications> unreadNotifications = new ArrayList<>();
            List<UserNotifications> notifications = null;
            if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
                notifications = notificationService.getNotifications(authenticatedUser.getId());
                unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
            }
            Date currentTime = new Date();
            model.addAttribute("currentTime", currentTime);
            model.addAttribute("user", authenticatedUser);
            model.addAttribute("notification", notifications);
            model.addAttribute("unreadNotifications", unreadNotifications);
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "add-lesson";
        }
        lesson.setSubjects(subjectService.get(subjectId));
        lessonService.save(lesson);
        return "redirect:" + "/course-content-creator/subjects/" + subjectId + "/lessons";
    }

    /**
     * Displays an edit form for updating information about an existing lesson by id.
     *
     * @param model     model object to add attributes to be used in view rendering
     * @param id        id of existing lesson being edited
     * @param subjectId id of subject associated with existing lesson being edited
     * @param auth      authentication object representing currently authenticated user
     * @return name of view to render
     */
    @GetMapping("/course-content-creator/subjects/{subjectId}/lessons/edit/{id}")
    public String showEditLessonForm(Model model, @PathVariable(value = "id") int id, @PathVariable int subjectId, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Lessons lesson = lessonService.get(id);
        model.addAttribute("lesson", lesson);
        List<Quizzes> quizzesList = quizService.listAll();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("quizzesList", quizzesList);
        return "edit-lesson";
    }

    /**
     * Handles updating information about an existing lesson by id from a course content creator form submission.
     *
     * @param updatedLesson Lessons object containing updated information about existing lesson from course content creator form submission
     * @param id            id of existing lesson being edited
     * @param subjectId     id of subject associated with existing lesson being edited
     * @return redirect string indicating whether updating information about existing lesson was successful or not
     */
    @PostMapping("/course-content-creator/subjects/{subjectId}/lessons/edit/{id}")
    public String editLesson(@ModelAttribute("lesson") Lessons updatedLesson, @PathVariable(value = "id") int id, @PathVariable int subjectId) {
        Lessons lesson = lessonService.get(id);
        lesson.setName(updatedLesson.getName());
        lesson.setType(updatedLesson.getType());
        lesson.setDescription(updatedLesson.getDescription());
        lesson.setVideoLink(updatedLesson.getVideoLink());
        lesson.setMarkdownContent(updatedLesson.getMarkdownContent());
        lesson.setQuizzes(updatedLesson.getQuizzes());
        lesson.setStatus(updatedLesson.getStatus());
        lessonService.save(lesson);
        return "redirect:" + "/course-content-creator/subjects/" + subjectId + "/lessons";
    }

    /**
     * Handles deleting an existing lesson by id from a course content creator request.
     *
     * @param id  id of existing lesson being deleted
     * @param subjectId  id of subject associated with existing lesson being deleted
     * @return redirect string indicating whether deleting existing lesson was successful or not
     */
    @RequestMapping("/course-content-creator/subjects/{subjectId}/lessons/delete/{id}")
    public String deleteLesson(@PathVariable int id, @PathVariable int subjectId) {
        lessonService.delete(id);
        return "redirect:" + "/course-content-creator/subjects/" + subjectId + "/lessons";
    }
}