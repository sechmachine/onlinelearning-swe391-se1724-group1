package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Security.SecurityServices;
import com.swp.onlinelearning.Security.UserRole;
import com.swp.onlinelearning.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

/**
 * Controller class for handling actions related to user login.
 */
@Controller
@RequiredArgsConstructor
public class LoginController {

    private final SecurityServices securityServices;

    @Autowired
    private UserService userService;

    /**
     * Displays the login form page.
     *
     * @return the name of the view to render
     */
    @GetMapping("/login")
    public String showLoginForm() {
        return "login";
    }

    /**
     * Processes a login form submission.
     *
     * @param email    email entered by user
     * @param password password entered by user
     * @param model    model object to add attributes to be used in view rendering
     * @return name of view to render
     */
    @PostMapping("/login")
    public String authenticateUser(@RequestParam("email") String email, @RequestParam("password") String password, Model model) {
        try {
            UserDetails userDetails = userService.loadUserByUsername(email);
            if (userDetails == null || !securityServices.passwordEncoder().matches(password, userDetails.getPassword())) {
                model.addAttribute("error", "Invalid email or password");
                return "login";
            } else {
                // get the current date
                LocalDate currentDate = LocalDate.now();
                // get the user object from the userDetails
                Users user = (Users) userDetails;
                // check if the user is a premium user
                if (user.getRole().equals(UserRole.Premium)) {
                    // get the premium expiration date from the user object
                    LocalDate premiumExpirationDate = user.getPremiumExpirationDate();
                    // compare the current date with the premium expiration date
                    if (currentDate.isAfter(premiumExpirationDate)) {
                        // if the current date is after the premium expiration date, then the user's premium subscription has expired
                        // set the user's role to common
                        user.setRole(UserRole.Common);
                        // save the updated user object using the userService
                        userService.save(user);
                        // set a message indicating that the user's premium subscription has expired
                        model.addAttribute("message", "Your premium subscription has expired. You have been downgraded to a common user.");
                    }
                }
                model.addAttribute("user", userDetails);
                return "redirect:/main";
            }
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "login";
        }
    }


}