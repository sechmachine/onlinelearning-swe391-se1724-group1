package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.SubjectCategories;
import com.swp.onlinelearning.Models.Subjects;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.*;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Controller class for handling actions related to the main page.
 */
@Controller
@RequiredArgsConstructor
public class MainpageController {

    @Autowired
    final SubjectService subjectService;

    @Autowired
    final UserService userService;

    @Autowired
    final LessonsService lessonService;

    @Autowired
    final CategoryService categoryService;

    @Autowired
    final UserNotificationService notificationService;

    /**
     * Displays the main page.
     *
     * @param auth  the authentication object containing user authentication information
     * @param model the model object to add attributes to be used in view rendering
     * @return the name of the view to render
     */
    @GetMapping("/main")
    public String getMainpage(Authentication auth, Model model) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        List<Subjects> subjectsList = subjectService.listByPage(0);
        int page = subjectService.getTotalPage();
        Date currentTime = new Date();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        List<SubjectCategories> categories = categoryService.listAll();
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        model.addAttribute("notification", notifications);
        model.addAttribute("subjectCounts", subjectCounts);
        model.addAttribute("category", categories);
        model.addAttribute("totalPage", page);
        model.addAttribute("listSubjects", subjectsList);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mainpage";
    }

    /**
     * Displays the index page.
     *
     * @return the name of the view to render
     */
    @GetMapping("/")
    public String getIndex() {
        return "index";
    }


    /**
     * Displays the main page with a given filter applied.
     *
     * @param model  the model object to add attributes to be used in view rendering
     * @param filter the filter to apply to the main page content
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/main/filter={filter}")
    public String getMainpage1(Model model, @PathVariable(name = "filter") String filter, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        List<Subjects> subjectsList = new ArrayList<>();
        int page = subjectService.getTotalPage();
        switch (filter) {
            case "New":
                subjectsList = subjectService.orderByDateCreated(0);
                break;
            case "Upcoming":
                subjectsList = subjectService.findAllUnpublished(0);
                page = subjectService.getTotalUnpublishedPage();
                break;
            case "Free":
                subjectsList = subjectService.freeSubjects(0);
                page = subjectService.getTotalFreeSubjectPages();
                break;
            case "Popular":
                subjectsList = subjectService.orderByEnrolledStudents(0);
                break;
            default:
                return "redirect:/main";
        }
        List<SubjectCategories> categories = categoryService.listAll();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        model.addAttribute("notification", notifications);
        model.addAttribute("subjectCounts", subjectCounts);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("category", categories);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("listSubjects", subjectsList);
        model.addAttribute("totalPage", page);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mainpage";
    }

    /**
     * Displays a specific page of the main page with a given filter applied.
     *
     * @param model  the model object to add attributes to be used in view rendering
     * @param page   the page number of the main page to display
     * @param filter the filter to apply to the main page content
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/main/filter={filter}/page={page}")
    public String getMainpage1(Model model, @PathVariable(name = "page") int page, @PathVariable(name = "filter") String filter, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<Subjects> subjectsList;
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        int page1 = subjectService.getTotalPage();
        int position = (page - 1) * 9;
        switch (filter) {
            case "New":
                subjectsList = subjectService.orderByDateCreated(position);
                break;
            case "Upcoming":
                subjectsList = subjectService.findAllUnpublished(position);
                page1 = subjectService.getTotalUnpublishedPage();
                break;
            case "Free":
                subjectsList = subjectService.freeSubjects(position);
                page1 = subjectService.getTotalFreeSubjectPages();
                break;
            case "Popular":
                subjectsList = subjectService.orderByEnrolledStudents(position);
                break;
            default:
                return "redirect:/main";
        }
        List<SubjectCategories> categories = categoryService.listAll();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        model.addAttribute("notification", notifications);
        model.addAttribute("subjectCounts", subjectCounts);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("category", categories);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("listSubjects", subjectsList);
        model.addAttribute("totalPage", page1);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mainpage";
    }

    /**
     * Processes a search form submission.
     *
     * @param model   the model object to add attributes to be used in view rendering
     * @param request the HttpServletRequest object containing request information
     * @param auth    the authentication object containing user authentication information
     * @return the name of the view to redirect to
     */
    @PostMapping(value = "/searching")
    public String searching(Model model, HttpServletRequest request, Authentication auth) {
        String rawSearch = request.getParameter("search");
        return "redirect:/main/search=" + rawSearch;
    }

    /**
     * Updates all notifications for an authenticated user.
     *
     * @param auth the authentication object containing user authentication information
     * @return the name of the view to redirect to
     */
    @GetMapping(value = "/updateAllNotification")
    public String deleteNotification(Authentication auth) {
        Users users = userService.getAuthenticatedUser(auth);
        notificationService.updateAll(users.getId());
        return "redirect:/main";
    }

    /**
     * Displays a specific page of the main page.
     *
     * @param auth  the authentication object containing user authentication information
     * @param model the model object to add attributes to be used in view rendering
     * @param page  the page number of the main page to display
     * @return the name of the view to render
     */
    @GetMapping("/main/page={page}")
    public String getMainpage(Authentication auth, Model model, @PathVariable(name = "page") int page) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        int page1 = subjectService.getTotalPage();
        int position = (page - 1) * 9;
        List<Subjects> subjectsList = subjectService.listByPage(position);
        List<SubjectCategories> categories = categoryService.listAll();
        Date currentTime = new Date();
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        model.addAttribute("subjectCounts", subjectCounts);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("category", categories);
        model.addAttribute("totalPage", page1);
        model.addAttribute("listSubjects", subjectsList);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mainpage";
    }

    /**
     * Displays subjects by category on main page.
     *
     * @param auth       the authentication object containing user authentication information
     * @param model      the model object to add attributes to be used in view rendering
     * @param categoryid ID of category whose subjects are displayed on main page.
     * @return the name of the view to render
     */
    @GetMapping("/main/categoryid={categoryid}")
    public String getSubjectCategories(Authentication auth, Model model, @PathVariable(name = "categoryid") int categoryid) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        int page = subjectService.getTotalPageByCategory(categoryid);
        List<Subjects> subjectsList = subjectService.getSubjectsByCategory(categoryid, 0);
        List<SubjectCategories> categories = categoryService.listAll();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        model.addAttribute("notification", notifications);
        model.addAttribute("subjectCounts", subjectCounts);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("category", categories);
        model.addAttribute("totalPage", page);
        model.addAttribute("listSubjects", subjectsList);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mainpage";
    }

    /**
     * Displays a specific page of subjects by category on main page.
     *
     * @param auth       the authentication object containing user authentication information
     * @param model      the model object to add attributes to be used in view rendering
     * @param page       the page number of subjects by category on main page to display
     * @param categoryid ID of category whose subjects are displayed on main page.
     * @return the name of the view to render
     */
    @GetMapping("/main/categoryid={categoryid}/page={page}")
    public String getMainpage(Authentication auth, Model model, @PathVariable(name = "page") int page, @PathVariable(name = "categoryid") int categoryid) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        int page1 = subjectService.getTotalPageByCategory(categoryid);
        int position = (page - 1) * 9;
        List<Subjects> subjectsList = subjectService.getSubjectsByCategory(categoryid, position);
        List<SubjectCategories> categories = categoryService.listAll();
        model.addAttribute("category", categories);
        model.addAttribute("totalPage", page1);
        model.addAttribute("listSubjects", subjectsList);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        model.addAttribute("notification", notifications);
        model.addAttribute("subjectCounts", subjectCounts);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mainpage";
    }

    /**
     * Displays a specific page of search results on main page.
     *
     * @param model  the model object to add attributes to be used in view rendering
     * @param page   the page number of search results on main page to display
     * @param search search query whose results are displayed on main page.
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping(value = "main/search={search}/page={page}")
    public String getMainpage2(Model model, @PathVariable(name = "page") int page, @PathVariable(name = "search") String search, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        int position = (page - 1) * 9;
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        String holder = "%" + search + "%";
        int page1 = subjectService.getTotalSearchPage(holder);
        List<Subjects> subjectsList = subjectService.getSubjectByRequest(holder, position);
        List<SubjectCategories> categories = categoryService.listAll();
        model.addAttribute("category", categories);
        model.addAttribute("totalPage", page1);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        Date currentTime = new Date();
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        model.addAttribute("subjectCounts", subjectCounts);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("listSubjects", subjectsList);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mainpage";
    }

    /**
     * Displays search results on main page.
     *
     * @param model  the model object to add attributes to be used in view rendering
     * @param search search query whose results are displayed on main page.
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping(value = "main/search={search}")
    public String getMainpage2(Model model, @PathVariable(name = "search") String search, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        String holder = "%" + search + "%";
        int page = subjectService.getTotalSearchPage(holder);
        List<Subjects> subjectsList = subjectService.getSubjectByRequest(holder, 0);
        List<SubjectCategories> categories = categoryService.listAll();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        model.addAttribute("notification", notifications);
        model.addAttribute("subjectCounts", subjectCounts);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("category", categories);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("totalPage", page);
        model.addAttribute("listSubjects", subjectsList);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "mainpage";
    }


}