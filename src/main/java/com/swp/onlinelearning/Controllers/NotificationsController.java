package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.Notifications;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.NotificationService;
import com.swp.onlinelearning.Service.UserNotificationService;
import com.swp.onlinelearning.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller class for handling actions related to notifications.
 */
@Controller
@RequiredArgsConstructor
public class NotificationsController {
    @Autowired
    UserService userService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    UserNotificationService userNotificationService;

    /**
     * Displays a specific notification.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object containing user authentication information
     * @param id    the ID of the notification to display
     * @return the name of the view to render
     */
    @GetMapping("/notification/id={id}")
    public String showNotificationi(Model model, Authentication auth, @PathVariable(name = "id") int id) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        userNotificationService.updateNotification(authenticatedUser.getId(), id);
        Notifications notifications2 = notificationService.get(id);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("notification2", notifications2);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        return "notification-detail";
    }

}
