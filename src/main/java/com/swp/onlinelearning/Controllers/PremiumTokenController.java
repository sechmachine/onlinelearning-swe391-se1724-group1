package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.PremiumTokens;
import com.swp.onlinelearning.Models.PricePackages;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.PricePackageService;
import com.swp.onlinelearning.Service.TokenService;
import com.swp.onlinelearning.Service.UserNotificationService;
import com.swp.onlinelearning.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller class for handling actions related to premium tokens.
 */
@Controller
@RequiredArgsConstructor
public class PremiumTokenController {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private PricePackageService pricePackageService;
    @Autowired
    private UserNotificationService notificationService;
    @Autowired
    private UserService userService;

    /**
     * Displays the admin tokens page.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/admin/tokens")
    public String adminTokens(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<PremiumTokens> tokensList = tokenService.getTokensList(0);
        int totalPage = userService.getTotalPage();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("tokensList", tokensList);
        return "tokens-list";
    }

    /**
     * Displays a specific page of the admin tokens page.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param page  the page number of the admin tokens page to display
     * @param auth  the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/admin/tokens/page={page}")
    public String adminTokens(Model model, @PathVariable(name = "page") int page, Authentication auth) {
        int totalPage = tokenService.getTotalPage();
        int position = (page - 1) * 15;
        List<PremiumTokens> tokensList = tokenService.getTokensList(position);
        Users user = userService.getAuthenticatedUser(auth);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(user.getId()) != null) {
            notifications = notificationService.getNotifications(user.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", user);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("tokensList", tokensList);
        return "tokens-list";
    }

    /**
     * Displays the generate token form page for an admin.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/admin/tokens/generate-token")
    public String viewGenerateTokenForm(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<PricePackages> pricePackages = pricePackageService.listAll();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("pricePackages", pricePackages);
        return "token-generator";
    }

    /**
     * Processes a generate token form submission for an admin.
     *
     * @param pricePackageId ID of price package associated with generated token.
     * @return the name of the view to redirect to
     */
    @PostMapping("/admin/tokens/generate-token")
    public String adminGenerateToken(@RequestParam("pricePackage") int pricePackageId) {
        PricePackages pricePackage = pricePackageService.get(pricePackageId);
        tokenService.generateToken(pricePackage);
        return "redirect:/admin/tokens";
    }

    /**
     * Processes a delete token action for an admin.
     *
     * @param tokenId ID of token to delete.
     * @return the name of the view to redirect to
     */
    @RequestMapping("/admin/tokens/delete/{id}")
    public String adminDeleteToken(@PathVariable("id") int tokenId) {
        tokenService.deleteUsedToken(tokenId);
        return "redirect:/admin/tokens";
    }
}
