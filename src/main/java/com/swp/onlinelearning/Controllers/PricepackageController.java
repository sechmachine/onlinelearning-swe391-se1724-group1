package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.PricePackages;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.PricePackageService;
import com.swp.onlinelearning.Service.UserNotificationService;
import com.swp.onlinelearning.Service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller class for handling actions related to price packages.
 */
@Controller
@RequiredArgsConstructor
public class PricepackageController {

    @Autowired
    PricePackageService service;

    @Autowired
    UserService userService;

    @Autowired
    private UserNotificationService userNotificationService;

    /**
     * Displays the home page for price packages.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @RequestMapping("/price")
    public String viewHomePage(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        List<PricePackages> listPricePackages = service.listAll();
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listPricePackages", listPricePackages);
        model.addAttribute("user", authenticatedUser);
        return "pricepackages";
    }

    /**
     * Displays the new price package form page.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @return the name of the view to render
     */
    @RequestMapping("/pricePackage/new")
    public String showNewPricePackagesPage(Model model) {
        PricePackages pricePackages = new PricePackages();
        model.addAttribute("PricePackages", pricePackages);
        return "new_PricePackages";
    }

    /**
     * Processes a save price package form submission.
     *
     * @param pricePackages the PricePackages object containing price package information to save
     * @return the name of the view to redirect to
     */
    @PostMapping(value = "/pricePackage/save")
    public String savePricePackages(@ModelAttribute("PricePackages") PricePackages pricePackages) {
        service.save(pricePackages);
        return "redirect:/main";
    }

    @PostMapping(value = "/banking")
    public String bankingString(Model model, HttpServletRequest request){
        String rawPackage = request.getParameter("package");
        return "redirect:/banking/package=" + rawPackage;
    }

    @RequestMapping("/banking/package={id}")
    public String banking(Model model, @PathVariable(name = "id") int id, Authentication auth){
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        PricePackages pricePackages = service.get(id);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("price", pricePackages);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        return "banking";
    }

    /**
     * Displays the edit price package form page for a given price package ID.
     *
     * @param id the ID of the price package to edit
     * @return a ModelAndView object containing view name and model attributes
     */
    @RequestMapping("/pricePackage/edit/{id}")
    public ModelAndView showEditPricePackagesPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit_PricePackages");
        PricePackages pricePackages = service.get(id);
        mav.addObject("PricePackages", pricePackages);
        return mav;
    }

    /**
     * Processes a delete price package action for a given price package ID.
     *
     * @param id the ID of the price package to delete
     * @return the name of the view to redirect to
     */
    @RequestMapping("/pricePackage/delete/{id}")
    public String deletePricePackages(@PathVariable(name = "id") int id) {
        service.delete(id);
        return "redirect:/main";
    }
}
