package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.Questions;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.*;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller class for handling actions related to questions.
 */
@Controller
public class QuestionsController {


    @Autowired
    QuestionService questionService;

    @Autowired
    LessonsService lessonService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    UserService userService;

    @Autowired
    UserNotificationService notificationService;

    /**
     * Displays the list of questions for a given lesson and subject.
     *
     * @param model     the model object to add attributes to be used in view rendering
     * @param lessonId  the ID of the lesson whose questions are displayed
     * @param subjectId the ID of the subject whose questions are displayed
     * @param auth      the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/course-content-creator/subjects/{subjectId}/lessons/{lessonId}/questions")
    public String viewQuestionsList(Model model, @PathVariable int lessonId, @PathVariable int subjectId, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<Questions> listQuestions = questionService.listQuestionsBySubjectIdAndLessonId(subjectId, lessonId);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("lesson", lessonService.get(lessonId));
        model.addAttribute("subject", subjectService.get(subjectId));
        model.addAttribute("listQuestions", listQuestions);
        return "questions-list";
    }


    /**
     * Displays the new question form page for a given lesson and subject.
     *
     * @param model     the model object to add attributes to be used in view rendering
     * @param lessonId  the ID of the lesson for which a new question is added
     * @param subjectId the ID of the subject for which a new question is added
     * @param auth      the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/course-content-creator/subjects/{subjectId}/lessons/{lessonId}/questions/add")
    public String showNewQuestionForm(Model model, @PathVariable int lessonId, @PathVariable int subjectId, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        model.addAttribute("lesson", lessonService.get(lessonId));
        model.addAttribute("subject", subjectService.get(subjectId));
        Questions question = new Questions();
        model.addAttribute("Questions", question);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        return "add-question";
    }

    /**
     * Processes a new question form submission for a given lesson and subject.
     *
     * @param question      the question object containing data from form submission
     * @param lessonId      the ID of the lesson for which a new question is added
     * @param subjectId     the ID of the subject for which a new question is added
     * @param bindingResult the BindingResult object containing validation errors
     * @param model         the model object to add attributes to be used in view rendering
     * @param auth          the authentication object containing user authentication information
     * @return the name of the view to redirect to or render depending on validation errors
     */
    @PostMapping("/course-content-creator/subjects/{subjectId}/lessons/{lessonId}/questions/add")
    public String addNewQuestion(@ModelAttribute("Questions") @Valid Questions question, @PathVariable int lessonId, @PathVariable int subjectId, BindingResult bindingResult, Model model, Authentication auth) {
        if (bindingResult.hasErrors()) {
            Users authenticatedUser;
            try {
                authenticatedUser = userService.getAuthenticatedUser(auth);
            } catch (Exception NullPointerException) {
                return "redirect:/login";
            }
            List<UserNotifications> unreadNotifications = new ArrayList<>();
            List<UserNotifications> notifications = null;
            if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
                notifications = notificationService.getNotifications(authenticatedUser.getId());
                unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
            }
            Date currentTime = new Date();
            model.addAttribute("currentTime", currentTime);
            model.addAttribute("user", authenticatedUser);
            model.addAttribute("notification", notifications);
            model.addAttribute("unreadNotifications", unreadNotifications);
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "add-question";
        }
        question.setLessons(lessonService.get(lessonId));
        question.setSubjects(subjectService.get(subjectId));
        questionService.save(question);
        return "redirect:/course-content-creator/subjects/{subjectId}/lessons/{lessonId}/questions";
    }

    /**
     * Displays the edit question form page for a given question, lesson and subject.
     *
     * @param model     the model object to add attributes to be used in view rendering
     * @param id        the ID of the question to edit
     * @param lessonId  the ID of the lesson whose question is edited
     * @param subjectId the ID of the subject whose question is edited
     * @param auth      the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/course-content-creator/subjects/{subjectId}/lessons/{lessonId}/questions/edit/{id}")
    public String showEditQuestionForm(Model model, @PathVariable int id, @PathVariable int lessonId, @PathVariable int subjectId, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Questions question = questionService.get(id);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("question", question);
        model.addAttribute("lesson", lessonService.get(lessonId));
        model.addAttribute("subject", subjectService.get(subjectId));
        return "edit-question";
    }

    /**
     * Processes an edit question form submission for a given question, lesson and subject.
     *
     * @param updatedQuestion updated data from form submission for an existing question object.
     *                        The updated data includes dimension, level, content, media,
     *                        explanation and status.
     * @param id              the ID of the question to edit
     * @param lessonId        the ID of the lesson whose question is edited
     * @param subjectId       the ID of the subject whose question is edited
     * @return the name of the view to redirect to
     */
    @PostMapping("/course-content-creator/subjects/{subjectId}/lessons/{lessonId}/questions/edit/{id}")
    public String editQuestion(@ModelAttribute("Questions") Questions updatedQuestion, @PathVariable int id, @PathVariable int lessonId, @PathVariable int subjectId) {
        Questions question = questionService.get(id);
        question.setDimension(updatedQuestion.getDimension());
        question.setLevel(updatedQuestion.getLevel());
        question.setContent(updatedQuestion.getContent());
        question.setMedia(updatedQuestion.getMedia());
        question.setExplanation(updatedQuestion.getExplanation());
        question.setStatus(updatedQuestion.getStatus());
        questionService.save(question);
        return "redirect:" + "/course-content-creator/subjects/" + subjectId + "/lessons/" + lessonId + "/questions";
    }


    /**
     * Processes a delete question action for a given question, lesson and subject.
     *
     * @param id        the ID of the question to delete
     * @param lessonId  the ID of the lesson whose question is deleted
     * @param subjectId the ID of the subject whose question is deleted
     * @return the name of the view to redirect to
     */
    @RequestMapping("course-content-creator/subjects/{subjectId}/lessons/{lessonId}/questions/delete/{id}")
    public String deleteQuestions(@PathVariable(name = "id") int id, @PathVariable int lessonId, @PathVariable int subjectId) {
        questionService.delete(id);
        return "redirect:" + "/course-content-creator/subjects/" + subjectId + "/lessons/" + lessonId + "/questions";
    }
}
