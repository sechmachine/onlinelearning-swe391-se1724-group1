package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.Quizzes;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.QuizService;
import com.swp.onlinelearning.Service.UserNotificationService;
import com.swp.onlinelearning.Service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Controller class for handling actions related to quizzes.
 */
@Controller
public class QuizController {
    @Autowired
    QuizService quizService;

    @Autowired
    UserService userService;

    @Autowired
    UserNotificationService notificationService;

    /**
     * Displays the list of quizzes.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes")
    public String viewQuizzesList(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<Quizzes> listQuizzes = quizService.getQuizList(0);
        int totalPage = quizService.getTotalPage();
        Map<Integer, Integer> quizQuestionCounts = quizService.getQuizQuestionCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listQuizzes", listQuizzes);
        model.addAttribute("quizQuestionCounts", quizQuestionCounts);
        return "quizzes-list";
    }

    /**
     * Displays a specific page of the list of quizzes.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object containing user authentication information
     * @param page  the page number of the list of quizzes to display
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/page={page}")
    public String viewQuizzesList(Model model, Authentication auth, @PathVariable(name = "page") int page) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        int position = (page - 1) * 15;
        int totalPage = quizService.getTotalPage();
        List<Quizzes> listQuizzes = quizService.getQuizList(position);
        Map<Integer, Integer> quizQuestionCounts = quizService.getQuizQuestionCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listQuizzes", listQuizzes);
        model.addAttribute("quizQuestionCounts", quizQuestionCounts);
        return "quizzes-list";
    }

    /**
     * Displays search results for quizzes.
     *
     * @param model  the model object to add attributes to be used in view rendering
     * @param auth   the authentication object containing user authentication information
     * @param search search query whose results are displayed on quizzes list page.
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/search={search}")
    public String viewQuizzesList(Model model, Authentication auth, @PathVariable(name = "search") String search) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        String holder = "%" + search + "%";
        List<Quizzes> listQuizzes = quizService.getQuizListByRequest(holder, 0);
        int totalPage = quizService.getTotalSearchPage(holder);
        Map<Integer, Integer> quizQuestionCounts = quizService.getQuizQuestionCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listQuizzes", listQuizzes);
        model.addAttribute("quizQuestionCounts", quizQuestionCounts);
        return "quizzes-list";
    }

    /**
     * Processes a search form submission for quizzes.
     *
     * @param request the HttpServletRequest object containing request information
     * @return the name of the view to redirect to
     */
    @PostMapping(value = "/test-content-creator/quizzes/search")
    public String searching(HttpServletRequest request) {
        String rawSearch = request.getParameter("search");
        return "redirect:/test-content-creator/quizzes/search=" + rawSearch;
    }

    /**
     * Displays a specific page of search results for quizzes.
     *
     * @param model  the model object to add attributes to be used in view rendering
     * @param auth   the authentication object containing user authentication information
     * @param page   the page number of search results on quizzes list page to display
     * @param search search query whose results are displayed on quizzes list page.
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/search={search}/page={page}")
    public String viewQuizzesList(Model model, Authentication auth, @PathVariable(name = "page") int page, @PathVariable(name = "search") String search) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        String holder = "%" + search + "%";
        int position = (page - 1) * 15;
        int totalPage = quizService.getTotalSearchPage(holder);
        List<Quizzes> listQuizzes = quizService.getQuizListByRequest(holder, position);
        Map<Integer, Integer> quizQuestionCounts = quizService.getQuizQuestionCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listQuizzes", listQuizzes);
        model.addAttribute("quizQuestionCounts", quizQuestionCounts);
        return "quizzes-list";
    }

    /**
     * Displays the edit quiz form page for a given quiz.
     *
     * @param id    the ID of the quiz to edit
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/edit/{id}")
    public String showEditQuizForm(@PathVariable("id") Integer id, Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Quizzes quiz = quizService.get(id);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("quiz", quiz);
        return "edit-quiz";
    }

    /**
     * Processes an edit quiz form submission for a given quiz.
     *
     * @param id          the ID of the quiz to edit
     * @param updatedQuiz updated data from form submission for an existing quiz object.
     *                    The updated data includes name, level, duration and type.
     * @return the name of the view to redirect to
     */
    @PostMapping("/test-content-creator/quizzes/edit/{id}")
    public String editQuiz(@PathVariable("id") Integer id, @ModelAttribute("quiz") Quizzes updatedQuiz) {
        Quizzes quiz = quizService.get(id);
        quiz.setName(updatedQuiz.getName());
        quiz.setLevel(updatedQuiz.getLevel());
        quiz.setDuration(updatedQuiz.getDuration());
        quiz.setType(updatedQuiz.getType());
        quiz.setPassCondition(updatedQuiz.getPassCondition());
        quizService.save(quiz);
        return "redirect:/test-content-creator/quizzes";
    }

    /**
     * Processes a delete quiz action for a given quiz.
     *
     * @param id the ID of the quiz to delete
     * @return the name of the view to redirect to
     */
    @GetMapping("/test-content-creator/quizzes/delete/{id}")
    public String deleteQuiz(@PathVariable("id") Integer id) {
        quizService.delete(id);
        return "redirect:/test-content-creator/quizzes";
    }

    /**
     * Displays the new quiz form page.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @param auth  the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/add")
    public String showNewQuizForm(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Quizzes quiz = new Quizzes();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("quiz", quiz);
        return "add-quiz";
    }

    /**
     * Processes a new quiz form submission.
     *
     * @param quiz          the quiz object containing data from form submission
     * @param auth          the authentication object containing user authentication information
     * @param bindingResult the BindingResult object containing validation errors
     * @param model         the model object to add attributes to be used in view rendering
     * @return the name of the view to redirect to or render depending on validation errors
     */
    @PostMapping("/test-content-creator/quizzes/add")
    public String addNewLesson(@ModelAttribute("quiz") @Valid Quizzes quiz, Authentication auth, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            Users authenticatedUser;
            try {
                authenticatedUser = userService.getAuthenticatedUser(auth);
            } catch (Exception NullPointerException) {
                return "redirect:/login";
            }
            List<UserNotifications> unreadNotifications = new ArrayList<>();
            List<UserNotifications> notifications = null;
            if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
                notifications = notificationService.getNotifications(authenticatedUser.getId());
                unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
            }
            Date currentTime = new Date();
            model.addAttribute("currentTime", currentTime);
            model.addAttribute("user", authenticatedUser);
            model.addAttribute("notification", notifications);
            model.addAttribute("unreadNotifications", unreadNotifications);
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "add-lesson";
        }
        quizService.addQuiz(quiz);
        return "redirect:/test-content-creator/quizzes";
    }
}
