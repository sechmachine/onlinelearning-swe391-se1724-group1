package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.*;
import com.swp.onlinelearning.Service.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller class for handling actions related to quiz questions.
 */
@Controller
public class QuizQuestionsController {
    @Autowired
    QuizQuestionsService quizQuestionsService;
    @Autowired
    QuizService quizService;
    @Autowired
    LessonsService lessonService;
    @Autowired
    SubjectService subjectService;
    @Autowired
    QuestionService questionService;
    @Autowired
    UserService userService;
    @Autowired
    UserNotificationService notificationService;

    /**
     * Displays the list of quiz questions for a given quiz.
     *
     * @param quizId the ID of the quiz whose questions are displayed
     * @param model  the model object to add attributes to be used in view rendering
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/{quizId}/quiz-questions")
    public String viewQuizQuestionsList(@PathVariable(name = "quizId") int quizId, Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<QuizQuestions> quizQuestionsList = quizQuestionsService.getQuizList(quizId, 0);
        int totalPage = quizQuestionsService.getTotalPage(quizId);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("quizQuestionsList", quizQuestionsList);
        model.addAttribute("quiz", quizService.get(quizId));
        return "quiz-questions-list";
    }

    /**
     * Displays a specific page of the list of quiz questions for a given quiz.
     *
     * @param page   the page number of the list of quiz questions to display
     * @param quizId the ID of the quiz whose questions are displayed
     * @param model  the model object to add attributes to be used in view rendering
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/{quizId}/quiz-questions/page={page}")
    public String viewQuizQuestionsList(@PathVariable(name = "page") int page, @PathVariable(name = "quizId") int quizId, Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        int position = (page - 1) * 15;
        List<QuizQuestions> quizQuestionsList = quizQuestionsService.getQuizList(quizId, position);
        int totalPage = quizQuestionsService.getTotalPage(quizId);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("quizQuestionsList", quizQuestionsList);
        model.addAttribute("quiz", quizService.get(quizId));
        return "quiz-questions-list";
    }

    /**
     * Processes a search form submission for quiz questions.
     *
     * @param id      the ID of the quiz whose questions are searched
     * @param request the HttpServletRequest object containing request information
     * @return the name of the view to redirect to
     */
    @PostMapping(value = "/test-content-creator/quizzes/{quizId}/quiz-questions/search")
    public String searching(@PathVariable(name = "quizId") int id, HttpServletRequest request) {
        String rawSearch = request.getParameter("search");
        return "redirect:/test-content-creator/quizzes/" + id + "/quiz-questions/search=" + rawSearch;
    }

    /**
     * Displays search results for quiz questions.
     *
     * @param search search query whose results are displayed on quiz questions list page.
     * @param quizId the ID of the quiz whose questions are searched and displayed
     * @param model  the model object to add attributes to be used in view rendering
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/{quizId}/quiz-questions/search={search}")
    public String viewQuizQuestionsList(@PathVariable(name = "search") String search, @PathVariable(name = "quizId") int quizId, Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        String holder = "%" + search + "%";
        List<QuizQuestions> quizQuestionsList = quizQuestionsService.getQuizListByRequest(quizId, holder, 0);
        int totalPage = quizQuestionsService.getTotalSearchPage(quizId, holder);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("quizQuestionsList", quizQuestionsList);
        model.addAttribute("quiz", quizService.get(quizId));
        return "quiz-questions-list";
    }

    /**
     * Displays a specific page of search results for quiz questions.
     *
     * @param page   the page number of search results on quiz questions list page to display
     * @param search search query whose results are displayed on quiz questions list page.
     * @param quizId the ID of the quiz whose questions are searched and displayed
     * @param model  the model object to add attributes to be used in view rendering
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/{quizId}/quiz-questions/search={search}/page={page}")
    public String viewQuizQuestionsList(@PathVariable(name = "page") int page, @PathVariable(name = "search") String search, @PathVariable(name = "quizId") int quizId, Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        String holder = "%" + search + "%";
        int position = (page - 1) * 15;
        List<QuizQuestions> quizQuestionsList = quizQuestionsService.getQuizListByRequest(quizId, holder, position);
        int totalPage = quizQuestionsService.getTotalSearchPage(quizId, holder);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("quizQuestionsList", quizQuestionsList);
        model.addAttribute("quiz", quizService.get(quizId));
        return "quiz-questions-list";
    }

    /**
     * Displays available questions that can be added to a given quiz.
     *
     * @param quizId the ID of the quiz for which available questions are displayed and added.
     *               Available questions are those that belong to same subject as that of given
     *               `quizId` but not yet added to it.
     * @param model  the model object to add attributes to be used in view rendering
     * @param auth   the authentication object containing user authentication information
     * @return the name of the view to render
     */
    @GetMapping("/test-content-creator/quizzes/{quizId}/quiz-questions/add")
    public String addQuizQuestions(@PathVariable int quizId, Model model, Authentication auth) {
        // Retrieve the quiz and its associated subject
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Quizzes quiz = quizService.get(quizId);
        Lessons lesson = lessonService.getByQuizId(quizId);
        Subjects subject = subjectService.get(lesson.getSubjects().getId());

        // Retrieve the questions in the subject that are not yet added to the quiz
        List<Questions> availableQuestions = questionService.listAllBySubjectIdNotInQuiz(subject.getId(), quizId);

        // Add the available questions to the model
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("availableQuestions", availableQuestions);
        model.addAttribute("quiz", quiz);
        return "add-quiz-question";
    }

    /**
     * Processes an add question action for a given quiz and question.
     *
     * @param quizId     the ID of the quiz to which a question is added
     * @param questionId the ID of the question to add to the quiz
     * @return the name of the view to redirect to
     */
    @RequestMapping("/test-content-creator/quizzes/{quizId}/quiz-questions/add/{questionId}")
    public String addQuizQuestion(@PathVariable int quizId, @PathVariable int questionId) {
        // Create a new QuizQuestions object
        QuizQuestions quizQuestions = new QuizQuestions();
        quizQuestions.setQuizzes(quizService.get(quizId));
        quizQuestions.setQuestions(questionService.get(questionId));

        // Save the new QuizQuestions object
        quizQuestionsService.save(quizQuestions);

        // Redirect to the quiz questions list page
        return "redirect:/test-content-creator/quizzes/" + quizId + "/quiz-questions";
    }

    /**
     * Processes a delete question action for a given quiz and quiz question.
     *
     * @param quizId          the ID of the quiz from which a question is deleted
     * @param quizQuestionsId the ID of the QuizQuestions object to delete
     * @return the name of the view to redirect to
     */
    @RequestMapping("/test-content-creator/quizzes/{quizId}/quiz-questions/delete/{quizQuestionsId}")
    public String deleteQuizQuestion(@PathVariable int quizId, @PathVariable int quizQuestionsId) {
        // Delete the QuizQuestions object
        quizQuestionsService.delete(quizQuestionsId);

        // Redirect to the quiz questions list page
        return "redirect:/test-content-creator/quizzes/" + quizId + "/quiz-questions";
    }

}
