package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.Notifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.NotificationService;
import com.swp.onlinelearning.Service.UserNotificationService;
import com.swp.onlinelearning.Service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * The RegistrationController class handles HTTP requests related to user registration.
 */
@Controller
@RequiredArgsConstructor
public class RegistrationController {

    private static final String register = "register";
    @Autowired
    private UserService userService;

    @Autowired
    private UserNotificationService userNotificationService;

    @Autowired
    private NotificationService notificationService;

    /**
     * Displays the user registration form page.
     *
     * @param model the model object to add attributes to be used in view rendering
     * @return the name of the view to render
     */
    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        model.addAttribute("user", new Users());
        return register;
    }

    /**
     * Processes a user registration form submission.
     *
     * @param user          the user object containing data from form submission
     * @param bindingResult the BindingResult object containing validation errors
     * @param model         the model object to add attributes to be used in view rendering
     * @return the name of the view to redirect to or render depending on validation errors
     */
    @PostMapping("/register")
    public String registerUser(@ModelAttribute("user") @Valid Users user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return register;
        }
        if (userService.emailExists(user.getEmail())) {
            model.addAttribute("emailExists", true);
            return register;
        }
        Notifications notifications = notificationService.get(7);
        userService.registerUser(user);
        userNotificationService.addNotification(user, notifications);
        return "redirect:/login";
    }
}