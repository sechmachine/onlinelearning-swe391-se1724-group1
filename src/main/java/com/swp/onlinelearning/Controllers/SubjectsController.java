package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.SubjectCategories;
import com.swp.onlinelearning.Models.Subjects;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Service.CategoryService;
import com.swp.onlinelearning.Service.SubjectService;
import com.swp.onlinelearning.Service.UserNotificationService;
import com.swp.onlinelearning.Service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Controller
public class SubjectsController {
    @Autowired
    SubjectService subjectService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    UserService userService;
    @Autowired
    UserNotificationService notificationService;

    public static String UPLOAD_DIRECTORY = System.getProperty("user.dir") + "/src/main/resources/static/uploads";

    @GetMapping("/course-content-creator/subjects")
    public String viewSubjectsList(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<Subjects> listSubjects = subjectService.getAllSubjectsByPage(0);
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        int totalPage = subjectService.getSubjectsTotalPage();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjects", listSubjects);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "subjects-list";
    }

    @GetMapping("/course-content-creator/subjects/page={page}")
    public String viewSubjectsList(Model model, Authentication auth, @PathVariable(name = "page") int page) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        int position = (page - 1) * 15;
        List<Subjects> listSubjects = subjectService.getAllSubjectsByPage(position);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        int totalPage = subjectService.getSubjectsTotalPage();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjects", listSubjects);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "subjects-list";
    }

    @PostMapping(value = "/course-content-creator/subjects/search")
    public String searching(HttpServletRequest request) {
        String rawSearch = request.getParameter("search");
        return "redirect:/course-content-creator/subjects/search=" + rawSearch;
    }

    @GetMapping("/course-content-creator/subjects/search={search}")
    public String viewSubjectsList(Model model, Authentication auth, @PathVariable(name = "search") String search) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        String holder = "%" + search + "%";
        List<Subjects> listSubjects = subjectService.getAllSubjectByRequest(holder,0);
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        int totalPage = subjectService.getSubjectsTotalPage();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjects", listSubjects);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "subjects-list";
    }

    @GetMapping("/course-content-creator/subjects/search={search}/page={page}")
    public String viewSubjectsList(Model model, Authentication auth, @PathVariable(name = "page") int page, @PathVariable(name = "search") String search) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        int position = (page - 1) * 15;
        String holder = "%" + search + "%";
        List<Subjects> listSubjects = subjectService.getAllSubjectByRequest(holder, position);
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        int totalPage = subjectService.getTotalSearchPage2(search);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjects", listSubjects);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "subjects-list";
    }

    @GetMapping("/course-content-creator/subject-categories")
    public String viewSubjectCategoriesList(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        int totalPage = categoryService.getTotalPage();
        List<SubjectCategories> listSubjectCategories = categoryService.getSubjectCategoriesList(0);
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjectCategories", listSubjectCategories);
        model.addAttribute("subjectCounts", subjectCounts);
        return "subject-categories-list";
    }

    @GetMapping("/course-content-creator/subject-categories/page={page}")
    public String viewSubjectCategoriesList(Model model, Authentication auth, @PathVariable(name = "page") int page) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        int totalPage = categoryService.getTotalPage();
        int position = (page - 1) * 15;
        List<SubjectCategories> listSubjectCategories = categoryService.getSubjectCategoriesList(position);
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjectCategories", listSubjectCategories);
        model.addAttribute("subjectCounts", subjectCounts);
        return "subject-categories-list";
    }

    @PostMapping(value = "/course-content-creator/subject-categories/search")
    public String searching2(HttpServletRequest request) {
        String rawSearch = request.getParameter("search");
        return "redirect:/course-content-creator/subject-categories/search=" + rawSearch;
    }

    @GetMapping("/course-content-creator/subject-categories/search={search}")
    public String viewSubjectCategoriesList(Model model, Authentication auth, @PathVariable(name = "search") String search) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        String holder = "%" + search + "%";
        int totalPage = categoryService.getTotalSearchPage(holder);
        List<SubjectCategories> listSubjectCategories = categoryService.getSubjectCategoriesListByRequest(holder,0);
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjectCategories", listSubjectCategories);
        model.addAttribute("subjectCounts", subjectCounts);
        return "subject-categories-list";
    }

    @GetMapping("/course-content-creator/subject-categories/search={search}/page={page}")
    public String viewSubjectCategoriesList(Model model, Authentication auth, @PathVariable(name = "page") int page, @PathVariable(name = "search") String search) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        String holder = "%" + search + "%";
        int position = (page - 1) * 15;
        int totalPage = categoryService.getTotalSearchPage(holder);
        List<SubjectCategories> listSubjectCategories = categoryService.getSubjectCategoriesListByRequest(holder,position);
        Map<Integer, Integer> subjectCounts = categoryService.getSubjectCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjectCategories", listSubjectCategories);
        model.addAttribute("subjectCounts", subjectCounts);
        return "subject-categories-list";
    }


    @GetMapping("/course-content-creator/subject-categories/{id}/subjects")
    public String viewSubjectsListByCategory(Model model, @PathVariable(value = "id") int id, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<Subjects> listSubjects = subjectService.listAllSubjectsByCategory(id);
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("notification", notifications);
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("listSubjects", listSubjects);
        model.addAttribute("enrolledCounts", enrolledCounts);
        return "subjects-list";
    }

    @GetMapping("/course-content-creator/subjects/add")
    public String showAddSubjectForm(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("subject", new Subjects());
        model.addAttribute("categoriesList", categoryService.listAll());
        return "add-subject";
    }

    @PostMapping(value = "/course-content-creator/subjects/add")
    public String addNewSubject(@ModelAttribute("subject") @Valid Subjects subject, BindingResult bindingResult, Model model, Authentication auth, @RequestParam("thumbnail") MultipartFile file) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "add-subject";
        }
        Users user = userService.getAuthenticatedUser(auth);
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                String fileName = file.getOriginalFilename();
                Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName);
                // Check if a file with the same name already exists
                if (Files.exists(path)) {
                    // Generate a random string to use as the new file name
                    String randomString = UUID.randomUUID().toString();
                    fileName = randomString + "_" + fileName;
                    path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName);
                }
                Files.write(path, bytes);
                String uploadedFilePath = "/uploads/" + fileName;
                subject.setThumbnailImage(uploadedFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        subjectService.addSubject(subject, user);
        return "redirect:/course-content-creator/subjects";
    }


    @GetMapping("course-content-creator/subjects/edit/{id}")
    public String showEditSubjectPage(Model model, @PathVariable(value = "id") int id, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        Subjects subject = subjectService.get(id);
        List<SubjectCategories> categoriesList = categoryService.listAll();
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("subject", subject);
        model.addAttribute("categoriesList", categoriesList);
        return "edit-subject";
    }

    @PostMapping("/course-content-creator/subjects/edit/{id}")
    public String editSubject(@PathVariable(value = "id") int id, @ModelAttribute("subject") Subjects updatedSubject, @RequestParam("thumbnail") MultipartFile file) {
        Subjects subject = subjectService.get(id);
        subject.setName(updatedSubject.getName());
        subject.setSubjectCategories(updatedSubject.getSubjectCategories());
        subject.setDuration(updatedSubject.getDuration());
        subject.setTier(updatedSubject.getTier());
        subject.setStatus(updatedSubject.getStatus());
        subject.setDescription(updatedSubject.getDescription());
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                String fileName = file.getOriginalFilename();
                Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName);
                // Check if a file with the same name already exists
                if (Files.exists(path)) {
                    // Generate a random string to use as the new file name
                    String randomString = UUID.randomUUID().toString();
                    fileName = randomString + "_" + fileName;
                    path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName);
                }
                Files.write(path, bytes);
                String uploadedFilePath = "/uploads/" + fileName;
                subject.setThumbnailImage(uploadedFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        subjectService.save(subject);
        return "redirect:/course-content-creator/subjects";
    }


    @RequestMapping("/course-content-creator/subjects/delete/{id}")
    public String deleteSubject(@PathVariable(value = "id") int id) {
        subjectService.delete(id);
        return "redirect:/course-content-creator/subjects";
    }

    @GetMapping("/course-content-creator/subject-categories/add")
    public String showAddSubjectCategoryForm(Model model, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("subjectCategory", new SubjectCategories());
        return "add-subject-category";
    }

    @PostMapping(value = "/course-content-creator/subject-categories/add")
    public String addNewSubjectCategory(@ModelAttribute("subjectCategory") @Valid SubjectCategories subjectCategory, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());
            return "add-subject-category";
        }
        categoryService.save(subjectCategory);
        return "redirect:/course-content-creator/subject-categories";
    }

    @GetMapping("course-content-creator/subject-categories/edit/{id}")
    public String showEditSubjectCategoryPage(Model model, @PathVariable(value = "id") int id, Authentication auth) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        SubjectCategories subjectCategory = categoryService.get(id);
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (notificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = notificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = notificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("subjectCategory", subjectCategory);
        return "edit-subject-category";
    }

    @PostMapping("/course-content-creator/subject-categories/edit/{id}")
    public String editSubjectCategory(@PathVariable(value = "id") int id, @ModelAttribute("subjectCategory") SubjectCategories updatedSubjectCategory) {
        SubjectCategories subjectCategory = categoryService.get(id);
        subjectCategory.setName(updatedSubjectCategory.getName());
        categoryService.save(subjectCategory);
        return "redirect:/course-content-creator/subject-categories";
    }

    @RequestMapping("/course-content-creator/subject-categories/delete/{id}")
    public String deleteSubjectCategory(@PathVariable(value = "id") int id) {
        categoryService.delete(id);
        return "redirect:/course-content-creator/subject-categories";
    }
}