package com.swp.onlinelearning.Controllers;

import com.swp.onlinelearning.Models.Notifications;
import com.swp.onlinelearning.Models.Subjects;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Repository.UserRepository;
import com.swp.onlinelearning.Service.NotificationService;
import com.swp.onlinelearning.Service.SubjectService;
import com.swp.onlinelearning.Service.UserNotificationService;
import com.swp.onlinelearning.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class UserInfoController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserNotificationService userNotificationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private SubjectService subjectService;

    public static String UPLOAD_DIRECTORY = System.getProperty("user.dir") + "/src/main/resources/static/uploads";

    @GetMapping("/profile")
    public String viewInfoPage(Authentication auth, Model model) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("imageuploadpath", UPLOAD_DIRECTORY);
        return "profile";
    }

    @GetMapping("/profile/id={id}")
    public String viewInfoPage(@PathVariable (name = "id") int id, Authentication auth, Model model) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Users users = userService.get(id);
        List<Subjects> subjectsList = subjectService.getAllSubjectByCreator(users.getId());
        Date currentTime = new Date();
        Map<Integer, Integer> enrolledCounts = subjectService.getStudentEnrolledCounts();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("enrolledCounts", enrolledCounts);
        model.addAttribute("subjectsList", subjectsList);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", users);
        model.addAttribute("imageuploadpath", UPLOAD_DIRECTORY);
        return "profile";
    }

    @GetMapping("/profile/edit")
    public String viewEditInfoForm(Authentication auth, Model model) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        return "profile-edit";
    }

    @PostMapping("/profile/edit")
    public String editUserInfo(Authentication auth, @ModelAttribute("user") Users updatedUser) {

        // Get the currently authenticated user
        Users user = userService.getAuthenticatedUser(auth);

        // Update the user's information

        user.setFullName(updatedUser.getFullName());
        user.setGender(updatedUser.getGender());
        user.setMobile(updatedUser.getMobile());
        // Save the updated user information
        userRepository.save(user);

        // Redirect to a success page
        return "redirect:/profile";
    }

    @GetMapping("/profile/upload-avatar")
    public String viewUploadAvatarForm(Authentication auth, Model model) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        return "profile-upload-avatar";
    }

    @PostMapping("/profile/upload-avatar")
    public String uploadAvatar(Authentication auth, @RequestParam("avatar") MultipartFile file) {
        Users user = userService.getAuthenticatedUser(auth);
        if (file.isEmpty()) {
            return "redirect:/profile/upload-avatar";
        }
        try {
            byte[] bytes = file.getBytes();
            String originalFileName = file.getOriginalFilename();
            assert originalFileName != null;
            String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
            String fileName = "Avatar-UID" + user.getId() + "-1" + fileExtension;
            Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName);
            // Check if a file with the same name already exists
            while (Files.exists(path)) {
                // Increment the last digit of the file name
                int lastDigitIndex = fileName.lastIndexOf("-") + 1;
                int lastDigit = Integer.parseInt(fileName.substring(lastDigitIndex, fileName.indexOf(".")));
                lastDigit++;
                fileName = fileName.substring(0, lastDigitIndex) + lastDigit + fileExtension;
                path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName);
            }
            Files.write(path, bytes);
            String uploadedFilePath = "/uploads/" + fileName;
            user.setAvatar(uploadedFilePath);
            userRepository.save(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/profile";
    }


    @GetMapping("/profile/change-password")
    public String viewChangePasswordForm(Authentication auth, Model model) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        return "profile-change-password";
    }

    @PostMapping("/profile/change-password")
    public String changePassword(Authentication auth, @RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword") String newPassword, @RequestParam("confirmNewPassword") String confirmPassword, Model model) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        List<UserNotifications> unreadNotifications = new ArrayList<>();
        List<UserNotifications> notifications = null;
        if (userNotificationService.getNotifications(authenticatedUser.getId()) != null) {
            notifications = userNotificationService.getNotifications(authenticatedUser.getId());
            unreadNotifications = userNotificationService.getUnreadNotificationsList(notifications);
        }
        Date currentTime = new Date();
        model.addAttribute("currentTime", currentTime);
        model.addAttribute("notification", notifications);
        model.addAttribute("unreadNotifications", unreadNotifications);
        model.addAttribute("user", authenticatedUser);
        if (oldPassword.isEmpty() || newPassword.isEmpty() || confirmPassword.isEmpty()) {
            model.addAttribute("error", "Please fill in all fields");
            return "profile-change-password";
        }
        if (!newPassword.equals(confirmPassword)) {
            model.addAttribute("error", "New password and confirm new password do not match");
            return "profile-change-password";
        }
        if (!passwordEncoder.matches(oldPassword, authenticatedUser.getPassword())) {
            model.addAttribute("error", "Old password is incorrect");
            return "profile-change-password";
        }
        userService.updatePassword(newPassword, authenticatedUser);
        model.addAttribute("message", "Password changed successfully");
        return "profile-change-password";
    }


    @PostMapping("/profile/token")
    public String checkToken(Authentication auth, @ModelAttribute("token") String token) {
        Users authenticatedUser;
        try {
            authenticatedUser = userService.getAuthenticatedUser(auth);
        } catch (Exception NullPointerException) {
            return "redirect:/login";
        }
        userService.upgradePremium(token, authenticatedUser);
        Notifications notification = notificationService.get(8);
        userNotificationService.addNotification(authenticatedUser, notification);
        return "redirect:/profile";
    }
}
