package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnTransformer;

/**
 * The AnswerOptions class represents an answer option for a question.
 * It contains information about the question, the content of the answer option, and whether it is the correct answer.
 */
@Entity
@Table(name = "answeroptions")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnswerOptions {

    /**
     * The unique ID of the answer option.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The ID of the question associated with this answer option.
     */

    @ManyToOne
    @JoinColumn(name = "QuestionID", nullable = false, referencedColumnName = "id")
    private Questions questions;

    /**
     * The content of the answer option.
     */
    @ColumnTransformer(read = "TRIM(Content)")
    @Column(name = "Content", nullable = false)
    private String content;

    /**
     * Whether this answer option is the correct answer for the associated question.
     */
    @Column(name = "IsKey", nullable = false)
    private Long isKey;

}