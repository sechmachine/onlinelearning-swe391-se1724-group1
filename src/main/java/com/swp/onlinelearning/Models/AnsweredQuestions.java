package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The AnsweredQuestions class represents a record of a question that has been answered by a user.
 * It contains information about the quiz history, the question, and the selected answer.
 */
@Entity
@Table(name = "answeredquestions")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnsweredQuestions {

    /**
     * The unique ID of the answered question.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The ID of the quiz history associated with this answered question.
     */
    @ManyToOne
    @JoinColumn(name = "QuizHistoryID", nullable = false, referencedColumnName = "id")
    private QuizHistory quizHistory;

    /**
     * The ID of the question associated with this answered question.
     */
    @ManyToOne
    @JoinColumn(name = "QuestionID", nullable = false, referencedColumnName = "id")
    private Questions questions;

    /**
     * The ID of the answer selected by the user for this question.
     */
    @Column(name = "SelectedAnswerId", nullable = false)
    private Integer selectedAnswerId;
}