package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnTransformer;

import java.io.Serializable;

/**
 * The Lessons class represents a lesson in a subject.
 * It contains information about the subject, the name of the lesson, its type, and its content.
 */
@Entity
@Table(name = "lessons")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Lessons implements Serializable {

    /**
     * The unique ID of the lesson.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The ID of the subject associated with this lesson.
     */

    @ManyToOne
    @JoinColumn(name = "SubjectID", nullable = false, referencedColumnName = "id")
    private Subjects subjects;

    /**
     * The name of the lesson.
     */
    @Column(name = "Name", nullable = false)
    private String name;

    /**
     * The type of the lesson.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "Type", nullable = false)
    private Type type;

    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)
    private Status status;

    /**
     * The link to the video associated with this lesson, if any.
     */
    @Column(name = "VideoLink")
    private String videoLink;

    /**
     * The HTML content of the lesson, if any.
     */
    @ColumnTransformer(read = "TRIM(MarkdownContent)")
    @Column(name = "MarkdownContent")
    private String markdownContent;

    /**
     * The ID of the quiz associated with this lesson, if any.
     */
    @ManyToOne
    @JoinColumn(name = "QuizID", nullable = false, referencedColumnName = "id")
    private Quizzes quizzes;

    /**
     * The Description of the lesson, if any.
     */
    @ColumnTransformer(read = "TRIM(Description)")
    @Column(name = "Description")
    private String description;


    /**
     * The possible types of a lesson.
     */
    public enum Type {
        SubjectTopic, Lesson, Quiz
    }

    public enum Status {
        Draft, Published, Unpublished
    }
}