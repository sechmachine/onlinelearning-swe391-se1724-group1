package com.swp.onlinelearning.Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "premiumtokens")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PremiumTokens implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "TokenCreationDate")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @JsonDeserialize(using = DateDeserializers.DateDeserializer.class)
    private LocalDate tokenCreationDate;

    @Column(name = "Token", nullable = false)
    private String token;

    @Column(name = "Uses", nullable = false)
    private Integer uses;

    @ManyToOne
    @JoinColumn(name = "PackageID", referencedColumnName = "id")
    private PricePackages pricePackages;

}
