package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The PricePackages class represents a price package for a subject.
 * It contains information about the subject, the name of the package, its access duration, status, list price, sale price, and description.
 */
@Entity
@Table(name = "pricepackages")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PricePackages implements Serializable {

    /**
     * The unique ID of the price package.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The duration of access to the subject provided by this price package.
     */
    @Column(name = "AccessDuration", nullable = false)
    private Integer accessDuration;


    /**
     * The list price of the price package.
     */
    @Column(name = "ListPrice", nullable = false)
    private BigDecimal listPrice;

    @Column(name = "SalePrice", nullable = false)
    private BigDecimal listSale;

}