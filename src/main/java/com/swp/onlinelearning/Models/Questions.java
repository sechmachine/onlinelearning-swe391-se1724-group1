package com.swp.onlinelearning.Models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * The Questions class represents a question in a subject or lesson.
 * It contains information about the subject, lesson, dimension, level, status, content, media, and explanation of the question.
 */
@Entity
@Table(name = "questions")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Questions implements Serializable {

    /**
     * The unique ID of the question.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The ID of the subject associated with this question.
     */
    @ManyToOne
    @JoinColumn(name = "SubjectID", nullable = false, referencedColumnName = "id")
    private Subjects subjects;

    /**
     * The ID of the lesson associated with this question, if any.
     */
    @ManyToOne
    @JoinColumn(name = "LessonID", nullable = false, referencedColumnName = "id")
    private Lessons lessons;

    /**
     * The dimension of the question.
     */
    @Column(name = "Dimension")
    private String dimension;

    /**
     * The level of difficulty of the question.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "Level", nullable = false)
    private Level level;

    /**
     * The status of the question.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)
    private Status status;

    /**
     * The content of the question.
     */
    @Column(name = "Content", nullable = false)
    private String content;

    /**
     * The media associated with this question, if any.
     */
    @Column(name = "Media")
    private String media;

    /**
     * The explanation for the correct answer to this question.
     */
    @Column(name = "Explanation")
    private String explanation;

    @OneToMany(mappedBy = "questions", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<AnswerOptions> answerOptionsList;

    /**
     * The possible levels of difficulty for a question.
     */
    public enum Level {
        Hard, Medium, Easy
    }

    /**
     * The possible statuses for a question.
     */
    public enum Status {
        Active, Inactive
    }
}