package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.Date;

/**
 * The QuizHistory class represents a record of a user taking a quiz.
 * It contains information about the user, the quiz, the score, and the date the quiz was taken.
 */
@Entity
@Table(name = "quizhistory")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuizHistory {

    /**
     * The unique ID of the quiz history record.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The ID of the user associated with this quiz history record.
     */
    @ManyToOne
    @JoinColumn(name = "UserID", nullable = false, referencedColumnName = "id")
    private Users users;

    /**
     * The ID of the quiz associated with this quiz history record.
     */
    @ManyToOne
    @JoinColumn(name = "QuizID", nullable = false, referencedColumnName = "id")
    private Quizzes quizzes;

    /**
     * The score achieved by the user on this quiz.
     */
    @Column(name = "Score", nullable = false)
    private Double score;

    /**
     * The date and time when the user took this quiz.
     */
    @Column(name = "DateTaken", nullable = false)
    private Date dateTaken;

    @Column(name = "Taken", nullable = true)
    private Time timeTaken;
}