package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * The QuizQuestions class represents a question in a quiz.
 * It contains information about the quiz and the question.
 */
@Entity
@Table(name = "quizquestions")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuizQuestions implements Serializable {

    /**
     * The unique ID of the quiz question.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The ID of the quiz associated with this quiz question.
     */

    @ManyToOne
    @JoinColumn(name = "QuizID", nullable = false, referencedColumnName = "id")
    private Quizzes quizzes;

    /**
     * The ID of the question associated with this quiz question.
     */
    @ManyToOne
    @JoinColumn(name = "QuestionID", nullable = false, referencedColumnName = "id")
    private Questions questions;
}