package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Quizzes class represents a quiz in a subject.
 * It contains information about the subject, the name of the quiz, its level of difficulty, number of questions, duration, pass rate, and type.
 */
@Entity
@Table(name = "quizzes")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Quizzes implements Serializable {

    /**
     * The unique ID of the quiz.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The name of the quiz.
     */
    @Column(name = "Name", nullable = false)
    private String name;

    /**
     * The level of difficulty of the quiz.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "Level", nullable = false)
    private Level level;

    /**
     * The duration of the quiz in minutes.
     */
    @Column(name = "Duration", nullable = false)
    private Integer duration;

    /**
     * The pass rate for the quiz as a percentage.
     */
    @Column(name = "PassCondition", nullable = false)
    private Double passCondition;

    /**
     * The type of the quiz.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "Type", nullable = false)
    private Type type;


    /**
     * The possible levels of difficulty for a quiz.
     */
    public enum Level {
        Hard, Medium, Easy
    }

    /**
     * The possible types for a quiz.
     */
    public enum Type {
        Simulation, Quiz
    }

}