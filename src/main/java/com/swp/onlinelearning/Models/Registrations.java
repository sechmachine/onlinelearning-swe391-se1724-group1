package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * The Registrations class represents a user's registration for a subject.
 * It contains information about the user, the subject, the price package, the total cost, the status, and the validity period of the registration.
 */
@Entity
@Table(name = "registrations")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Registrations implements Serializable {

    /**
     * The unique ID of the registration.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The ID of the user associated with this registration.
     */
    @Column(name = "UserID", nullable = false, insertable = false, updatable = false)
    private Integer userId;
    @ManyToOne
    @JoinColumn(name = "UserID", referencedColumnName = "id")
    private Users user;

    /**
     * The ID of the subject associated with this registration.
     */
    @ManyToOne
    @JoinColumn(name = "SubjectID", referencedColumnName = "id")
    private Subjects subject;


    /**
     * The status of this registration.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)
    private Status status;

    /**
     * The possible statuses for a registration.
     */
    public enum Status {
        Enrolled, Dropped
    }
}