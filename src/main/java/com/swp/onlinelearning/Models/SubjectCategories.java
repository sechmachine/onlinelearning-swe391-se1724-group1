package com.swp.onlinelearning.Models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * The SubjectCategories class represents a category for subjects.
 * It contains information about the name of the category.
 */
@Entity
@Table(name = "subjectcategories")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubjectCategories implements Serializable {

    /**
     * The unique ID of the subject category.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The name of the subject category.
     */
    @Column(name = "Name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "subjectCategories", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Subjects> subjectsList;

}