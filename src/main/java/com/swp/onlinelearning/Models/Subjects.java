package com.swp.onlinelearning.Models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnTransformer;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The Subjects class represents a subject.
 * It contains information about the category, thumbnail image, owner, status, dimension, description, duration, and date created of the subject.
 */
@Entity
@Table(name = "subjects")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subjects implements Serializable {

    /**
     * The name of the subject.
     */
    @Column(name = "Name", nullable = false)
    public String name;

    /**
     * The unique ID of the subject.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The ID of the category associated with this subject.
     */

    @ManyToOne
    @JoinColumn(name = "CategoryID", nullable = false, referencedColumnName = "id")
    private SubjectCategories subjectCategories;

    /**
     * The thumbnail image associated with this subject, if any.
     */
    @Column(name = "ThumbnailImage")
    private String thumbnailImage;

    /**
     * The ID of the owner of this subject.
     */

    @ManyToOne
    @JoinColumn(name = "OwnerID", nullable = false, referencedColumnName = "id")
    private Users creator;

    /**
     * The status of this subject.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)
    private Status status;

    /**
     * The dimension of this subject.
     */
    @Column(name = "Dimension")
    private String dimension;

    /**
     * The description of this subject.
     */
    @ColumnTransformer(read = "TRIM(Description)")
    @Column(name = "Description")
    private String description;

    /**
     * The duration of this subject in minutes.
     */
    @Column(name = "Duration")
    private Integer duration;

    /**
     * The date when this subject was created.
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "DateCreated", nullable = false)
    private Date dateCreated;

    @OneToMany(mappedBy = "subjects", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Lessons> lessonsList;

    @OneToMany(mappedBy = "subjects", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Questions> questionsList;

    @Enumerated(EnumType.STRING)
    @Column(name = "Tier", nullable = false)
    private Tier tier;

    /**
     * The possible statuses for a subject.
     */
    public enum Status {
        Draft, Published, Unpublished
    }

    public enum Tier {
        Free, Premium
    }
}