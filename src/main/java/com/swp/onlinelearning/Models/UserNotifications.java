package com.swp.onlinelearning.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "usernotifications")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserNotifications implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "time", nullable = false)
    private Date time;

    @ManyToOne
    @JoinColumn(name = "UserID", nullable = false, referencedColumnName = "id")
    private Users users;

    @ManyToOne
    @JoinColumn(name = "NotificationID", nullable = false, referencedColumnName = "id")
    private Notifications notifications;

    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)

    private UserNotifications.Status status;
    public enum Status {
        Read, Unread
    }
}
