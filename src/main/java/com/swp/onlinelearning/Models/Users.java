package com.swp.onlinelearning.Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.swp.onlinelearning.Security.UserRole;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

/**
 * The Users class represents a user of the online learning software.
 * It contains information about the user's full name, gender, email, mobile number, password, role, status, and avatar.
 * It also implements the UserDetails interface for Spring Security authentication.
 */
@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Users implements Serializable, UserDetails {

    /**
     * The unique ID of the user.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The full name of the user.
     */
    @Column(name = "FullName", nullable = false)
    private String fullName;

    /**
     * The gender of the user.
     */
    @Column(name = "Gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    /**
     * The email address of the user.
     */
    @Column(name = "Email", nullable = false)
    private String email;

    /**
     * The mobile number of the user.
     */
    @Column(name = "Mobile")
    private String mobile;

    /**
     * The password of the user.
     */
    @Column(name = "Password", nullable = false)
    private String password;

    /**
     * The role of the user.
     */
    @Column(name = "Role", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role;

    /**
     * The status of the user.
     */
    @Column(name = "Status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    /**
     * The PremiumStartDate of the user.
     */
    @Column(name = "PremiumStartDate")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @JsonDeserialize(using = DateDeserializers.DateDeserializer.class)
    private LocalDate premiumStartDate;

    /**
     * The PremiumExpirationDate of the user.
     */
    @Column(name = "PremiumExpirationDate")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @JsonDeserialize(using = DateDeserializers.DateDeserializer.class)
    private LocalDate premiumExpirationDate;


    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Subjects> subjectsList;


    /**
     * The avatar associated with this user, if any.
     */
    @Column(name = "Avatar")
    private String avatar;

    /**
     * Returns the username used to authenticate the user. This method is required by the UserDetails interface.
     *
     * @return the username
     */
    @Column(name = "PasswordResetToken")
    private String passwordResetToken;

    @Override
    public String getUsername() {
        return email;
    }

    /**
     * Indicates whether the user's account has expired. This method is required by the UserDetails interface.
     *
     * @return true if the user's account is valid (non-expired), false otherwise
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Indicates whether the user is locked or unlocked. This method is required by the UserDetails interface.
     *
     * @return true if the user is not locked, false otherwise
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Indicates whether the user's credentials (password) have expired. This method is required by the UserDetails interface.
     *
     * @return true if the user's credentials are valid (non-expired), false otherwise
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Indicates whether the user is enabled or disabled. This method is required by the UserDetails interface.
     *
     * @return true if the user is enabled, false otherwise
     */
    @Override
    public boolean isEnabled() {
        return status == Status.Active;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return role.getAuthorities();
    }

    /**
     * The possible genders for a user.
     */
    public enum Gender {
        Male, Female, Other
    }


    /**
     * The possible statuses for a user.
     */
    public enum Status {
        Active, Inactive
    }
}