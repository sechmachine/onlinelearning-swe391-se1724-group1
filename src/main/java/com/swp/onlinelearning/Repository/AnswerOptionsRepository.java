package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.AnswerOptions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AnswerOptionsRepository extends JpaRepository<AnswerOptions, Integer> {

    @Query(value = "SELECT * from answeroptions where QuestionID = ?", nativeQuery = true)
    List<AnswerOptions> listAnswerOptionsByQuestionsId(int id);

    @Query(value = "SELECT * from answeroptions", nativeQuery = true)
    List<AnswerOptions> getAllAnswer();

    @Query(value = "SELECT * from answeroptions where QuestionID = ?", nativeQuery = true)
    List<AnswerOptions> listAnswerOptionsByQuestionId(int id);
}
