package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.AnsweredQuestions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AnsweredQuestionsRepository extends JpaRepository<AnsweredQuestions, Integer> {

    @Query(value = "SELECT * FROM  answeredquestions WHERE QuizHistoryID = ?", nativeQuery = true)
    List<AnsweredQuestions> getAllByQuizHistoryId(int id);

}
