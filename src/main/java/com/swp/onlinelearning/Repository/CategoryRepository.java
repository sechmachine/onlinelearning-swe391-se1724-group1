package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.SubjectCategories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<SubjectCategories, Integer> {
    @Query(value = "SELECT COUNT(*) FROM subjects WHERE CategoryID = ?", nativeQuery = true)
    int countSubjectsByCategory(int id);

    @Query(value = "SELECT COUNT(*) FROM subjectcategories where Name LIKE ?", nativeQuery = true)
    int countAllByRequest(String search);

    @Query(value = "SELECT COUNT(*) FROM subjectcategories", nativeQuery = true)
    int countAllCategories();

    @Query(value = "SELECT * FROM subjectcategories where Name LIKE ? limit 15 offset ?",nativeQuery = true)
    List<SubjectCategories> getAllByRequest(String search, int page);

    @Query(value = "SELECT * FROM subjectcategories limit 15 offset ?",nativeQuery = true)
    List<SubjectCategories> getAllCategories(int page);
}

