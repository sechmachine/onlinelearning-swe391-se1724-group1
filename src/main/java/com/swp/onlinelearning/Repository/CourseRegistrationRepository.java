package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.Registrations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CourseRegistrationRepository extends JpaRepository<Registrations, Integer> {
    @Query(value = "SELECT * FROM registrations WHERE UserID = ? AND Status = 'Enrolled'", nativeQuery = true)
    List<Registrations> findAllByUserId(int userId);

    @Query(value = "SELECT COUNT(*) FROM registrations WHERE UserID = ? AND SubjectID = ?", nativeQuery = true)
    int countRegistrations(int userId, int subjectId);

    @Query(value = "SELECT * FROM registrations WHERE UserID = ? AND SubjectID = ?", nativeQuery = true)
    Registrations getRegistration(Integer id, int subjectId);
}
