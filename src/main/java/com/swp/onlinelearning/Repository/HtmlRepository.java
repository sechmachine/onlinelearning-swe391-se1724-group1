package com.swp.onlinelearning.Repository;

public interface HtmlRepository {
    String markdownToHtml(String html);
}
