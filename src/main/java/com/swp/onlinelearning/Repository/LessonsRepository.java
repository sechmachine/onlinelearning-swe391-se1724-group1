package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.Lessons;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lessons, Integer> {
    @Query(value = "SELECT * FROM lessons WHERE SubjectID = ?", nativeQuery = true)
    List<Lessons> listLessonsBySubjectId(int id);

    @Query(value = "SELECT * FROM lessons WHERE QuizID = ?", nativeQuery = true)
    Lessons getByQuizId(int quizId);

    @Query(value = "SELECT COUNT(*) FROM lessons WHERE SubjectID = ?", nativeQuery = true)
    int countLessonsBySubjectsId(int id);
}
