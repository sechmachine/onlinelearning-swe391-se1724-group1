package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.PricePackages;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PricePackageRepository extends JpaRepository<PricePackages, Integer> {

}
