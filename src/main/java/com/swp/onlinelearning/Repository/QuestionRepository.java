package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.Questions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Questions, Integer> {

    @Query(value = "SELECT * from questions where id = ?", nativeQuery = true)
    Questions getQuestionsById(int id);

    @Query(value = "SELECT * FROM  questions", nativeQuery = true)
    List<Questions> getAllQuestions();

    @Query(value = "SELECT * FROM questions WHERE SubjectID = ? AND LessonID = ?", nativeQuery = true)
    List<Questions> listQuestionsBySubjectIdAndLessonId(int subjectId, int lessonId);

    @Query(value = "SELECT * FROM questions WHERE SubjectID = ? AND id NOT IN (SELECT QuestionID FROM quizquestions WHERE QuizID = ?)", nativeQuery = true)
    List<Questions> listAllBySubjectIdNotInQuiz(int subjectId, int quizId);
}
