package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.QuizHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuizHistoryRepository extends JpaRepository<QuizHistory, Integer> {

    @Query(value = "SELECT * from quizhistory WHERE id = ?", nativeQuery = true)
    QuizHistory getQuizHistoryById(int id);

    @Query(value = "SELECT * from quizhistory where UserID = ? AND QuizID = ?", nativeQuery = true)
    List<QuizHistory> getQuizHistoryByUsers(int id, int quizId);

}
