package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.Questions;
import com.swp.onlinelearning.Models.QuizQuestions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuizQuestionsRepository extends JpaRepository<QuizQuestions, Integer> {
    @Query(value = "SELECT * FROM quizquestions WHERE QuizID = ?\n" + "LIMIT 1 OFFSET ?", nativeQuery = true)
    QuizQuestions getQuizQuestions(int id, int questionNumber);

    @Query(value = "SELECT * FROM quizquestions WHERE QuizID = ?\n" + "LIMIT 1 OFFSET 0", nativeQuery = true)
    QuizQuestions getQuizQuestions(int id);

    @Query(value = "SELECT * FROM quizquestions WHERE QuizID = ?", nativeQuery = true)
    List<QuizQuestions> listAllByQuizId(int id);

    List<QuizQuestions> getQuizQuestionsById(int id);

    @Query(value = "SELECT COUNT(*) FROM quizquestions join questions q on q.id = quizquestions.QuestionID WHERE QuizID = ?" +
            " AND q.Content LIKE ?",nativeQuery = true)
    int countAllQuestionsByRequest(int id, String search);

    @Query(value = "SELECT COUNT(*) FROM quizquestions join questions q on q.id = quizquestions.QuestionID WHERE QuizID = ?",nativeQuery = true)
    int countAllQuestionsByRequest(int id);

    @Query(value = "SELECT quizquestions.id, QuizID, QuestionID, SubjectID, LessonID, Dimension, Level, Status, Content, Media, Explanation" +
            " FROM quizquestions join questions q on q.id = quizquestions.QuestionID WHERE QuizID = ?" +
            " AND q.Content LIKE ? limit 15 offset ?",nativeQuery = true)
    List<QuizQuestions> findAllByRequest(int id, String search, int page);

    @Query(value = "SELECT quizquestions.id, QuizID, QuestionID, SubjectID, LessonID, Dimension, Level, Status, Content, Media, Explanation FROM quizquestions join questions q on q.id = quizquestions.QuestionID WHERE QuizID = ?" +
            " limit 15 offset ?",nativeQuery = true)
    List<QuizQuestions> findAllQuestions(int id, int page);

}
