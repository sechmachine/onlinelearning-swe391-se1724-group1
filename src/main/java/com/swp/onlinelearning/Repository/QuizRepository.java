package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.Quizzes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuizRepository extends JpaRepository<Quizzes, Integer> {
    @Query(value = "SELECT COUNT(*) FROM quizquestions WHERE QuizID = ?", nativeQuery = true)
    int countQuizQuestions(int id);

    @Query(value = "SELECT COUNT(*) FROM quizzes WHERE Name LIKE ?",nativeQuery = true)
    int countAllQuizByRequest(String search);

    @Query(value = "SELECT COUNT(*) FROM quizzes",nativeQuery = true)
    int countAllQuiz();

    @Query(value = "SELECT * from quizzes where Name LIKE ? limit 15 offset ?", nativeQuery = true)
    List<Quizzes> findAllByRequest(String search, int page);

    @Query(value = "SELECT * from quizzes limit 15 offset ?", nativeQuery = true)
    List<Quizzes> findAllQuiz(int page);
}
