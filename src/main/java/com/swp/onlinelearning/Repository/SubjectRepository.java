package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.Subjects;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subjects, Integer> {

    @Query(value = "SELECT * FROM subjects ORDER BY DateCreated DESC limit 9 offset ?", nativeQuery = true)
    List<Subjects> findAllOrderByDateCreated(int page);

    @Query(value = "SELECT s.*, COUNT(r.UserID) AS EnrolledCount FROM subjects s LEFT JOIN registrations r ON s.id = r.SubjectID AND r.Status = 'Enrolled' GROUP BY s.id ORDER BY EnrolledCount DESC LIMIT 9 OFFSET ?", nativeQuery = true)
    List<Subjects> findAllOrderByPopular(int page);

    @Query(value = "SELECT * FROM subjects WHERE Status = 'Unpublished' limit 9 offset ?", nativeQuery = true)
    List<Subjects> findAllUnpublished(int page);

    @Query(value = "SELECT COUNT(*) FROM subjects WHERE Status = 'Unpublished'", nativeQuery = true)
    int subjectCountUnpublished();

    @Query(value = "SELECT * FROM subjects WHERE Status = 'Published'" +
            "limit 9 offset ?", nativeQuery = true)
    List<Subjects> findAllByPage(int page);

    @Query(value = "SELECT COUNT(*) FROM subjects", nativeQuery = true)
    int subjectCount();

    @Query(value = "SELECT * FROM subjects " +
            "limit 15 offset ?", nativeQuery = true)
    List<Subjects> findAllSubjectsByPage(int page);

    @Query(value = "SELECT COUNT(*) FROM subjects", nativeQuery = true)
    int countAllSubject();

    @Query(value = """
            SELECT COUNT(*)
            FROM subjects s JOIN users u on u.id = s.OwnerId
            JOIN subjectcategories c on c.id = s.CategoryID
            WHERE s.Name LIKE ?1
            OR u.FullName LIKE ?2
            OR c.Name LIKE ?3
            """, nativeQuery = true)
    int subjectSearchCount(String search1, String search2, String search3);

    @Query(value = "SELECT s.Name, s.id, s.CategoryID, s.ThumbnailImage, s.OwnerId, s.Status, s.Dimension, s.Description, s.Duration, s.DateCreated, s.Tier " +
            " FROM subjects s JOIN users u on u.id = s.OwnerId" +
            " JOIN subjectcategories c on c.id = s.CategoryID" +
            " WHERE s.Name LIKE ?1" +
            " OR u.FullName LIKE ?2" +
            " OR c.Name LIKE ?3" +
            " limit 15 offset ?4", nativeQuery = true)
    List<Subjects> getAllSubjectsByRequest(String search1, String search2, String search3, int page);

    @Query(value = """
            SELECT s.Name, s.id, s.CategoryID, s.ThumbnailImage, s.OwnerId, s.Status, s.Dimension, s.Description, s.Duration, s.DateCreated, s.Tier
            FROM subjects s JOIN users u on u.id = s.OwnerId
            JOIN subjectcategories c on c.id = s.CategoryID
            WHERE s.Name LIKE ?1
            OR u.FullName LIKE ?2
            OR c.Name LIKE ?3
            limit 9 offset ?4""", nativeQuery = true)
    List<Subjects> getSubjectsByRequest(String search1, String search2, String search3, int page);

    @Query(value = "SELECT COUNT(*) FROM subjects WHERE CategoryID = ?", nativeQuery = true)
    int subjectCountByCategory(int id);

    @Query(value = "SELECT * FROM subjects WHERE CategoryID = ?1 limit 9 offset ?2", nativeQuery = true)
    List<Subjects> getSubjectsByCategory(int id, int page);

    @Query(value = "SELECT * FROM subjects WHERE Tier = 'Premium'\n" +
            "order by DateCreated desc limit 5 offset 0", nativeQuery = true)
    List<Subjects> getSubjectsBySuggest();

    @Query(value = "SELECT COUNT(*) FROM subjects WHERE Tier = 'Free'", nativeQuery = true)
    int freeSubjectCount();

    @Query(value = "SELECT * FROM subjects WHERE Tier = 'Free' limit 9 offset ?", nativeQuery = true)
    List<Subjects> getFreeSubjects(int page);

    @Query(value = "SELECT * FROM subjects where CategoryID = ?", nativeQuery = true)
    List<Subjects> listSubjectsByCategory(int id);

    @Query(value = "SELECT COUNT(*) FROM registrations WHERE SubjectID = ? AND Status = 'Enrolled'", nativeQuery = true)
    int countStudentEnrolled(int subjectId);

    @Query(value = "SELECT * FROM subjects where OwnerId = ? ORDER BY DateCreated DESC ", nativeQuery = true)
    List<Subjects> listSubjectsByCreator(int id);

}
