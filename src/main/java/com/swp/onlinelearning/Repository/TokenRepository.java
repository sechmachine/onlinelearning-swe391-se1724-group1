package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.PremiumTokens;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<PremiumTokens, Integer> {
    @Query(value = "SELECT * FROM premiumtokens WHERE Token = ? and Uses = 1 LIMIT 1", nativeQuery = true)
    Optional<PremiumTokens> checkToken(String token);

    @Query(value = "SELECT * FROM premiumtokens WHERE id = ? and Uses = 0", nativeQuery = true)
    PremiumTokens getUsedToken(int id);

    @Query(value = "SELECT * FROM premiumtokens order by Uses desc ", nativeQuery = true)
    List<PremiumTokens> listAllTokens();

    @Query(value = "SELECT * FROM premiumtokens order by Uses desc limit 15 offset ?", nativeQuery = true)
    List<PremiumTokens> listAllTokensByPage(int page);

    @Query(value = "SELECT COUNT(*) FROM premiumtokens", nativeQuery = true)
    int countAllTokens();
}
