package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.Notifications;
import com.swp.onlinelearning.Models.UserNotifications;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserNotificationRepository extends JpaRepository<UserNotifications, Integer> {

    @Query(value = "SELECT * from usernotifications where UserId = ? order by time desc", nativeQuery = true)
    List<UserNotifications> getAllByUser(int userId);

    @Modifying
    @Query(value = "update usernotifications set Status = 'Read' where UserId = ?", nativeQuery = true)
    void updateAllByUserId(int userId);

    @Modifying
    @Query(value = "update usernotifications set Status = 'Read' where UserId = ? and NotificationId = ?", nativeQuery = true)
    void update(int userId, int notificationId);
}
