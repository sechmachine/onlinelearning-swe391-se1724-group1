package com.swp.onlinelearning.Repository;

import com.swp.onlinelearning.Models.Subjects;
import com.swp.onlinelearning.Models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {
    Optional<Users> findByEmail(String email);

    Users getUserByEmail(String email);

    Users findByPasswordResetToken(String token);

    @Query(value = "SELECT COUNT(*) FROM users ",nativeQuery = true)
    int countAllUsers();

    @Query(value = "SELECT COUNT(*) from users where FullName LIKE ?", nativeQuery = true)
    int countUserByName(String search);

    @Query(value = "SELECT COUNT(*) from users where Role = 'Common'", nativeQuery = true)
    int countCommonMembers();

    @Query(value = "SELECT COUNT(*) from users where Role = 'Premium'", nativeQuery = true)
    int countPremiumMembers();

    @Query(value = "SELECT * FROM users where FullName LIKE %?% OR Email LIKE %?% limit 15 offset ?",nativeQuery = true)
    List<Users> getUsersByName(String search,String search2 , int page);

    @Query(value = "SELECT * FROM users\n" +
            "limit 15 offset ?", nativeQuery = true)
    List<Users> findAllUsersByPage(int page);

    @Query(value = "SELECT * from users where Role = 'Premium' and PremiumStartDate >= ?", nativeQuery = true)
    List<Users> getPremiumByWeek(String date);

    @Query(value = "SELECT count(*) from users where Role = 'Premium' and PremiumStartDate >= ?", nativeQuery = true)
    int countPremiumByWeeks(String date);
}
