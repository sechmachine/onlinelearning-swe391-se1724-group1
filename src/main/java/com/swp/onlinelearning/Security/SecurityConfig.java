package com.swp.onlinelearning.Security;

import com.swp.onlinelearning.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.concurrent.TimeUnit;

/**
 * The SecurityConfig class configures the security settings for the online learning software.
 * It uses a SecurityServices and a UserService to configure authentication and authorization.
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {


    /**
     * The SecurityServices used to encode and verify passwords.
     */
    private final SecurityServices securityServices;

    /**
     * The UserService used to retrieve information about users.
     */
    private final UserService userService;

    /**
     * Configures the security filter chain for the online learning software.
     *
     * @param http the HttpSecurity object used to configure security settings
     * @return the configured SecurityFilterChain object
     * @throws Exception if an error occurs while configuring the security filter chain
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeHttpRequests(requests -> {
            try {
                requests.requestMatchers("/admin/**")
                        .hasAuthority(UserRole.Admin.name())
                        .requestMatchers("/premium/**").hasAnyAuthority(UserRole.Premium.name(), UserRole.Admin.name(), UserRole.TestContentCreator.name(), UserRole.CourseContentCreator.name())
                        .requestMatchers("/course-content-creator/**").hasAnyAuthority(UserRole.CourseContentCreator.name(), UserRole.Admin.name(), UserRole.TestContentCreator.name())
                        .requestMatchers("/test-content-creator/**").hasAnyAuthority(UserRole.TestContentCreator.name(), UserRole.Admin.name(), UserRole.CourseContentCreator.name())
                        .requestMatchers("/resources/**", "/**").permitAll().anyRequest().authenticated();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        http.formLogin().loginPage("/login").loginProcessingUrl("/login").failureUrl("/login?error").permitAll().defaultSuccessUrl("/main", true).passwordParameter("password").usernameParameter("email").and().rememberMe().tokenValiditySeconds((int) TimeUnit.SECONDS.toSeconds(20)).key("nevergonnagiveyouup").rememberMeParameter("remember-me").and().logout().logoutUrl("/logout").logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET")).permitAll();

        http.authenticationProvider(daoAuthenticationProvider());
        http.httpBasic();
        return http.build();
    }

    /**
     * Creates a DaoAuthenticationProvider object for authenticating users.
     *
     * @return the created DaoAuthenticationProvider object
     */
    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(securityServices.passwordEncoder());
        provider.setUserDetailsService(userService);
        return provider;
    }

    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedDoubleSlash(true);
        return firewall;
    }
}