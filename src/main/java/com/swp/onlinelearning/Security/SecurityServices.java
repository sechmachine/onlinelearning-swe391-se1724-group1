package com.swp.onlinelearning.Security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * The SecurityServices class provides security-related services.
 * It provides a method for creating a password encoder.
 */
@Service
public class SecurityServices {

    /**
     * Creates a PasswordEncoder object for encoding and verifying passwords.
     *
     * @return the created PasswordEncoder object
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }
}
