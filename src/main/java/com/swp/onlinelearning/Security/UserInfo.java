package com.swp.onlinelearning.Security;

import com.swp.onlinelearning.Models.Users;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserInfo implements UserDetails {
    private final Users user;

    public UserInfo(Users user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    public String getMobile() {
        return user.getMobile();
    }

    public void setMobile(String mobile) {
        this.user.setMobile(mobile);
    }

    public String getFullName() {
        return user.getFullName();
    }

    public void setFullName(String fullName) {
        this.user.setFullName(fullName);
    }

    public String getEmail() {
        return user.getEmail();
    }

    public void setEmail(String email) {
        this.user.setEmail(email);
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

}
