package com.swp.onlinelearning.Security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * UserRole is an enumeration of the different roles a user can have.
 * Each role is associated with a set of UserPermissions.
 */
@RequiredArgsConstructor
public enum UserRole {
    Common(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.USER_PROFILE_UPDATE, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.SUBJECT_VIEW, UserPermission.COURSE_VIEW, UserPermission.COURSE_REGISTER, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.QUIZ_VIEW, UserPermission.QUESTION_VIEW, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.QUIZQUESTION_VIEW, UserPermission.QUIZHISTORY_VIEW)),
    Premium(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.USER_PROFILE_UPDATE, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.SUBJECT_VIEW, UserPermission.COURSE_VIEW, UserPermission.COURSE_REGISTER, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.QUIZ_VIEW, UserPermission.QUESTION_VIEW, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.QUIZQUESTION_VIEW, UserPermission.QUIZHISTORY_VIEW)),
    CourseContentCreator(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.USER_PROFILE_UPDATE, UserPermission.COURSE_VIEW, UserPermission.COURSE_CREATE, UserPermission.COURSE_UPDATE, UserPermission.COURSE_DELETE, UserPermission.COURSE_REGISTER, UserPermission.SUBJECT_CREATE, UserPermission.SUBJECT_UPDATE, UserPermission.SUBJECT_DELETE, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CATEGORY_CREATE, UserPermission.SUBJECT_CATEGORY_UPDATE, UserPermission.SUBJECT_CATEGORY_DELETE, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.LESSON_CREATE, UserPermission.LESSON_UPDATE, UserPermission.LESSON_DELETE, UserPermission.QUIZ_VIEW, UserPermission.QUESTION_VIEW, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.QUIZQUESTION_VIEW, UserPermission.QUIZHISTORY_VIEW)),
    TestContentCreator(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.USER_PROFILE_UPDATE, UserPermission.COURSE_VIEW, UserPermission.COURSE_REGISTER, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.PRICEPACKAGE_VIEW, UserPermission.REGISTRATION_VIEW, UserPermission.LESSON_VIEW, UserPermission.QUIZ_VIEW, UserPermission.QUIZ_CREATE, UserPermission.QUIZ_UPDATE, UserPermission.QUIZ_DELETE, UserPermission.QUESTION_VIEW, UserPermission.QUESTION_CREATE, UserPermission.QUESTION_UPDATE, UserPermission.QUESTION_DELETE, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.ANSWEROPTIONS_CREATE, UserPermission.ANSWEROPTIONS_UPDATE, UserPermission.ANSWEROPTIONS_DELETE, UserPermission.QUIZQUESTION_CREATE, UserPermission.QUIZQUESTION_UPDATE, UserPermission.QUIZQUESTION_DELETE, UserPermission.QUIZQUESTION_VIEW, UserPermission.QUIZHISTORY_VIEW)),
    Admin(newHashSet(UserPermission.USER_PROFILE_READ, UserPermission.USER_PROFILE_WRITE, UserPermission.USER_PROFILE_UPDATE, UserPermission.USER_PROFILE_DELETE, UserPermission.COURSE_REGISTER, UserPermission.COURSE_VIEW, UserPermission.COURSE_CREATE, UserPermission.COURSE_UPDATE, UserPermission.COURSE_DELETE, UserPermission.LESSON_VIEW, UserPermission.LESSON_CREATE, UserPermission.LESSON_UPDATE, UserPermission.LESSON_DELETE, UserPermission.SUBJECT_VIEW, UserPermission.SUBJECT_CREATE, UserPermission.SUBJECT_UPDATE, UserPermission.SUBJECT_DELETE, UserPermission.SUBJECT_CATEGORY_VIEW, UserPermission.SUBJECT_CATEGORY_CREATE, UserPermission.SUBJECT_CATEGORY_UPDATE, UserPermission.SUBJECT_CATEGORY_DELETE, UserPermission.PRICEPACKAGE_VIEW, UserPermission.PRICEPACKAGE_CREATE, UserPermission.PRICEPACKAGE_UPDATE, UserPermission.PRICEPACKAGE_DELETE, UserPermission.QUIZ_CREATE, UserPermission.QUIZ_VIEW, UserPermission.QUIZ_UPDATE, UserPermission.QUIZ_DELETE, UserPermission.QUESTION_VIEW, UserPermission.QUESTION_CREATE, UserPermission.QUESTION_UPDATE, UserPermission.QUESTION_DELETE, UserPermission.QUIZQUESTION_CREATE, UserPermission.QUIZQUESTION_VIEW, UserPermission.QUIZQUESTION_UPDATE, UserPermission.QUIZQUESTION_DELETE, UserPermission.ANSWEROPTIONS_VIEW, UserPermission.ANSWEROPTIONS_CREATE, UserPermission.ANSWEROPTIONS_UPDATE, UserPermission.ANSWEROPTIONS_DELETE, UserPermission.REGISTRATION_CREATE, UserPermission.REGISTRATION_VIEW, UserPermission.REGISTRATION_UPDATE, UserPermission.REGISTRATION_DELETE, UserPermission.QUIZHISTORY_VIEW, UserPermission.QUESTION_UPDATE, UserPermission.QUIZHISTORY_CREATE, UserPermission.ANSWEROPTIONS_WRITE, UserPermission.QUIZHISTORY_DELETE));

    @Getter
    private final Set<UserPermission> permissions;


    /**
     * Creates a new HashSet containing the given objects.
     *
     * @param objs the objects to add to the HashSet
     * @return a new HashSet containing the given objects
     */
    @SafeVarargs
    public static <T> Set<T> newHashSet(T... objs) {
        Set<T> set = new HashSet<>();
        Collections.addAll(set, objs);
        return set;
    }


    /**
     * Returns the set of granted authorities associated with this UserRole.
     *
     * @return the list of granted authorities
     */
    public List<SimpleGrantedAuthority> getAuthorities() {
        var authorities = getPermissions().stream().map(permission -> new SimpleGrantedAuthority(permission.name())).collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority(this.name()));
        return authorities;
    }
}

