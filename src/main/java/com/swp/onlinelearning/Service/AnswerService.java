package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.AnswerOptions;
import com.swp.onlinelearning.Repository.AnswerOptionsRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class AnswerService {
    @Autowired
    private AnswerOptionsRepository answerRepo;

    public AnswerOptions get(int id) {
        if (answerRepo.findById(id).isPresent()) {
            return answerRepo.findById(id).get();
        } else {
            return null;
        }
    }

    public void save(AnswerOptions answerOptions) {
        answerRepo.save(answerOptions);
    }

    public void delete(int id) {
        answerRepo.deleteById(id);
    }

    public List<AnswerOptions> listAnswerOptionsByQuestionId(int id) {
        if (answerRepo.listAnswerOptionsByQuestionId(id).isEmpty()) {
            return null;
        } else {
            return answerRepo.listAnswerOptionsByQuestionId(id);
        }
    }
}
