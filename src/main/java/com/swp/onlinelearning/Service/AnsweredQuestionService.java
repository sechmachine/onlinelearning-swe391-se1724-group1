package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.AnsweredQuestions;
import com.swp.onlinelearning.Repository.AnsweredQuestionsRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class AnsweredQuestionService {
    @Autowired
    private AnsweredQuestionsRepository aqRepo;

    public void save(AnsweredQuestions answeredQuestions) {
        aqRepo.save(answeredQuestions);
    }

    public List<AnsweredQuestions> getAnsweredQuestions(int id) {
        if (aqRepo.getAllByQuizHistoryId(id).isEmpty()) {
            return null;
        } else {
            return aqRepo.getAllByQuizHistoryId(id);
        }
    }

}
