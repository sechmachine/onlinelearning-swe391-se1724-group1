package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Quizzes;
import com.swp.onlinelearning.Models.SubjectCategories;
import com.swp.onlinelearning.Repository.CategoryRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepo;

    public List<SubjectCategories> listAll() {
        if (categoryRepo.findAll().isEmpty()) {
            return null;
        } else {
            return categoryRepo.findAll();
        }
    }

    public void save(SubjectCategories categories) {
        categoryRepo.save(categories);
    }

    public SubjectCategories get(int id) {
        Optional<SubjectCategories> category = categoryRepo.findById(id);
        return category.orElse(null);
    }

    public void delete(int id) {
        categoryRepo.deleteById(id);
    }

    public int countSubjectsByCategory(int id) {
        return categoryRepo.countSubjectsByCategory(id);
    }

    public Map<Integer, Integer> getSubjectCounts() {
        List<SubjectCategories> listSubjectCategories = listAll();
        Map<Integer, Integer> subjectCounts = new HashMap<>();
        for (SubjectCategories category : listSubjectCategories) {
            int count = countSubjectsByCategory(category.getId());
            subjectCounts.put(category.getId(), count);
        }
        return subjectCounts;
    }

    public List<SubjectCategories> getSubjectCategoriesList(int page) {
        if (categoryRepo.getAllCategories(page).isEmpty()) {
            return null;
        } else {
            return categoryRepo.getAllCategories(page);
        }
    }

    public int getTotalPage() {
        int total = categoryRepo.countAllCategories();
        int countPage = total/15;
        if (total % 15 != 0){
            countPage++;
        }
        return countPage;
    }

    public int getCategory(){
        return categoryRepo.countAllCategories();
    }

    public int getTotalSearchPage(String search) {
        int total = categoryRepo.countAllByRequest(search);
        int countPage = total/15;
        if (total % 15 != 0){
            countPage++;
        }
        return countPage;
    }

    public List<SubjectCategories> getSubjectCategoriesListByRequest(String search, int page) {
        if (categoryRepo.getAllByRequest(search, page).isEmpty()) {
            return null;
        } else {
            return categoryRepo.getAllByRequest(search, page);
        }
    }
}
