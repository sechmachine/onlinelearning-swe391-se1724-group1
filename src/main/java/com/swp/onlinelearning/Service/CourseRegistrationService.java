package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Registrations;
import com.swp.onlinelearning.Repository.CourseRegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional

public class CourseRegistrationService {
    @Autowired
    private CourseRegistrationRepository registrationRepository;

    public List<Registrations> listAll() {
        return registrationRepository.findAll();
    }

    public List<Registrations> listAllByUserId(int userId) {
        return registrationRepository.findAllByUserId(userId);
    }

    public void save(Registrations registrations) {
        registrationRepository.save(registrations);
    }

    public Registrations get(int id) {
        Optional<Registrations> registrations = registrationRepository.findById(id);
        return registrations.orElse(null);
    }


    public void delete(int id) {
        registrationRepository.deleteById(id);
    }

    public boolean checkRegistration(int userId, int subjectId) {
        int count = registrationRepository.countRegistrations(userId, subjectId);
        return count != 0;
    }

    public Registrations getRegistration(Integer id, int subjectId) {
        return registrationRepository.getRegistration(id, subjectId);
    }

}
