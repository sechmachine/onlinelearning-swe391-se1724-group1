package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Lessons;
import com.swp.onlinelearning.Models.SubjectCategories;
import com.swp.onlinelearning.Repository.LessonsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class LessonsService {
    @Autowired
    private LessonsRepository repo;

    public Lessons get(int id) {
        Optional<Lessons> lessons = repo.findById(id);
        return lessons.orElse(null);
    }

    public List<Lessons> listLessonsBySubjectId(int id) {
        if (repo.listLessonsBySubjectId(id).isEmpty()) {
            return Collections.emptyList();
        } else {
            return repo.listLessonsBySubjectId(id);
        }
    }

    public int countLessonsBySubjectsId(int id) {
        return repo.countLessonsBySubjectsId(id);
    }

    public Map<Integer, Integer> getLessonCounts() {
        List<Lessons> lessonsList = listAll();
        Map<Integer, Integer> lessonCounts = new HashMap<>();
        for (Lessons lessons : lessonsList) {
            int count = countLessonsBySubjectsId(lessons.getId());
            lessonCounts.put(lessons.getId(), count);
        }
        return lessonCounts;
    }

    public List<Lessons> listAll() {
        return repo.findAll();
    }

    public void save(Lessons lessons) {
        repo.save(lessons);
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }

    public Lessons getByQuizId(int quizId) {
        return repo.getByQuizId(quizId);
    }
}