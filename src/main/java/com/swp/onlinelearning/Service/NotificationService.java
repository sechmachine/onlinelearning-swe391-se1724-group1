package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Notifications;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Repository.NotificationRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class NotificationService {
    @Autowired
    private NotificationRepository notificationRepository;

    public Notifications get(int id) {
        return notificationRepository.getReferenceById(id);
    }


    public void delete(Notifications notifications) {
        notificationRepository.delete(notifications);
    }

    public void update(Notifications notifications) {
        notificationRepository.save(notifications);
    }
}
