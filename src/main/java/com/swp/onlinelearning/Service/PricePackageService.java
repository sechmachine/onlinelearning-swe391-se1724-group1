package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.PricePackages;
import com.swp.onlinelearning.Repository.PricePackageRepository;
import com.swp.onlinelearning.Repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PricePackageService {
    @Autowired
    private PricePackageRepository pricePackageRepo;

    @Autowired
    private TokenRepository tokenRepo;

    public List<PricePackages> listAll() {
        return pricePackageRepo.findAll();
    }

    public void save(PricePackages pricePackages) {
        pricePackageRepo.save(pricePackages);
    }

    public PricePackages get(int id) {
        Optional<PricePackages> pricePackages = pricePackageRepo.findById(id);
        return pricePackages.orElse(null);
    }

    public void delete(int id) {
        pricePackageRepo.deleteById(id);
    }


}
