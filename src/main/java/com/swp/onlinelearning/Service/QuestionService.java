package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Questions;
import com.swp.onlinelearning.Repository.QuestionRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class QuestionService {
    @Autowired
    private QuestionRepository quesRepo;

    public Questions get(int id) {
        if (quesRepo.getQuestionsById(id) != null) {
            return quesRepo.getQuestionsById(id);
        } else {
            return null;
        }
    }

    public List<Questions> listAll() {
        if (quesRepo.getAllQuestions().isEmpty()) {
            return null;
        } else {
            return quesRepo.getAllQuestions();
        }
    }

    public List<Questions> listQuestionsBySubjectIdAndLessonId(int subjectId, int lessonId) {
        if (quesRepo.listQuestionsBySubjectIdAndLessonId(subjectId, lessonId).isEmpty()) {
            return null;
        } else {
            return quesRepo.listQuestionsBySubjectIdAndLessonId(subjectId, lessonId);
        }
    }

    public void save(Questions questions) {
        quesRepo.save(questions);
    }

    public void delete(int id) {
        quesRepo.deleteById(id);
    }

    public List<Questions> listAllBySubjectIdNotInQuiz(int subjectId, int quizId) {
        if (quesRepo.listAllBySubjectIdNotInQuiz(subjectId, quizId).isEmpty()) {
            return null;
        } else {
            return quesRepo.listAllBySubjectIdNotInQuiz(subjectId, quizId);
        }
    }
}
