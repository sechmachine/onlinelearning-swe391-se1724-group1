package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.QuizHistory;
import com.swp.onlinelearning.Repository.QuizHistoryRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class QuizHistoryService {
    @Autowired
    private QuizHistoryRepository qhRepo;

    public void save(QuizHistory quizHistory) {
        qhRepo.save(quizHistory);
    }

    public QuizHistory getQuizHistory(int id) {
        if (qhRepo.getQuizHistoryById(id) != null) {
            return qhRepo.getQuizHistoryById(id);
        } else {
            return null;
        }
    }

    public List<QuizHistory> getQuizHistoryByUser(int id, int quizId) {
        if (qhRepo.getQuizHistoryByUsers(id, quizId).isEmpty()) {
            return null;
        } else {
            return qhRepo.getQuizHistoryByUsers(id, quizId);
        }
    }
}
