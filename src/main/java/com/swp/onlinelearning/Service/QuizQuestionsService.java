package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.QuizQuestions;
import com.swp.onlinelearning.Models.Quizzes;
import com.swp.onlinelearning.Repository.QuizQuestionsRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class QuizQuestionsService {
    @Autowired
    private QuizQuestionsRepository qqrepo;

    public QuizQuestions get(int id) {
        if (qqrepo.getQuizQuestions(id) != null) {
            return qqrepo.getQuizQuestions(id);
        } else {
            return null;
        }
    }

    public QuizQuestions get(int id, int page) {
        if (qqrepo.getQuizQuestions(id, page) != null) {
            return qqrepo.getQuizQuestions(id, page);
        } else {
            return null;
        }
    }

    public List<QuizQuestions> listAllByQuizId(int id) {
        if (qqrepo.listAllByQuizId(id).isEmpty()) {
            return null;
        } else {
            return qqrepo.listAllByQuizId(id);
        }
    }

    public List<QuizQuestions> getQuizList(int id, int page) {
        if (qqrepo.findAllQuestions(id, page).isEmpty()) {
            return null;
        } else {
            return qqrepo.findAllQuestions(id, page);
        }
    }

    public int getTotalPage(int id) {
        int total = qqrepo.countAllQuestionsByRequest(id);
        int countPage = total/15;
        if (total % 15 != 0){
            countPage++;
        }
        return countPage;
    }

    public int getTotalSearchPage(int id, String search) {
        int total = qqrepo.countAllQuestionsByRequest(id, search);
        int countPage = total/15;
        if (total % 15 != 0){
            countPage++;
        }
        return countPage;
    }

    public List<QuizQuestions> getQuizListByRequest(int id, String search, int page) {
        if (qqrepo.findAllByRequest(id, search, page).isEmpty()) {
            return null;
        } else {
            return qqrepo.findAllByRequest(id, search, page);
        }
    }

    public List<QuizQuestions> getQuizQuestionsById(int id) {
        if (qqrepo.getQuizQuestionsById(id).isEmpty()) {
            return null;
        } else {
            return qqrepo.getQuizQuestionsById(id);
        }
    }

    public List<QuizQuestions> listAll() {
        return qqrepo.findAll();
    }


    public void save(QuizQuestions quizQuestions) {
        qqrepo.save(quizQuestions);
    }

    public QuizQuestions get(Integer id) {
        Optional<QuizQuestions> result = qqrepo.findById(id);
        return result.orElse(null);
    }

    public void delete(Integer id) {
        qqrepo.deleteById(id);
    }

}
