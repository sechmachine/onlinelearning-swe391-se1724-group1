package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Quizzes;
import com.swp.onlinelearning.Repository.QuizRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class QuizService {
    @Autowired
    private QuizRepository repo;

    public List<Quizzes> listAll() {
        return repo.findAll();
    }

    public void save(Quizzes registrations) {
        repo.save(registrations);
    }

    public Quizzes get(int id) {
        Optional<Quizzes> result = repo.findById(id);
        return result.orElse(null);
    }

    public void delete(int id) {
        repo.deleteById(id);
    }

    public void addQuiz(Quizzes quiz) {
        if (quiz.getPassCondition().isNaN()) {
            quiz.setPassCondition(0.00);
        }
        repo.save(quiz);
    }

    public List<Quizzes> getQuizList(int page) {
        if (repo.findAllQuiz(page).isEmpty()) {
            return null;
        } else {
            return repo.findAllQuiz(page);
        }
    }

    public int getTotalPage() {
        int total = repo.countAllQuiz();
        int countPage = total / 15;
        if (total % 15 != 0) {
            countPage++;
        }
        return countPage;
    }

    public int getTotalSearchPage(String search) {
        int total = repo.countAllQuizByRequest(search);
        int countPage = total / 15;
        if (total % 15 != 0) {
            countPage++;
        }
        return countPage;
    }

    public List<Quizzes> getQuizListByRequest(String search, int page) {
        if (repo.findAllByRequest(search, page).isEmpty()) {
            return null;
        } else {
            return repo.findAllByRequest(search, page);
        }
    }

    public int countQuizQuestions(int id) {
        return repo.countQuizQuestions(id);
    }

    public Map<Integer, Integer> getQuizQuestionCounts() {
        List<Quizzes> listQuizzes = listAll();
        Map<Integer, Integer> quizQuestionCounts = new HashMap<>();
        for (Quizzes quiz : listQuizzes) {
            int count = countQuizQuestions(quiz.getId());
            quizQuestionCounts.put(quiz.getId(), count);
        }
        return quizQuestionCounts;
    }

}

