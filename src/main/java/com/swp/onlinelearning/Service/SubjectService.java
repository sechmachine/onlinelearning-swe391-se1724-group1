package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Subjects;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Repository.SubjectRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
@Transactional
public class SubjectService {
    @Autowired
    private SubjectRepository subjectRepo;

    public List<Subjects> listAll() {
        if (subjectRepo.findAll().isEmpty()) {
            return null;
        } else {
            return subjectRepo.findAll();
        }
    }

    public List<Subjects> listSuggest() {
        if (subjectRepo.getSubjectsBySuggest().isEmpty()) {
            return null;
        } else {
            return subjectRepo.getSubjectsBySuggest();
        }
    }

    public List<Subjects> listByPage(int page) {
        if (subjectRepo.findAllByPage(page).isEmpty()) {
            return null;
        } else {
            return subjectRepo.findAllByPage(page);
        }
    }

    public void save(Subjects subject) {
        subjectRepo.save(subject);
    }

    public List<Subjects> getAllSubjectByRequest(String search, int page) {
        if (subjectRepo.getAllSubjectsByRequest(search, search, search, page).isEmpty()) {
            return null;
        } else {
            return subjectRepo.getAllSubjectsByRequest(search, search, search, page);
        }
    }

    public List<Subjects> getAllSubjectByCreator(int id) {
        if (subjectRepo.listSubjectsByCreator(id).isEmpty()) {
            return null;
        } else {
            return subjectRepo.listSubjectsByCreator(id);
        }
    }


    public List<Subjects> getSubjectByRequest(String search, int page) {
        if (subjectRepo.getSubjectsByRequest(search, search, search, page).isEmpty()) {
            return null;
        } else {
            return subjectRepo.getSubjectsByRequest(search, search, search, page);
        }
    }

    public Subjects get(int id) {
        Optional<Subjects> result = subjectRepo.findById(id);
        return result.orElse(null);
    }

    public int getTotalPage() {
        int total = subjectRepo.subjectCount();
        int countPage = total/9;
        if (total % 9 != 0){
            countPage++;
        }
        return countPage;
    }

    public int getSubjectsTotalPage() {
        int total = subjectRepo.countAllSubject();
        int countPage = total/15;
        if (total % 15 != 0){
            countPage++;
        }
        return countPage;
    }

    public List<Subjects> getAllSubjectsByPage(int page) {
        if (subjectRepo.findAllSubjectsByPage(page).isEmpty()) {
            return null;
        } else {
            return subjectRepo.findAllSubjectsByPage(page);
        }
    }


    public int getTotalPageByCategory(int id) {
        int total = subjectRepo.subjectCountByCategory(id);
        int countPage = total/9;
        if (total % 9 != 0){
            countPage++;
        }
        return countPage;
    }

    public List<Subjects> getSubjectsByCategory(int id, int page) {
        if (subjectRepo.getSubjectsByCategory(id, page).isEmpty()) {
            return null;
        } else {
            return subjectRepo.getSubjectsByCategory(id, page);
        }
    }

    public int getTotalUnpublishedPage() {
        int total = subjectRepo.subjectCountUnpublished();
        int countPage = total/9;
        if (total % 9 != 0){
            countPage++;
        }
        return countPage;
    }

    public int getTotalSearchPage(String search) {
        int total = subjectRepo.subjectSearchCount(search, search, search);
        int countPage = total/9;
        if (total % 9 != 0){
            countPage++;
        }
        return countPage;
    }

    public int getTotalSearchPage2(String search) {
        int total = subjectRepo.subjectSearchCount(search, search, search);
        int countPage = total/15;
        if (total % 15 != 0){
            countPage++;
        }
        return countPage;
    }

    public int getTotalFreeSubjectPages() {
        int total = subjectRepo.freeSubjectCount();
        int countPage = total/9;
        if (total % 9 != 0){
            countPage++;
        }
        return countPage;
    }

    public List<Subjects> freeSubjects(int page) {
        return subjectRepo.getFreeSubjects(page);
    }

    public void delete(int id) {
        subjectRepo.deleteById(id);
    }


    public List<Subjects> orderByDateCreated(int page) {
        return subjectRepo.findAllOrderByDateCreated(page);
    }

    public List<Subjects> orderByEnrolledStudents(int page) {
        return subjectRepo.findAllOrderByPopular(page);
    }

    public List<Subjects> findAllUnpublished(int page) {
        return subjectRepo.findAllUnpublished(page);
    }

    public void addSubject(Subjects subject, Users owner) {
        LocalDate localDate = LocalDate.now();
        Instant instant = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);
        subject.setDateCreated(date);
        subject.setCreator(owner);
        subjectRepo.save(subject);
    }

    public List<Subjects> listAllSubjectsByCategory(int id) {
        if (subjectRepo.listSubjectsByCategory(id).isEmpty()) {
            return null;
        } else {
            return subjectRepo.listSubjectsByCategory(id);
        }
    }

    public int countStudentEnrolled(int id) {
        return subjectRepo.countStudentEnrolled(id);
    }

    public Map<Integer, Integer> getStudentEnrolledCounts() {
        List<Subjects> listSubjects = listAll();
        Map<Integer, Integer> enrolledCounts = new HashMap<>();
        for (Subjects subjects : listSubjects) {
            int count = countStudentEnrolled(subjects.getId());
            enrolledCounts.put(subjects.getId(), count);
        }
        return enrolledCounts;
    }

    public int getSubject(){
        return subjectRepo.countAllSubject();
    }
}
