package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.PremiumTokens;
import com.swp.onlinelearning.Models.PricePackages;
import com.swp.onlinelearning.Repository.TokenRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class TokenService {
    @Autowired
    private TokenRepository tokenRepo;

    public void generateToken(PricePackages pricePackage) {
        String token = java.util.UUID.randomUUID().toString().replace("-", "").substring(0, 32);
        PremiumTokens premiumToken = new PremiumTokens();
        premiumToken.setToken(token);
        premiumToken.setTokenCreationDate(LocalDate.now());
        premiumToken.setUses(1);
        premiumToken.setPricePackages(pricePackage);
        tokenRepo.save(premiumToken);
    }

    public List<PremiumTokens> getTokensList(int page) {
        if (tokenRepo.listAllTokensByPage(page).isEmpty()) {
            return null;
        } else {
            return tokenRepo.listAllTokensByPage(page);
        }
    }

    public int getTotalPage() {
        int total = tokenRepo.countAllTokens();
        int countPage = total / 15;
        if (total % 15 != 0) {
            countPage++;
        }
        return countPage;
    }

    public void save(PremiumTokens premiumToken) {
        tokenRepo.save(premiumToken);
    }

    public List<PremiumTokens> listAll() {
        if (tokenRepo.listAllTokens().isEmpty()) {
            return null;
        } else {
            return tokenRepo.listAllTokens();
        }
    }

    public PremiumTokens findToken(String token) {
        return tokenRepo.checkToken(token).orElse(null);
    }

    public void deleteUsedToken(int id) {
        tokenRepo.deleteById(tokenRepo.getUsedToken(id).getId());
    }
}
