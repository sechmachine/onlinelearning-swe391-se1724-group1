package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.Notifications;
import com.swp.onlinelearning.Models.UserNotifications;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Repository.UserNotificationRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class UserNotificationService {

    @Autowired
    private UserNotificationRepository repository;

    public void addNotification(Users user, Notifications notifications) {
        UserNotifications userNotifications = new UserNotifications();
        userNotifications.setNotifications(notifications);
        userNotifications.setUsers(user);
        userNotifications.setStatus(UserNotifications.Status.Unread);
        userNotifications.setTime(new Date());
        repository.save(userNotifications);
    }

    public void updateNotification(int userId, int id) {
        repository.update(userId, id);
    }

    public List<UserNotifications> getNotifications(int id) {
        if (!repository.getAllByUser(id).isEmpty()) {
            return repository.getAllByUser(id);
        } else {
            return null;
        }
    }

    public List<UserNotifications> getUnreadNotificationsList(List<UserNotifications> notifications) {
        List<UserNotifications> unreadNotificationsList = new ArrayList<>();
        for (UserNotifications userNotifications : notifications) {
            if (userNotifications.getStatus().toString().equals("Unread")) {
                unreadNotificationsList.add(userNotifications);
            }
        }
        return unreadNotificationsList;
    }

    public void updateAll(int id) {
        if (!repository.getAllByUser(id).isEmpty()) {
            repository.updateAllByUserId(id);
        }
    }
}
