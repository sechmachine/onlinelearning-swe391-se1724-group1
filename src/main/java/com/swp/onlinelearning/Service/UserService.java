package com.swp.onlinelearning.Service;

import com.swp.onlinelearning.Models.PremiumTokens;
import com.swp.onlinelearning.Models.Users;
import com.swp.onlinelearning.Repository.UserRepository;
import com.swp.onlinelearning.Security.UserRole;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Users getAuthenticatedUser(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String email = userDetails.getUsername();
        Optional<Users> users = userRepository.findByEmail(email);
        if (users.isEmpty()) {
            throw new NullPointerException("Can't get User !!!");
        }
        return users.get();
    }

    public void upgradePremium(String token, Users user) {
        PremiumTokens premiumToken = tokenService.findToken(token);
        if (premiumToken != null && (user.getRole().equals(UserRole.Common))) {
            user.setRole(UserRole.Premium);
            user.setPremiumStartDate(LocalDate.now());
            user.setPremiumExpirationDate(LocalDate.now().plusDays(premiumToken.getPricePackages().getAccessDuration()));
            premiumToken.setUses(premiumToken.getUses() - 1);
            userRepository.save(user);
            tokenService.save(premiumToken);

        }
    }

    public List<Users> listAll() {
        if (userRepository.findAll().isEmpty()) {
            return null;
        } else {
            return userRepository.findAll();
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Users> users = userRepository.findByEmail(username);
        return users.orElse(null);
    }

    public void registerUser(Users user) {
        user.setRole(UserRole.Common);
        user.setStatus(Users.Status.Active);
        user.setAvatar("/img/defaultAvatar.png");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public void adminAddUser(Users user) {
        user.setAvatar("/img/defaultAvatar.png");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public int getTotalPage() {
        int total = userRepository.countAllUsers();
        int countPage = total / 15;
        if (total % 15 != 0) {
            countPage++;
        }
        return countPage;
    }

    public List<Users> getUsersList(int page) {
        if (userRepository.findAllUsersByPage(page).isEmpty()) {
            return null;
        } else {
            return userRepository.findAllUsersByPage(page);
        }
    }

    public boolean emailExists(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    public void delete(int id) {
        userRepository.deleteById(id);
    }

    public Users get(int id) {
        Optional<Users> user = userRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new NullPointerException("Can't get User !!!");
        }
    }

    public void updatePasswordResetToken(String token, String email) throws UsernameNotFoundException {
        Users user = userRepository.getUserByEmail(email);
        if (user != null && user.getEmail().equals(email)) {
            user.setPasswordResetToken(token);
            userRepository.save(user);
        } else {
            throw new UsernameNotFoundException("Could not find any customer with the email " + email);
        }
    }

    public Users getByPasswordResetToken(String token) {
        return userRepository.findByPasswordResetToken(token);
    }

    public void updatePassword(String password, Users user) {
        user.setPassword(passwordEncoder.encode(password));
        user.setPasswordResetToken(null);
        userRepository.save(user);
    }

    public int getTotalUserSearchPage(String search) {
        String holder = "%" + search + "%";
        int total = userRepository.countUserByName(holder);
        int countPage = total / 15;
        if (total % 15 != 0) {
            countPage++;
        }
        return countPage;
    }

    public List<Users> getUserListByName(String search, int page) {
        if (userRepository.getUsersByName(search, search, page).isEmpty()) {
            return null;
        } else {
            return userRepository.getUsersByName(search, search, page);
        }
    }

    public void save(Users user) {
        userRepository.save(user);
    }

    public int getTotalCommon() {
        return userRepository.countCommonMembers();
    }

    public int getTotalPremium() {
        return userRepository.countPremiumMembers();
    }

    public List<Users> getPremiumByWeek() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        Date firstDayOfWeek = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(firstDayOfWeek);
        if (userRepository.getPremiumByWeek(formattedDate).isEmpty()) {
            return null;
        } else {
            return userRepository.getPremiumByWeek(formattedDate);
        }
    }

    public int countPremiumByWeek(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        Date firstDayOfWeek = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(firstDayOfWeek);
        return userRepository.countPremiumByWeeks(formattedDate);
    }
}
