<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>OLS | FPT Online Learning System</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="
    ../../../img/hopeUI/favicon.ico"/>

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="
    ../../../css/hopeUI/core/libs.min.css"/>


    <!-- Hope Ui Design System Css -->
    <link rel="stylesheet" href="
    ../../../css/hopeUI/hope-ui.min.css?v=2.0.0"/>

    <!-- Custom Css -->
    <link rel="stylesheet" href="
    ../../../css/hopeUI/custom.min.css?v=2.0.0"/>

    <!-- Dark Css -->
    <link rel="stylesheet" href="
    ../../../css/hopeUI/dark.min.css"/>

    <!-- Customizer Css -->
    <link rel="stylesheet" href="
    ../../../css/hopeUI/customizer.min.css"/>

    <!-- RTL Css -->
    <link rel="stylesheet" href="
    ../../../css/hopeUI/rtl.min.css"/>

    <link rel="stylesheet" href="/css/mainPage.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="  ">
<!-- loader Start -->
<div id="loading">
    <div class="loader simple-loader">
        <div class="loader-body"></div>
    </div>
</div>
<!-- loader END -->

<aside class="sidebar sidebar-default sidebar-white sidebar-base navs-rounded-all ">
    <div class="sidebar-header d-flex align-items-center justify-content-start">
        <a href="/main" class="navbar-brand">
            <div class="logo-main">
                <div class="logo-normal">
                    <svg class=" icon-30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="-0.757324" y="19.2427" width="28" height="4" rx="2"
                              transform="rotate(-45 -0.757324 19.2427)" fill="currentColor"/>
                        <rect x="7.72803" y="27.728" width="28" height="4" rx="2" transform="rotate(-45 7.72803 27.728)"
                              fill="currentColor"/>
                        <rect x="10.5366" y="16.3945" width="16" height="4" rx="2"
                              transform="rotate(45 10.5366 16.3945)" fill="currentColor"/>
                        <rect x="10.5562" y="-0.556152" width="28" height="4" rx="2"
                              transform="rotate(45 10.5562 -0.556152)" fill="currentColor"/>
                    </svg>
                </div>
                <div class="logo-mini">
                    <svg class=" icon-30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="-0.757324" y="19.2427" width="28" height="4" rx="2"
                              transform="rotate(-45 -0.757324 19.2427)" fill="currentColor"/>
                        <rect x="7.72803" y="27.728" width="28" height="4" rx="2" transform="rotate(-45 7.72803 27.728)"
                              fill="currentColor"/>
                        <rect x="10.5366" y="16.3945" width="16" height="4" rx="2"
                              transform="rotate(45 10.5366 16.3945)" fill="currentColor"/>
                        <rect x="10.5562" y="-0.556152" width="28" height="4" rx="2"
                              transform="rotate(45 10.5562 -0.556152)" fill="currentColor"/>
                    </svg>
                </div>
            </div>
            <!--logo End-->


            <h4 class="logo-title">OLS</h4>
        </a>
        <div class="sidebar-toggle" data-toggle="sidebar" data-active="true">
            <i class="icon">
                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M4.25 12.2744L19.25 12.2744" stroke="currentColor" stroke-width="1.5"
                          stroke-linecap="round" stroke-linejoin="round"></path>
                    <path d="M10.2998 18.2988L4.2498 12.2748L10.2998 6.24976" stroke="currentColor" stroke-width="1.5"
                          stroke-linecap="round" stroke-linejoin="round"></path>
                </svg>
            </i>
        </div>
    </div>
    <div class="sidebar-body pt-0 data-scrollbar">
        <div class="sidebar-list">
            <!-- Sidebar Menu Start -->
            <ul class="navbar-nav iq-main-menu" id="sidebar-menu">
                <li>
                    <hr class="hr-horizontal">
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/main">
                        <i class="fa-solid fa-house"></i>
                        <span class="item-name"> Home</span>
                    </a>
                </li>
                <li>
                    <hr class="hr-horizontal">
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/mycourse">
                        <i class="fa-solid fa-graduation-cap"></i>
                        <span class="item-name"> My courses</span>
                    </a>
                </li>
                <li>
                    <hr class="hr-horizontal">
                </li>
                <c:set value="Admin" var="role">
                </c:set>
                <c:set value="CourseContentCreator" var="creatorRole">
                </c:set>
                <c:set value="TestContentCreator" var="creatorRole2">
                </c:set>
                <c:if test="${user.role eq role}">
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#sidebar-special" role="button"
                           aria-expanded="false" aria-controls="sidebar-special">
                            <i class="icon">
                                <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M7.7688 8.71387H16.2312C18.5886 8.71387 20.5 10.5831 20.5 12.8885V17.8254C20.5 20.1308 18.5886 22 16.2312 22H7.7688C5.41136 22 3.5 20.1308 3.5 17.8254V12.8885C3.5 10.5831 5.41136 8.71387 7.7688 8.71387ZM11.9949 17.3295C12.4928 17.3295 12.8891 16.9419 12.8891 16.455V14.2489C12.8891 13.772 12.4928 13.3844 11.9949 13.3844C11.5072 13.3844 11.1109 13.772 11.1109 14.2489V16.455C11.1109 16.9419 11.5072 17.3295 11.9949 17.3295Z"
                                          fill="currentColor"></path>
                                    <path opacity="0.4"
                                          d="M17.523 7.39595V8.86667C17.1673 8.7673 16.7913 8.71761 16.4052 8.71761H15.7447V7.39595C15.7447 5.37868 14.0681 3.73903 12.0053 3.73903C9.94257 3.73903 8.26594 5.36874 8.25578 7.37608V8.71761H7.60545C7.20916 8.71761 6.83319 8.7673 6.47754 8.87661V7.39595C6.4877 4.41476 8.95692 2 11.985 2C15.0537 2 17.523 4.41476 17.523 7.39595Z"
                                          fill="currentColor"></path>
                                </svg>
                            </i>
                            <span class="item-name">Admin</span>
                            <i class="right-icon">
                                <svg class="icon-18" xmlns="http://www.w3.org/2000/svg" width="18" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M9 5l7 7-7 7"/>
                                </svg>
                            </i>
                        </a>
                        <ul class="sub-nav collapse" id="sidebar-special" data-bs-parent="#sidebar-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="/admin/dashboard">
                                    <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                                         class="icon-20">
                                        <path opacity="0.4"
                                              d="M16.0756 2H19.4616C20.8639 2 22.0001 3.14585 22.0001 4.55996V7.97452C22.0001 9.38864 20.8639 10.5345 19.4616 10.5345H16.0756C14.6734 10.5345 13.5371 9.38864 13.5371 7.97452V4.55996C13.5371 3.14585 14.6734 2 16.0756 2Z"
                                              fill="currentColor"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M4.53852 2H7.92449C9.32676 2 10.463 3.14585 10.463 4.55996V7.97452C10.463 9.38864 9.32676 10.5345 7.92449 10.5345H4.53852C3.13626 10.5345 2 9.38864 2 7.97452V4.55996C2 3.14585 3.13626 2 4.53852 2ZM4.53852 13.4655H7.92449C9.32676 13.4655 10.463 14.6114 10.463 16.0255V19.44C10.463 20.8532 9.32676 22 7.92449 22H4.53852C3.13626 22 2 20.8532 2 19.44V16.0255C2 14.6114 3.13626 13.4655 4.53852 13.4655ZM19.4615 13.4655H16.0755C14.6732 13.4655 13.537 14.6114 13.537 16.0255V19.44C13.537 20.8532 14.6732 22 16.0755 22H19.4615C20.8637 22 22 20.8532 22 19.44V16.0255C22 14.6114 20.8637 13.4655 19.4615 13.4655Z"
                                              fill="currentColor"></path>
                                    </svg>
                                    <i class="sidenav-mini-icon"> D </i>
                                    <span class="item-name">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="/admin/users">
                                    <i class="icon">
                                        <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M11.9488 14.54C8.49884 14.54 5.58789 15.1038 5.58789 17.2795C5.58789 19.4562 8.51765 20.0001 11.9488 20.0001C15.3988 20.0001 18.3098 19.4364 18.3098 17.2606C18.3098 15.084 15.38 14.54 11.9488 14.54Z"
                                                  fill="currentColor"></path>
                                            <path opacity="0.4"
                                                  d="M11.949 12.467C14.2851 12.467 16.1583 10.5831 16.1583 8.23351C16.1583 5.88306 14.2851 4 11.949 4C9.61293 4 7.73975 5.88306 7.73975 8.23351C7.73975 10.5831 9.61293 12.467 11.949 12.467Z"
                                                  fill="currentColor"></path>
                                            <path opacity="0.4"
                                                  d="M21.0881 9.21923C21.6925 6.84176 19.9205 4.70654 17.664 4.70654C17.4187 4.70654 17.1841 4.73356 16.9549 4.77949C16.9244 4.78669 16.8904 4.802 16.8725 4.82902C16.8519 4.86324 16.8671 4.90917 16.8895 4.93889C17.5673 5.89528 17.9568 7.0597 17.9568 8.30967C17.9568 9.50741 17.5996 10.6241 16.9728 11.5508C16.9083 11.6462 16.9656 11.775 17.0793 11.7948C17.2369 11.8227 17.3981 11.8371 17.5629 11.8416C19.2059 11.8849 20.6807 10.8213 21.0881 9.21923Z"
                                                  fill="currentColor"></path>
                                            <path d="M22.8094 14.817C22.5086 14.1722 21.7824 13.73 20.6783 13.513C20.1572 13.3851 18.747 13.205 17.4352 13.2293C17.4155 13.232 17.4048 13.2455 17.403 13.2545C17.4003 13.2671 17.4057 13.2887 17.4316 13.3022C18.0378 13.6039 20.3811 14.916 20.0865 17.6834C20.074 17.8032 20.1698 17.9068 20.2888 17.8888C20.8655 17.8059 22.3492 17.4853 22.8094 16.4866C23.0637 15.9589 23.0637 15.3456 22.8094 14.817Z"
                                                  fill="currentColor"></path>
                                            <path opacity="0.4"
                                                  d="M7.04459 4.77973C6.81626 4.7329 6.58077 4.70679 6.33543 4.70679C4.07901 4.70679 2.30701 6.84201 2.9123 9.21947C3.31882 10.8216 4.79355 11.8851 6.43661 11.8419C6.60136 11.8374 6.76343 11.8221 6.92013 11.7951C7.03384 11.7753 7.09115 11.6465 7.02668 11.551C6.3999 10.6234 6.04263 9.50765 6.04263 8.30991C6.04263 7.05904 6.43303 5.89462 7.11085 4.93913C7.13234 4.90941 7.14845 4.86348 7.12696 4.82926C7.10906 4.80135 7.07593 4.78694 7.04459 4.77973Z"
                                                  fill="currentColor"></path>
                                            <path d="M3.32156 13.5127C2.21752 13.7297 1.49225 14.1719 1.19139 14.8167C0.936203 15.3453 0.936203 15.9586 1.19139 16.4872C1.65163 17.4851 3.13531 17.8066 3.71195 17.8885C3.83104 17.9065 3.92595 17.8038 3.91342 17.6832C3.61883 14.9167 5.9621 13.6046 6.56918 13.3029C6.59425 13.2885 6.59962 13.2677 6.59694 13.2542C6.59515 13.2452 6.5853 13.2317 6.5656 13.2299C5.25294 13.2047 3.84358 13.3848 3.32156 13.5127Z"
                                                  fill="currentColor"></path>
                                        </svg>
                                    </i>
                                    <i class="sidenav-mini-icon"> U </i>
                                    <span class="item-name">Users</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/admin/tokens">
                                    <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M19.761 12.001C19.761 12.8145 20.429 13.4764 21.25 13.4764C21.664 13.4764 22 13.8093 22 14.2195V16.8958C22 19.159 20.142 21 17.858 21H6.143C3.859 21 2 19.159 2 16.8958V14.2195C2 13.8093 2.336 13.4764 2.75 13.4764C3.572 13.4764 4.24 12.8145 4.24 12.001C4.24 11.2083 3.599 10.6118 2.75 10.6118C2.551 10.6118 2.361 10.5335 2.22 10.3938C2.079 10.2541 2 10.0648 2 9.86866L2.002 7.10514C2.002 4.84201 3.86 3 6.144 3H17.856C20.14 3 21.999 4.84201 21.999 7.10514L22 9.78245C22 9.97864 21.921 10.1689 21.781 10.3076C21.64 10.4473 21.45 10.5256 21.25 10.5256C20.429 10.5256 19.761 11.1875 19.761 12.001ZM14.252 12.648L15.431 11.5105C15.636 11.3143 15.707 11.025 15.618 10.7575C15.53 10.4899 15.3 10.2997 15.022 10.261L13.393 10.0252L12.664 8.5627C12.539 8.31102 12.285 8.15446 12.002 8.15347H12C11.718 8.15347 11.464 8.31003 11.337 8.56171L10.608 10.0252L8.982 10.26C8.701 10.2997 8.471 10.4899 8.382 10.7575C8.294 11.025 8.365 11.3143 8.569 11.5105L9.748 12.648L9.47 14.2562C9.422 14.5336 9.535 14.8091 9.765 14.9746C9.895 15.0667 10.046 15.1143 10.199 15.1143C10.316 15.1143 10.434 15.0855 10.542 15.0291L12 14.2701L13.455 15.0271C13.707 15.1608 14.006 15.14 14.235 14.9736C14.466 14.8091 14.579 14.5336 14.531 14.2562L14.252 12.648Z"
                                              fill="currentColor"></path>
                                    </svg>
                                    <i class="sidenav-mini-icon"> T </i>
                                    <span class="item-name">Tokens</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <hr class="hr-horizontal">
                    </li>
                </c:if>
                <c:if test="${user.role eq creatorRole or user.role eq role or user.role eq creatorRole2}">
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#sidebar-content" role="button"
                           aria-expanded="false" aria-controls="sidebar-content">
                            <i class="icon">
                                <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M10.7044 3.51898C10.034 3.51898 9.46373 3.9848 9.30365 4.61265H14.6863C14.5263 3.9848 13.956 3.51898 13.2856 3.51898H10.7044ZM16.2071 4.61264H18.1881C20.2891 4.61264 22 6.34428 22 8.47085C22 8.47085 21.94 9.3711 21.92 10.6248C21.918 10.724 21.8699 10.8212 21.7909 10.88C21.3097 11.2354 20.8694 11.5291 20.8294 11.5493C19.1686 12.6632 17.2386 13.447 15.1826 13.8369C15.0485 13.8632 14.9165 13.7934 14.8484 13.6739C14.2721 12.6754 13.1956 12.0253 11.995 12.0253C10.8024 12.0253 9.71586 12.6683 9.12256 13.6678C9.05353 13.7853 8.92346 13.8531 8.7904 13.8278C6.75138 13.4369 4.82141 12.6541 3.17059 11.5594L2.21011 10.8911C2.13007 10.8405 2.08004 10.7493 2.08004 10.6481C2.05003 10.1316 2 8.47085 2 8.47085C2 6.34428 3.71086 4.61264 5.81191 4.61264H7.78289C7.97299 3.1443 9.2036 2 10.7044 2H13.2856C14.7864 2 16.017 3.1443 16.2071 4.61264ZM21.6598 12.8152L21.6198 12.8355C19.5988 14.1924 17.1676 15.0937 14.6163 15.4684C14.2561 15.519 13.8959 15.2861 13.7959 14.9216C13.5758 14.0912 12.8654 13.5443 12.015 13.5443H12.005H11.985C11.1346 13.5443 10.4242 14.0912 10.2041 14.9216C10.1041 15.2861 9.74387 15.519 9.38369 15.4684C6.83242 15.0937 4.4012 14.1924 2.38019 12.8355C2.37019 12.8254 2.27014 12.7646 2.1901 12.8152C2.10005 12.8659 2.10005 12.9874 2.10005 12.9874L2.17009 18.1519C2.17009 20.2785 3.87094 22 5.97199 22H18.018C20.1191 22 21.8199 20.2785 21.8199 18.1519L21.9 12.9874C21.9 12.9874 21.9 12.8659 21.8099 12.8152C21.7599 12.7849 21.6999 12.795 21.6598 12.8152ZM12.7454 17.0583C12.7454 17.4836 12.4152 17.8177 11.995 17.8177C11.5848 17.8177 11.2446 17.4836 11.2446 17.0583V15.7519C11.2446 15.3367 11.5848 14.9924 11.995 14.9924C12.4152 14.9924 12.7454 15.3367 12.7454 15.7519V17.0583Z"
                                          fill="currentColor"></path>
                                </svg>
                            </i>
                            <span class="item-name">Content Creator</span>
                            <i class="right-icon">
                                <svg class="icon-18" xmlns="http://www.w3.org/2000/svg" width="18" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M9 5l7 7-7 7"/>
                                </svg>
                            </i>
                        </a>
                        <ul class="sub-nav collapse" id="sidebar-content" data-bs-parent="#sidebar-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="/course-content-creator/subjects">
                                    <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M8.62871 12.99C8.62871 13.4 8.96535 13.73 9.37129 13.73H14.2624C14.6683 13.73 15.005 13.4 15.005 12.99C15.005 12.57 14.6683 12.24 14.2624 12.24H9.37129C8.96535 12.24 8.62871 12.57 8.62871 12.99ZM19.3381 9.02561C19.5708 9.02292 19.8242 9.02 20.0545 9.02C20.302 9.02 20.5 9.22 20.5 9.47V17.51C20.5 19.99 18.5099 22 16.0446 22H8.17327C5.59901 22 3.5 19.89 3.5 17.29V6.51C3.5 4.03 5.5 2 7.96535 2H13.2525C13.5099 2 13.7079 2.21 13.7079 2.46V5.68C13.7079 7.51 15.203 9.01 17.0149 9.02C17.4381 9.02 17.8112 9.02316 18.1377 9.02593C18.3917 9.02809 18.6175 9.03 18.8168 9.03C18.9578 9.03 19.1405 9.02789 19.3381 9.02561ZM19.61 7.5662C18.7961 7.5692 17.8367 7.5662 17.1466 7.5592C16.0516 7.5592 15.1496 6.6482 15.1496 5.5422V2.9062C15.1496 2.4752 15.6674 2.2612 15.9635 2.5722C16.4995 3.1351 17.2361 3.90891 17.9693 4.67913C18.7002 5.44689 19.4277 6.21108 19.9496 6.7592C20.2387 7.0622 20.0268 7.5652 19.61 7.5662Z"
                                              fill="currentColor"></path>
                                    </svg>
                                    <i class="sidenav-mini-icon"> S </i>
                                    <span class="item-name">Subjects</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/course-content-creator/subject-categories">
                                    <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.4"
                                              d="M11.9912 18.6215L5.49945 21.864C5.00921 22.1302 4.39768 21.9525 4.12348 21.4643C4.0434 21.3108 4.00106 21.1402 4 20.9668V13.7087C4 14.4283 4.40573 14.8725 5.47299 15.37L11.9912 18.6215Z"
                                              fill="currentColor"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M8.89526 2H15.0695C17.7773 2 19.9735 3.06605 20 5.79337V20.9668C19.9989 21.1374 19.9565 21.3051 19.8765 21.4554C19.7479 21.7007 19.5259 21.8827 19.2615 21.9598C18.997 22.0368 18.7128 22.0023 18.4741 21.8641L11.9912 18.6215L5.47299 15.3701C4.40573 14.8726 4 14.4284 4 13.7088V5.79337C4 3.06605 6.19625 2 8.89526 2ZM8.22492 9.62227H15.7486C16.1822 9.62227 16.5336 9.26828 16.5336 8.83162C16.5336 8.39495 16.1822 8.04096 15.7486 8.04096H8.22492C7.79137 8.04096 7.43991 8.39495 7.43991 8.83162C7.43991 9.26828 7.79137 9.62227 8.22492 9.62227Z"
                                              fill="currentColor"></path>
                                    </svg>
                                    <i class="sidenav-mini-icon"> C </i>
                                    <span class="item-name">Categories</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/test-content-creator/quizzes">
                                    <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M17.7689 8.3818H22C22 4.98459 19.9644 3 16.5156 3H7.48444C4.03556 3 2 4.98459 2 8.33847V15.6615C2 19.0154 4.03556 21 7.48444 21H16.5156C19.9644 21 22 19.0154 22 15.6615V15.3495H17.7689C15.8052 15.3495 14.2133 13.7975 14.2133 11.883C14.2133 9.96849 15.8052 8.41647 17.7689 8.41647V8.3818ZM17.7689 9.87241H21.2533C21.6657 9.87241 22 10.1983 22 10.6004V13.131C21.9952 13.5311 21.6637 13.8543 21.2533 13.8589H17.8489C16.8548 13.872 15.9855 13.2084 15.76 12.2643C15.6471 11.6783 15.8056 11.0736 16.1931 10.6122C16.5805 10.1509 17.1573 9.88007 17.7689 9.87241ZM17.92 12.533H18.2489C18.6711 12.533 19.0133 12.1993 19.0133 11.7877C19.0133 11.3761 18.6711 11.0424 18.2489 11.0424H17.92C17.7181 11.0401 17.5236 11.1166 17.38 11.255C17.2364 11.3934 17.1555 11.5821 17.1556 11.779C17.1555 12.1921 17.4964 12.5282 17.92 12.533ZM6.73778 8.3818H12.3822C12.8044 8.3818 13.1467 8.04812 13.1467 7.63649C13.1467 7.22487 12.8044 6.89119 12.3822 6.89119H6.73778C6.31903 6.89116 5.9782 7.2196 5.97333 7.62783C5.97331 8.04087 6.31415 8.37705 6.73778 8.3818Z"
                                              fill="currentColor"></path>
                                    </svg>
                                    <i class="sidenav-mini-icon"> Q </i>
                                    <span class="item-name">Quiz</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <hr class="hr-horizontal">
                    </li>
                </c:if>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="offcanvas"
                       data-bs-target="#offcanvasExample" role="button" aria-controls="offcanvasExample">
                        <i class="icon">
                            <svg width="20" viewBox="0 0 24 24" class="animated-rotate icon-20" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M20.8064 7.62361L20.184 6.54352C19.6574 5.6296 18.4905 5.31432 17.5753 5.83872V5.83872C17.1397 6.09534 16.6198 6.16815 16.1305 6.04109C15.6411 5.91402 15.2224 5.59752 14.9666 5.16137C14.8021 4.88415 14.7137 4.56839 14.7103 4.24604V4.24604C14.7251 3.72922 14.5302 3.2284 14.1698 2.85767C13.8094 2.48694 13.3143 2.27786 12.7973 2.27808H11.5433C11.0367 2.27807 10.5511 2.47991 10.1938 2.83895C9.83644 3.19798 9.63693 3.68459 9.63937 4.19112V4.19112C9.62435 5.23693 8.77224 6.07681 7.72632 6.0767C7.40397 6.07336 7.08821 5.98494 6.81099 5.82041V5.82041C5.89582 5.29601 4.72887 5.61129 4.20229 6.52522L3.5341 7.62361C3.00817 8.53639 3.31916 9.70261 4.22975 10.2323V10.2323C4.82166 10.574 5.18629 11.2056 5.18629 11.8891C5.18629 12.5725 4.82166 13.2041 4.22975 13.5458V13.5458C3.32031 14.0719 3.00898 15.2353 3.5341 16.1454V16.1454L4.16568 17.2346C4.4124 17.6798 4.82636 18.0083 5.31595 18.1474C5.80554 18.2866 6.3304 18.2249 6.77438 17.976V17.976C7.21084 17.7213 7.73094 17.6516 8.2191 17.7822C8.70725 17.9128 9.12299 18.233 9.37392 18.6717C9.53845 18.9489 9.62686 19.2646 9.63021 19.587V19.587C9.63021 20.6435 10.4867 21.5 11.5433 21.5H12.7973C13.8502 21.5001 14.7053 20.6491 14.7103 19.5962V19.5962C14.7079 19.088 14.9086 18.6 15.2679 18.2407C15.6272 17.8814 16.1152 17.6807 16.6233 17.6831C16.9449 17.6917 17.2594 17.7798 17.5387 17.9394V17.9394C18.4515 18.4653 19.6177 18.1544 20.1474 17.2438V17.2438L20.8064 16.1454C21.0615 15.7075 21.1315 15.186 21.001 14.6964C20.8704 14.2067 20.55 13.7894 20.1108 13.5367V13.5367C19.6715 13.284 19.3511 12.8666 19.2206 12.3769C19.09 11.8873 19.16 11.3658 19.4151 10.928C19.581 10.6383 19.8211 10.3982 20.1108 10.2323V10.2323C21.0159 9.70289 21.3262 8.54349 20.8064 7.63277V7.63277V7.62361Z"
                                      stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                      stroke-linejoin="round"></path>
                                <circle cx="12.1747" cy="11.8891" r="2.63616" stroke="currentColor" stroke-width="1.5"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"></circle>
                            </svg>
                        </i>
                        <span class="item-name">Setting</span>
                    </a>
                </li>
            </ul>
            <!-- Sidebar Menu End -->
        </div>
    </div>
    <div class="sidebar-footer"></div>
</aside>
<main class="main-content">
    <div class="position-relative iq-banner">
        <!--Nav Start-->
        <nav class="nav navbar navbar-expand-lg navbar-light iq-navbar">
            <div class="container-fluid navbar-inner">
                <a href="#" class="navbar-brand">
                    <!--Logo start-->
                    <!--logo End-->

                    <div class="logo-main">
                        <div class="logo-normal">
                            <svg class="text-primary icon-30" viewBox="0 0 30 30" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <rect x="-0.757324" y="19.2427" width="28" height="4" rx="2"
                                      transform="rotate(-45 -0.757324 19.2427)" fill="currentColor"/>
                                <rect x="7.72803" y="27.728" width="28" height="4" rx="2"
                                      transform="rotate(-45 7.72803 27.728)" fill="currentColor"/>
                                <rect x="10.5366" y="16.3945" width="16" height="4" rx="2"
                                      transform="rotate(45 10.5366 16.3945)" fill="currentColor"/>
                                <rect x="10.5562" y="-0.556152" width="28" height="4" rx="2"
                                      transform="rotate(45 10.5562 -0.556152)" fill="currentColor"/>
                            </svg>
                        </div>
                        <div class="logo-mini">
                            <svg class="text-primary icon-30" viewBox="0 0 30 30" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <rect x="-0.757324" y="19.2427" width="28" height="4" rx="2"
                                      transform="rotate(-45 -0.757324 19.2427)" fill="currentColor"/>
                                <rect x="7.72803" y="27.728" width="28" height="4" rx="2"
                                      transform="rotate(-45 7.72803 27.728)" fill="currentColor"/>
                                <rect x="10.5366" y="16.3945" width="16" height="4" rx="2"
                                      transform="rotate(45 10.5366 16.3945)" fill="currentColor"/>
                                <rect x="10.5562" y="-0.556152" width="28" height="4" rx="2"
                                      transform="rotate(45 10.5562 -0.556152)" fill="currentColor"/>
                            </svg>
                        </div>
                    </div>
                    <!--logo End-->
                </a>
                <div class="sidebar-toggle" data-toggle="sidebar" data-active="true">
                    <i class="icon">
                        <svg width="20px" class="icon-20" viewBox="0 0 24 24">
                            <path fill="currentColor"
                                  d="M4,11V13H16L10.5,18.5L11.92,19.92L19.84,12L11.92,4.08L10.5,5.5L16,11H4Z"/>
                        </svg>
                    </i>
                </div>
                <form action="/searching" method="post">
                    <div class="input-group search-input">
              <span class="input-group-text" id="search-input">
                <svg class="icon-18" width="18" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="11.7669" cy="11.7666" r="8.98856" stroke="currentColor" stroke-width="1.5"
                            stroke-linecap="round" stroke-linejoin="round"></circle>
                    <path d="M18.0186 18.4851L21.5426 22" stroke="currentColor" stroke-width="1.5"
                          stroke-linecap="round" stroke-linejoin="round"></path>
                </svg>
              </span>
                        <button type="submit" hidden="hidden"></button>
                        <input type="search" name="search" class="form-control" placeholder="Search..." required>
                    </div>
                </form>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon">
                  <span class="mt-2 navbar-toggler-bar bar1"></span>
                  <span class="navbar-toggler-bar bar2"></span>
                  <span class="navbar-toggler-bar bar3"></span>
                </span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="mb-2 navbar-nav ms-auto align-items-center navbar-list mb-lg-0">
                        <li class="me-0 me-xl-2">
                            <a class="btn btn-primary btn-sm d-flex gap-2 align-items-center" aria-current="page"
                               href="/price"
                               target="_blank">
                                <svg class="icon-16" width="16" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21.4274 2.5783C20.9274 2.0673 20.1874 1.8783 19.4974 2.0783L3.40742 6.7273C2.67942 6.9293 2.16342 7.5063 2.02442 8.2383C1.88242 8.9843 2.37842 9.9323 3.02642 10.3283L8.05742 13.4003C8.57342 13.7163 9.23942 13.6373 9.66642 13.2093L15.4274 7.4483C15.7174 7.1473 16.1974 7.1473 16.4874 7.4483C16.7774 7.7373 16.7774 8.2083 16.4874 8.5083L10.7164 14.2693C10.2884 14.6973 10.2084 15.3613 10.5234 15.8783L13.5974 20.9283C13.9574 21.5273 14.5774 21.8683 15.2574 21.8683C15.3374 21.8683 15.4274 21.8683 15.5074 21.8573C16.2874 21.7583 16.9074 21.2273 17.1374 20.4773L21.9074 4.5083C22.1174 3.8283 21.9274 3.0883 21.4274 2.5783Z"
                                          fill="currentColor"></path>
                                    <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M3.01049 16.8079C2.81849 16.8079 2.62649 16.7349 2.48049 16.5879C2.18749 16.2949 2.18749 15.8209 2.48049 15.5279L3.84549 14.1619C4.13849 13.8699 4.61349 13.8699 4.90649 14.1619C5.19849 14.4549 5.19849 14.9299 4.90649 15.2229L3.54049 16.5879C3.39449 16.7349 3.20249 16.8079 3.01049 16.8079ZM6.77169 18.0003C6.57969 18.0003 6.38769 17.9273 6.24169 17.7803C5.94869 17.4873 5.94869 17.0133 6.24169 16.7203L7.60669 15.3543C7.89969 15.0623 8.37469 15.0623 8.66769 15.3543C8.95969 15.6473 8.95969 16.1223 8.66769 16.4153L7.30169 17.7803C7.15569 17.9273 6.96369 18.0003 6.77169 18.0003ZM7.02539 21.5683C7.17139 21.7153 7.36339 21.7883 7.55539 21.7883C7.74739 21.7883 7.93939 21.7153 8.08539 21.5683L9.45139 20.2033C9.74339 19.9103 9.74339 19.4353 9.45139 19.1423C9.15839 18.8503 8.68339 18.8503 8.39039 19.1423L7.02539 20.5083C6.73239 20.8013 6.73239 21.2753 7.02539 21.5683Z"
                                          fill="currentColor"></path>
                                </svg>
                                Premium
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link" id="notification-drop" data-bs-toggle="dropdown">
                                <c:if test="${unreadNotifications.size() >= 1}">
                                    <i class="fa-solid fa-bell fa-xl" style="color: blue"></i>
                                    <span class="bg-danger dots"></span>
                                </c:if>
                                <c:if test="${unreadNotifications.size() < 1}">
                                    <i class="fa-solid fa-bell fa-xl"></i>
                                    <span class="bg-danger dots"></span>
                                </c:if>
                            </a>
                            <div class="p-0 sub-drop dropdown-menu dropdown-menu-end"
                                 aria-labelledby="notification-drop">
                                <div class="m-0 shadow-none card">
                                    <div class="py-3 card-header d-flex justify-content-between bg-primary">
                                        <div class="header-title" style="display: flex">
                                            <div class="col-10">
                                                <h5 class="mb-0 text-white">Notifications</h5>
                                            </div>
                                            <div class="col-6">
                                                <a class="read-btn" href="/updateAllNotification"><p>Mark all as
                                                    read</p></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="py-3 card-header d-flex justify-content-between">
                                        <div class="header-title" style="display: flex">
                                            <button id="notification-btn1" class="notification-button active"
                                                    type="button"><strong>All</strong></button>
                                            <button id="notification-btn2" class="notification-button" type="button">
                                                <strong>Unread</strong></button>
                                        </div>
                                    </div>
                                    <div id="notification-content1" class="p-0 card-body dropdown-scroll">
                                        <c:if test="${notification eq null}">
                                            <div class="notification-box" style="margin-bottom: 10px">
                                                No notification
                                            </div>
                                        </c:if>
                                        <c:if test="${notification ne null}">
                                            <c:forEach items="${notification}" var="un" varStatus="loop">
                                                <c:set value="${un.notifications}" var="n">
                                                </c:set>
                                                <a href="/notification/id=${n.id}" class="iq-sub-card">
                                                    <div class="d-flex align-items-center">
                                                        <img class="p-1 avatar-40 rounded-pill bg-soft-primary"
                                                             src="/img/hopeUI/shapes/01.png" alt="">
                                                        <div class="ms-3 w-100" style="display: flex">
                                                            <div class="col-10">
                                                                <h6 class="mb-0 ">${n.content}</h6>
                                                                <div class="d-flex justify-content-between align-items-center">
                                                                    <small class="float-end font-size-12">
                                                                        <c:if test="${currentTime.time - un.time.time <= (1000*120)}">
                                                                            Just now
                                                                        </c:if>
                                                                        <c:if test="${currentTime.time - un.time.time <= (60*60*1000) and currentTime.time - un.time.time > (1000*120)}">
                                                                            <fmt:formatNumber type="number"
                                                                                              maxFractionDigits="0"
                                                                                              value="${((currentTime.time - un.time.time)%(60 * 60 * 1000))/(60 * 1000)}">
                                                                            </fmt:formatNumber>
                                                                            minutes ago
                                                                        </c:if>
                                                                        <c:if test="${currentTime.time - un.time.time <= (24*60*60*1000) and currentTime.time - un.time.time > (60*60*1000)}">
                                                                            <fmt:formatNumber type="number"
                                                                                              maxFractionDigits="0"
                                                                                              value="${((currentTime.time - un.time.time)%(24 * 60 * 60 * 1000))/(60 * 60 * 1000)}">
                                                                            </fmt:formatNumber>
                                                                            hours ago
                                                                        </c:if>
                                                                        <c:if test="${currentTime.time - un.time.time > (24*60*60*1000)}">
                                                                            <fmt:formatNumber type="number"
                                                                                              maxFractionDigits="0"
                                                                                              value="${(currentTime.time - un.time.time)/(24 * 60 * 60 * 1000)}">
                                                                            </fmt:formatNumber>
                                                                            days ago
                                                                        </c:if>
                                                                    </small>
                                                                </div>
                                                            </div>
                                                            <div class="col-2"
                                                                 style="align-self: center; justify-content: center">
                                                                <c:set value="Unread" var="status">
                                                                </c:set>
                                                                <c:if test="${un.status eq status}">
                                                                    <i class="fa-solid fa-circle"
                                                                       style="color: blue"></i>
                                                                </c:if>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                    <div id="notification-content2" class="p-0 card-body dropdown-scroll none">
                                        <c:if test="${unreadNotifications.size() < 1}">
                                            <div class="notification-box" style="margin-bottom: 10px">
                                                No notification
                                            </div>
                                        </c:if>
                                        <c:if test="${unreadNotifications ne null}">
                                            <c:forEach items="${unreadNotifications}" var="un" varStatus="loop">
                                                <c:set value="${un.notifications}" var="n">
                                                </c:set>
                                                <a href="/notification/id=${n.id}" class="iq-sub-card">
                                                    <div class="d-flex align-items-center">
                                                        <img class="p-1 avatar-40 rounded-pill bg-soft-primary"
                                                             src="/img/hopeUI/shapes/01.png" alt="">
                                                        <div class="ms-3 w-100" style="display: flex">
                                                            <div class="col-10">
                                                                <h6 class="mb-0 ">${n.content}</h6>
                                                                <div class="d-flex justify-content-between align-items-center">
                                                                    <small class="float-end font-size-12">
                                                                        <c:if test="${currentTime.time - un.time.time <= (1000*120)}">
                                                                            Just now
                                                                        </c:if>
                                                                        <c:if test="${currentTime.time - un.time.time <= (60*60*1000) and currentTime.time - un.time.time > (1000*120)}">
                                                                            <fmt:formatNumber type="number"
                                                                                              maxFractionDigits="0"
                                                                                              value="${((currentTime.time - un.time.time)%(60 * 60 * 1000))/(60 * 1000)}">
                                                                            </fmt:formatNumber>
                                                                            minutes ago
                                                                        </c:if>
                                                                        <c:if test="${currentTime.time - un.time.time <= (24*60*60*1000) and currentTime.time - un.time.time > (60*60*1000)}">
                                                                            <fmt:formatNumber type="number"
                                                                                              maxFractionDigits="0"
                                                                                              value="${((currentTime.time - un.time.time)%(24 * 60 * 60 * 1000))/(60 * 60 * 1000)}">
                                                                            </fmt:formatNumber>
                                                                            hours ago
                                                                        </c:if>
                                                                        <c:if test="${currentTime.time - un.time.time > (24*60*60*1000)}">
                                                                            <fmt:formatNumber type="number"
                                                                                              maxFractionDigits="0"
                                                                                              value="${(currentTime.time - un.time.time)/(24 * 60 * 60 * 1000)}">
                                                                            </fmt:formatNumber>
                                                                            days ago
                                                                        </c:if>
                                                                    </small>
                                                                </div>
                                                            </div>
                                                            <div class="col-2"
                                                                 style="align-self: center; justify-content: center">
                                                                <c:set value="Unread" var="status">
                                                                </c:set>
                                                                <c:if test="${un.status eq status}">
                                                                    <i class="fa-solid fa-circle"
                                                                       style="color: blue"></i>
                                                                </c:if>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="py-0 nav-link d-flex align-items-center" href="#" id="navbarDropdown"
                               role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="${user.avatar}" alt="User-Profile"
                                     class="theme-color-default-img img-fluid avatar avatar-50 avatar-rounded">
                                <div class="caption ms-3 d-none d-md-block ">
                                    <h6 class="mb-0 caption-title">${user.fullName}</h6>
                                    <p class="mb-0 caption-sub-title">${user.role}</p>
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="/profile">Profile</a>
                                </li>
                                <li><a class="dropdown-item" href="#">Privacy
                                    Setting</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="/logout">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>          <!-- Nav Header Component Start -->
        <div class="iq-navbar-header" style="height: 215px;">
            <div class="container-fluid iq-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flex-wrap d-flex justify-content-between align-items-center">
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="iq-header-img">
                <img src="
                ../../../img/hopeUI/dashboard/top-header.png" alt="header"
                     class="theme-color-default-img img-fluid w-100 h-100 animated-scaleX">
                <img src="
                ../../../img/hopeUI/dashboard/top-header1.png" alt="header"
                     class="theme-color-purple-img img-fluid w-100 h-100 animated-scaleX">
                <img src="
                ../../../img/hopeUI/dashboard/top-header2.png" alt="header"
                     class="theme-color-blue-img img-fluid w-100 h-100 animated-scaleX">
                <img src="
                ../../../img/hopeUI/dashboard/top-header3.png" alt="header"
                     class="theme-color-green-img img-fluid w-100 h-100 animated-scaleX">
                <img src="
                ../../../img/hopeUI/dashboard/top-header4.png" alt="header"
                     class="theme-color-yellow-img img-fluid w-100 h-100 animated-scaleX">
                <img src="
                ../../../img/hopeUI/dashboard/top-header5.png" alt="header"
                     class="theme-color-pink-img img-fluid w-100 h-100 animated-scaleX">
            </div>
        </div>          <!-- Nav Header Component End -->
        <!--Nav End-->
    </div>

    <div class="conatiner-fluid content-inner mt-n5 py-0">
        <div>
            <div class="row">
                <div class="card d-flex">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Update User: ${editUser.fullName}</h4>
                        </div>
                    </div>

                    <div class="card-body mx-auto" style="width: 50%;">
                        <form class="validate-form" method="post" action="/admin/users/edit/${editUser.id}">
                            <div class="form-group validate-input">
                                <input type="hidden" name="id" class="form-control" id="id"
                                       value="${editUser.id}"
                                       placeholder="ID">
                            </div>
                            <div class="form-group validate-input">
                                <label style="color: black; font-weight: bolder" class="form-label" for="fullName">Full Name</label>
                                <input type="text" name="fullName" class="form-control" id="fullName"
                                       value="${editUser.fullName}"
                                       placeholder="Enter Name">
                            </div>
                            <div class="form-group validate-input">
                                <label style="color: black; font-weight: bolder" class="form-label" for="email">Email </label>
                                <input type="email" name="email" class="form-control" id="email"
                                       value="${editUser.email}" placeholder="Enter Email">
                            </div>
                            <div class="form-group validate-input">
                                <label style="color: black; font-weight: bolder" class="form-label" for="gender">Gender</label>
                                <select type="gender" name="gender" id="gender" class="form-select mb-3 shadow-none">
                                    <option value="${editUser.gender}" hidden selected>${editUser.gender}</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                            <div class="form-group validate-input">
                                <label style="color: black; font-weight: bolder" class="form-label" for="mobile">Mobile</label>
                                <input type="tel" name="mobile" class="form-control" id="mobile"
                                       value="${editUser.mobile}">
                            </div>
                            <div class="form-group validate-input">
                                <label style="color: black; font-weight: bolder" class="form-label" for="role">Role</label>
                                <select type="role" name="role" id="role" class="form-select mb-3 shadow-none">
                                    <option value="${editUser.role}" hidden selected>${editUser.role}</option>
                                    <option value="Common">Common</option>
                                    <option value="Customer">Customer</option>
                                    <option value="Learning">Learning</option>
                                    <option value="CourseContentCreator">Course Content Creator</option>
                                    <option value="TestContentCreator">Test Content Creator</option>
                                    <option value="Admin">Admin</option>
                                </select>
                            </div>
                            <div class="form-group validate-input">
                                <label style="color: black; font-weight: bolder" class="form-label">Status</label>
                                <div class="form-check" style="margin-left: 50px">
                                    <input class="form-check-input" type="radio" name="status" id="status1"
                                           value="Active" <c:if test="${editUser.status == 'Active'}">checked</c:if>>
                                    <label class="form-check-label" for="status1">
                                        Active
                                    </label>
                                </div>
                                <div class="form-check" style="margin-left: 50px">
                                    <input class="form-check-input" type="radio" name="status" id="status2"
                                           value="Inactive"
                                           <c:if test="${editUser.status == 'Inactive'}">checked</c:if>>
                                    <label class="form-check-label" for="status2">
                                        Inactive
                                    </label>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary w-100">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Footer Section Start -->
    <footer class="footer">
        <div class="footer-body">
            <ul class="left-panel list-inline mb-0 p-0">
                <li class="list-inline-item"><a href="../../../dashboard/extra/privacy-policy.html">Privacy Policy</a>
                </li>
                <li class="list-inline-item"><a href="../../../dashboard/extra/terms-of-service.html">Terms of Use</a>
                </li>
            </ul>
            <div class="right-panel">
                ©
                <script>document.write(new Date().getFullYear())</script>
                Hope UI, Made with
                <span class="">
                      <svg class="icon-15" width="15" viewBox="0 0 24 24" fill="none"
                           xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M15.85 2.50065C16.481 2.50065 17.111 2.58965 17.71 2.79065C21.401 3.99065 22.731 8.04065 21.62 11.5806C20.99 13.3896 19.96 15.0406 18.611 16.3896C16.68 18.2596 14.561 19.9196 12.28 21.3496L12.03 21.5006L11.77 21.3396C9.48102 19.9196 7.35002 18.2596 5.40102 16.3796C4.06102 15.0306 3.03002 13.3896 2.39002 11.5806C1.26002 8.04065 2.59002 3.99065 6.32102 2.76965C6.61102 2.66965 6.91002 2.59965 7.21002 2.56065H7.33002C7.61102 2.51965 7.89002 2.50065 8.17002 2.50065H8.28002C8.91002 2.51965 9.52002 2.62965 10.111 2.83065H10.17C10.21 2.84965 10.24 2.87065 10.26 2.88965C10.481 2.96065 10.69 3.04065 10.89 3.15065L11.27 3.32065C11.3618 3.36962 11.4649 3.44445 11.554 3.50912C11.6104 3.55009 11.6612 3.58699 11.7 3.61065C11.7163 3.62028 11.7329 3.62996 11.7496 3.63972C11.8354 3.68977 11.9247 3.74191 12 3.79965C13.111 2.95065 14.46 2.49065 15.85 2.50065ZM18.51 9.70065C18.92 9.68965 19.27 9.36065 19.3 8.93965V8.82065C19.33 7.41965 18.481 6.15065 17.19 5.66065C16.78 5.51965 16.33 5.74065 16.18 6.16065C16.04 6.58065 16.26 7.04065 16.68 7.18965C17.321 7.42965 17.75 8.06065 17.75 8.75965V8.79065C17.731 9.01965 17.8 9.24065 17.94 9.41065C18.08 9.58065 18.29 9.67965 18.51 9.70065Z"
                                fill="currentColor"></path>
                      </svg>
                  </span> by <a href="https://iqonic.design/">IQONIC Design</a>.
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->
</main>
<!-- Wrapper End-->
<!-- offcanvas start -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasExample" data-bs-scroll="true" data-bs-backdrop="true"
     aria-labelledby="offcanvasExampleLabel">
    <div class="offcanvas-header">
        <div class="d-flex align-items-center">
            <h3 class="offcanvas-title me-3" id="offcanvasExampleLabel">Settings</h3>
        </div>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body data-scrollbar">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="mb-3">Scheme</h5>
                <div class="d-grid gap-3 grid-cols-3 mb-4">
                    <div class="btn btn-border" data-setting="color-mode" data-name="color" data-value="auto">
                        <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill="currentColor" d="M7,2V13H10V22L17,10H13L17,2H7Z"/>
                        </svg>
                        <span class="ms-2 "> Auto </span>
                    </div>

                    <div class="btn btn-border" data-setting="color-mode" data-name="color" data-value="dark">
                        <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill="currentColor"
                                  d="M9,2C7.95,2 6.95,2.16 6,2.46C10.06,3.73 13,7.5 13,12C13,16.5 10.06,20.27 6,21.54C6.95,21.84 7.95,22 9,22A10,10 0 0,0 19,12A10,10 0 0,0 9,2Z"/>
                        </svg>
                        <span class="ms-2 "> Dark  </span>
                    </div>
                    <div class="btn btn-border active" data-setting="color-mode" data-name="color" data-value="light">
                        <svg class="icon-20" width="20" viewBox="0 0 24 24" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill="currentColor"
                                  d="M12,8A4,4 0 0,0 8,12A4,4 0 0,0 12,16A4,4 0 0,0 16,12A4,4 0 0,0 12,8M12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6A6,6 0 0,1 18,12A6,6 0 0,1 12,18M20,8.69V4H15.31L12,0.69L8.69,4H4V8.69L0.69,12L4,15.31V20H8.69L12,23.31L15.31,20H20V15.31L23.31,12L20,8.69Z"/>
                        </svg>
                        <span class="ms-2 "> Light</span>
                    </div>
                </div>
                <hr class="hr-horizontal">
                <div class="d-flex align-items-center justify-content-between">
                    <h5 class="mt-4 mb-3">Color Customizer</h5>
                    <button class="btn btn-transparent p-0 border-0" data-value="theme-color-default"
                            data-info="#079aa2" data-setting="color-mode1" data-name="color" data-bs-toggle="tooltip"
                            data-bs-placement="top" title="" data-bs-original-title="Default">
                        <svg class="icon-18" width="18" viewBox="0 0 24 24" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M21.4799 12.2424C21.7557 12.2326 21.9886 12.4482 21.9852 12.7241C21.9595 14.8075 21.2975 16.8392 20.0799 18.5506C18.7652 20.3986 16.8748 21.7718 14.6964 22.4612C12.518 23.1505 10.1711 23.1183 8.01299 22.3694C5.85488 21.6205 4.00382 20.196 2.74167 18.3126C1.47952 16.4293 0.875433 14.1905 1.02139 11.937C1.16734 9.68346 2.05534 7.53876 3.55018 5.82945C5.04501 4.12014 7.06478 2.93987 9.30193 2.46835C11.5391 1.99683 13.8711 2.2599 15.9428 3.2175L16.7558 1.91838C16.9822 1.55679 17.5282 1.62643 17.6565 2.03324L18.8635 5.85986C18.945 6.11851 18.8055 6.39505 18.549 6.48314L14.6564 7.82007C14.2314 7.96603 13.8445 7.52091 14.0483 7.12042L14.6828 5.87345C13.1977 5.18699 11.526 4.9984 9.92231 5.33642C8.31859 5.67443 6.8707 6.52052 5.79911 7.74586C4.72753 8.97119 4.09095 10.5086 3.98633 12.1241C3.8817 13.7395 4.31474 15.3445 5.21953 16.6945C6.12431 18.0446 7.45126 19.0658 8.99832 19.6027C10.5454 20.1395 12.2278 20.1626 13.7894 19.6684C15.351 19.1743 16.7062 18.1899 17.6486 16.8652C18.4937 15.6773 18.9654 14.2742 19.0113 12.8307C19.0201 12.5545 19.2341 12.3223 19.5103 12.3125L21.4799 12.2424Z"
                                  fill="#31BAF1"/>
                            <path d="M20.0941 18.5594C21.3117 16.848 21.9736 14.8163 21.9993 12.7329C22.0027 12.4569 21.7699 12.2413 21.4941 12.2512L19.5244 12.3213C19.2482 12.3311 19.0342 12.5633 19.0254 12.8395C18.9796 14.283 18.5078 15.6861 17.6628 16.8739C16.7203 18.1986 15.3651 19.183 13.8035 19.6772C12.2419 20.1714 10.5595 20.1483 9.01246 19.6114C7.4654 19.0746 6.13845 18.0534 5.23367 16.7033C4.66562 15.8557 4.28352 14.9076 4.10367 13.9196C4.00935 18.0934 6.49194 21.37 10.008 22.6416C10.697 22.8908 11.4336 22.9852 12.1652 22.9465C13.075 22.8983 13.8508 22.742 14.7105 22.4699C16.8889 21.7805 18.7794 20.4073 20.0941 18.5594Z"
                                  fill="#0169CA"/>
                        </svg>
                    </button>
                </div>
                <div class="grid-cols-5 mb-4 d-grid gap-x-2">
                    <div class="btn btn-border bg-transparent" data-value="theme-color-blue" data-info="#573BFF"
                         data-setting="color-mode1" data-name="color" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="" data-bs-original-title="Theme-1">
                        <svg class="customizer-btn icon-32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                             width="32">
                            <circle cx="12" cy="12" r="10" fill="#00C3F9"/>
                            <path d="M2,12 a1,1 1 1,0 20,0" fill="#573BFF"/>
                        </svg>
                    </div>
                    <div class="btn btn-border bg-transparent" data-value="theme-color-gray" data-info="#FD8D00"
                         data-setting="color-mode1" data-name="color" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="" data-bs-original-title="Theme-2">
                        <svg class="customizer-btn icon-32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                             width="32">
                            <circle cx="12" cy="12" r="10" fill="#91969E"/>
                            <path d="M2,12 a1,1 1 1,0 20,0" fill="#FD8D00"/>
                        </svg>
                    </div>
                    <div class="btn btn-border bg-transparent" data-value="theme-color-red" data-info="#366AF0"
                         data-setting="color-mode1" data-name="color" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="" data-bs-original-title="Theme-3">
                        <svg class="customizer-btn icon-32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                             width="32">
                            <circle cx="12" cy="12" r="10" fill="#DB5363"/>
                            <path d="M2,12 a1,1 1 1,0 20,0" fill="#366AF0"/>
                        </svg>
                    </div>
                    <div class="btn btn-border bg-transparent" data-value="theme-color-yellow" data-info="#6410F1"
                         data-setting="color-mode1" data-name="color" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="" data-bs-original-title="Theme-4">
                        <svg class="customizer-btn icon-32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                             width="32">
                            <circle cx="12" cy="12" r="10" fill="#EA6A12"/>
                            <path d="M2,12 a1,1 1 1,0 20,0" fill="#6410F1"/>
                        </svg>
                    </div>
                    <div class="btn btn-border bg-transparent" data-value="theme-color-pink" data-info="#25C799"
                         data-setting="color-mode1" data-name="color" data-bs-toggle="tooltip" data-bs-placement="top"
                         title="" data-bs-original-title="Theme-5">
                        <svg class="customizer-btn icon-32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                             width="32">
                            <circle cx="12" cy="12" r="10" fill="#E586B3"/>
                            <path d="M2,12 a1,1 1 1,0 20,0" fill="#25C799"/>
                        </svg>
                    </div>

                </div>
                <hr class="hr-horizontal">
                <h5 class="mb-3 mt-4">Scheme Direction</h5>
                <div class="d-grid gap-3 grid-cols-2 mb-4">
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/01.png" alt="ltr"
                             class="mode dark-img img-fluid btn-border p-0 flex-column active mb-2"
                             data-setting="dir-mode" data-name="dir" data-value="ltr">
                        <img src="
                        ../../../img/hopeUI/settings/light/01.png" alt="ltr"
                             class="mode light-img img-fluid btn-border p-0 flex-column active mb-2"
                             data-setting="dir-mode" data-name="dir" data-value="ltr">
                        <span class=" mt-2"> LTR </span>
                    </div>
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/02.png" alt=""
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="dir-mode"
                             data-name="dir" data-value="rtl">
                        <img src="
                        ../../../img/hopeUI/settings/light/02.png" alt=""
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="dir-mode"
                             data-name="dir" data-value="rtl">
                        <span class="mt-2 "> RTL  </span>
                    </div>
                </div>
                <hr class="hr-horizontal">
                <h5 class="mt-4 mb-3">Sidebar Color</h5>
                <div class="d-grid gap-3 grid-cols-2 mb-4">
                    <div class="btn btn-border d-block" data-setting="sidebar" data-name="sidebar-color"
                         data-value="sidebar-white">
                        <span class=""> Default </span>
                    </div>
                    <div class="btn btn-border d-block" data-setting="sidebar" data-name="sidebar-color"
                         data-value="sidebar-dark">
                        <span class=""> Dark </span>
                    </div>
                    <div class="btn btn-border d-block" data-setting="sidebar" data-name="sidebar-color"
                         data-value="sidebar-color">
                        <span class=""> Color </span>
                    </div>

                    <div class="btn btn-border d-block" data-setting="sidebar" data-name="sidebar-color"
                         data-value="sidebar-transparent">
                        <span class=""> Transparent </span>
                    </div>
                </div>
                <hr class="hr-horizontal">
                <h5 class="mt-4 mb-3">Sidebar Types</h5>
                <div class="d-grid gap-3 grid-cols-3 mb-4">
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/03.png" alt="mini"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-type" data-value="sidebar-mini">
                        <img src="
                        ../../../img/hopeUI/settings/light/03.png" alt="mini"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-type" data-value="sidebar-mini">
                        <span class="mt-2">Mini</span>
                    </div>
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/04.png" alt="hover"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-type" data-value="sidebar-hover" data-extra-value="sidebar-mini">
                        <img src="
                        ../../../img/hopeUI/settings/light/04.png" alt="hover"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-type" data-value="sidebar-hover" data-extra-value="sidebar-mini">
                        <span class="mt-2">Hover</span>
                    </div>
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/05.png" alt="boxed"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-type" data-value="sidebar-boxed">
                        <img src="
                        ../../../img/hopeUI/settings/light/05.png" alt="boxed"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-type" data-value="sidebar-boxed">
                        <span class="mt-2">Boxed</span>
                    </div>
                </div>
                <hr class="hr-horizontal">
                <h5 class="mt-4 mb-3">Sidebar Active Style</h5>
                <div class="d-grid gap-3 grid-cols-2 mb-4">
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/06.png" alt="rounded-one-side"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-item" data-value="navs-rounded">
                        <img src="
                        ../../../img/hopeUI/settings/light/06.png" alt="rounded-one-side"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-item" data-value="navs-rounded">
                        <span class="mt-2">Rounded One Side</span>
                    </div>
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/07.png" alt="rounded-all"
                             class="mode dark-img img-fluid btn-border p-0 flex-column active mb-2"
                             data-setting="sidebar" data-name="sidebar-item" data-value="navs-rounded-all">
                        <img src="
                        ../../../img/hopeUI/settings/light/07.png" alt="rounded-all"
                             class="mode light-img img-fluid btn-border p-0 flex-column active mb-2"
                             data-setting="sidebar" data-name="sidebar-item" data-value="navs-rounded-all">
                        <span class="mt-2">Rounded All</span>
                    </div>
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/08.png" alt="pill-one-side"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-item" data-value="navs-pill">
                        <img src="
                        ../../../img/hopeUI/settings/light/09.png" alt="pill-one-side"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="sidebar"
                             data-name="sidebar-item" data-value="navs-pill">
                        <span class="mt-2">Pill One Side</span>
                    </div>
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/09.png" alt="pill-all"
                             class="mode dark-img img-fluid btn-border p-0 flex-column" data-setting="sidebar"
                             data-name="sidebar-item" data-value="navs-pill-all">
                        <img src="
                        ../../../img/hopeUI/settings/light/08.png" alt="pill-all"
                             class="mode light-img img-fluid btn-border p-0 flex-column" data-setting="sidebar"
                             data-name="sidebar-item" data-value="navs-pill-all">
                        <span class="mt-2">Pill All</span>
                    </div>
                </div>
                <hr class="hr-horizontal">
                <h5 class="mt-4 mb-3">Navbar Style</h5>
                <div class="d-grid gap-3 grid-cols-2 ">
                    <div class=" text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/11.png" alt="image"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="navbar"
                             data-target=".iq-navbar" data-name="navbar-type" data-value="nav-glass">
                        <img src="
                        ../../../img/hopeUI/settings/light/10.png" alt="image"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="navbar"
                             data-target=".iq-navbar" data-name="navbar-type" data-value="nav-glass">
                        <span class="mt-2">Glass</span>
                    </div>
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/10.png" alt="color"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="navbar"
                             data-target=".iq-navbar-header" data-name="navbar-type" data-value="navs-bg-color">
                        <img src="
                        ../../../img/hopeUI/settings/light/11.png" alt="color"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="navbar"
                             data-target=".iq-navbar-header" data-name="navbar-type" data-value="navs-bg-color">
                        <span class="mt-2">Color</span>
                    </div>
                    <div class=" text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/12.png" alt="sticky"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="navbar"
                             data-target=".iq-navbar" data-name="navbar-type" data-value="navs-sticky">
                        <img src="
                        ../../../img/hopeUI/settings/light/12.png" alt="sticky"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="navbar"
                             data-target=".iq-navbar" data-name="navbar-type" data-value="navs-sticky">
                        <span class="mt-2">Sticky</span>
                    </div>
                    <div class="text-center">
                        <img src="
                        ../../../img/hopeUI/settings/dark/13.png" alt="transparent"
                             class="mode dark-img img-fluid btn-border p-0 flex-column mb-2" data-setting="navbar"
                             data-target=".iq-navbar" data-name="navbar-type" data-value="navs-transparent">
                        <img src="
                        ../../../img/hopeUI/settings/light/13.png" alt="transparent"
                             class="mode light-img img-fluid btn-border p-0 flex-column mb-2" data-setting="navbar"
                             data-target=".iq-navbar" data-name="navbar-type" data-value="navs-transparent">
                        <span class="mt-2">Transparent</span>
                    </div>
                    <div class="btn btn-border active col-span-full mt-4 d-block" data-setting="navbar"
                         data-name="navbar-default" data-value="default">
                        <span class=""> Default Navbar</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Library Bundle Script -->
<script src="
../../../js/hopeUIjs/core/libs.min.js"></script>

<!-- External Library Bundle Script -->
<script src="
../../../js/hopeUIjs/core/external.min.js"></script>

<!-- Widgetchart Script -->
<script src="
../../../js/hopeUIjs/charts/widgetcharts.js"></script>

<!-- mapchart Script -->
<script src="
../../../js/hopeUIjs/charts/vectore-chart.js"></script>
<script src="
../../../js/hopeUIjs/charts/dashboard.js"></script>

<!-- fslightbox Script -->
<script src="
../../../js/hopeUIjs/plugins/fslightbox.js"></script>

<!-- Settings Script -->
<script src="
../../../js/hopeUIjs/plugins/setting.js"></script>

<!-- Slider-tab Script -->
<script src="
../../../js/hopeUIjs/plugins/slider-tabs.js"></script>

<!-- Form Wizard Script -->
<script src="
../../../js/hopeUIjs/plugins/form-wizard.js"></script>

<!-- AOS Animation Plugin-->

<!-- App Script -->
<script src="
../../../js/hopeUIjs/hope-ui.js" defer></script>
<script>
    const btn1 = document.getElementById("notification-btn1");
    const btn2 = document.getElementById("notification-btn2");
    const notificationContent1 = document.getElementById("notification-content1");
    const notificationContent2 = document.getElementById("notification-content2");
    btn1.addEventListener('click', () => {
        btn2.classList.remove('active');
        btn1.classList.add('active');
        notificationContent1.classList.remove('none');
        notificationContent1.classList.add('show');
        notificationContent2.classList.remove('show');
        notificationContent2.classList.add('none');
    });
    btn2.addEventListener('click', () => {
        btn1.classList.remove('active');
        btn2.classList.add('active');
        notificationContent2.classList.remove('none');
        notificationContent2.classList.add('show');
        notificationContent1.classList.remove('show');
        notificationContent1.classList.add('none');
    });
</script>
</body>
</html>