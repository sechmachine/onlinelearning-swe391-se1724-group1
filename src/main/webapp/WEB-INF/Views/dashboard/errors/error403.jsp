<%--
  Created by IntelliJ IDEA.
  User: CYKA
  Date: 18/06/2023
  Time: 15:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Hope UI | Responsive Bootstrap 5 Admin Dashboard Template</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="../../../img/hopeUI/favicon.ico"/>

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="../../../css/hopeUI/core/libs.min.css"/>


    <!-- Hope Ui Design System Css -->
    <link rel="stylesheet" href="../../../css/hopeUI/hope-ui.min.css?v=2.0.0"/>

    <!-- Custom Css -->
    <link rel="stylesheet" href="../../../css/hopeUI/custom.min.css?v=2.0.0"/>

    <!-- Dark Css -->
    <link rel="stylesheet" href="../../../css/hopeUI/dark.min.css"/>

    <!-- Customizer Css -->
    <link rel="stylesheet" href="../../../css/hopeUI/customizer.min.css"/>

    <!-- RTL Css -->
    <link rel="stylesheet" href="../../../css/hopeUI/rtl.min.css"/>


</head>
<body class=" " data-bs-spy="scroll" data-bs-target="#elements-section" data-bs-offset="0" tabindex="0">
<!-- loader Start -->
<div id="loading">
    <div class="loader simple-loader">
        <div class="loader-body"></div>
    </div>
</div>
<!-- loader END -->

<div class="wrapper">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>

    <div class="gradient">
        <div class="container">
            <%--            <img src="../../../img/hopeUI/error/404.png" class="img-fluid mb-4 w-50" alt="">--%>
            <h2 class="mb-0 mt-4 text-white">Oops! You don't have access to this page.</h2>
            <a class="btn bg-white text-primary d-inline-flex align-items-center" href="../index.html">Back to Home</a>
        </div>
        <div class="box">
            <div class="c xl-circle">
                <div class="c lg-circle">
                    <div class="c md-circle">
                        <div class="c sm-circle">
                            <div class="c xs-circle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Library Bundle Script -->
<script src="../../../js/core/libs.min.js"></script>

<!-- External Library Bundle Script -->
<script src="../../../js/core/external.min.js"></script>

<!-- Widgetchart Script -->
<script src="../../../js/charts/widgetcharts.js"></script>

<!-- mapchart Script -->
<script src="../../../js/charts/vectore-chart.js"></script>
<script src="../../../js/charts/dashboard.js"></script>

<!-- fslightbox Script -->
<script src="../../../js/plugins/fslightbox.js"></script>

<!-- Settings Script -->
<script src="../../../js/plugins/setting.js"></script>

<!-- Slider-tab Script -->
<script src="../../../js/plugins/slider-tabs.js"></script>

<!-- Form Wizard Script -->
<script src="../../../js/plugins/form-wizard.js"></script>

<!-- AOS Animation Plugin-->

<!-- App Script -->
<script src="../../../js/hope-ui.js" defer></script>

</body>
</html>