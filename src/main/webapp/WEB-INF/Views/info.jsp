<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>My Course</title>
    <link rel="icon" href="../img/logo.png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
            integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
    />
    <link href="/css/myCourse.css" rel="stylesheet">
    <link href="/css/profile.css" rel="stylesheet">
</head>
<script>
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    };
</script>
<body>
<div class="container-fluid">
    <nav id="fixNav">
        <ul>
            <li><a class="icon" href="#"></a></li>
            <li><h2 style="margin-top: 32px"><strong style="color: black">Online Learning</strong></h2></li>
            <li class="dropdown nav">
                <button class="dropbtn"><i style="font-size: 30px" class="glyphicon glyphicon-bell"></i></button>
                <div class="dropdown-content one">
                    <p>Notification 1</p>
                    <p>Notification 2</p>
                    <p>Notification 3</p>
                </div>
            </li>
            <li class="a">
                <form action="" id="search-box">
                    <button id="search-button"><i class="glyphicon glyphicon-search" style="color: gray"></i></button>
                    <input type="text" id="search-text" placeholder="Search" required="">
                </form>
            </li>
        </ul>
    </nav>
    <div class="row">
        <div class="left">
            <div class="left_menu">
                <div class="user_button">
                    <a href="#" class="next round" style="justify-content: space-between">
                        <div class="item" style="background-image: url(${user.avatar});"></div>
                        <b style="font-size: 10px; color: white; margin-left: 5px; margin-top: 15px">
                            <strong style="font-size: 17px">${user.fullName}</strong>
                            <br/>${user.email}
                        </b>
                        <div style="margin-top: 3px; margin-right: 10px">
                            <i class="glyphicon glyphicon-menu-right" style="color: white; font-size: 20px"></i>
                        </div>
                    </a>
                </div>
                <div class="home_button">
                    <a href="#" class="home round">
                        <i class="glyphicon glyphicon-home"
                           style="color: white; margin-top: 8px; margin-left: 10px"></i>
                        <div style="margin-top: 6px; margin-left: 15px">
                            <b style="color: white; font-size: 15px">Home</b>
                        </div>
                    </a>
                </div>
                <div class="button_left">
                    <a href="/mycourse" class="button round">
                        <i class="glyphicon glyphicon-bookmark" style="margin-top: 10px; margin-left: 10px"></i>
                        <div style="margin-top: 6px; margin-left: 15px">
                            <b style="font-size: 15px">Course</b>
                        </div>
                    </a>
                </div>
                <div class="button_left">
                    <a href="#" class="button round">
                        <i class="glyphicon glyphicon-user" style="margin-top: 10px; margin-left: 10px"></i>
                        <div style="margin-top: 6px; margin-left: 15px">
                            <b style="font-size: 15px">Profile</b>
                        </div>
                    </a>
                </div>
                <div class="button_left">
                    <a href="#" class="button round">
                        <i class="glyphicon glyphicon-cog" style="margin-top: 10px; margin-left: 10px"></i>
                        <div style="margin-top: 6px; margin-left: 15px">
                            <b style="font-size: 15px">Setting</b>
                        </div>
                    </a>
                </div>
                <div class="button_left">
                    <a href="#" class="button round">
                        <i class="glyphicon glyphicon-info-sign" style="margin-top: 10px; margin-left: 10px"></i>
                        <div style="margin-top: 6px; margin-left: 15px">
                            <b style="font-size: 15px">More</b>
                        </div>
                    </a>
                </div>
                <div class="button_left" style="margin-bottom: 10px">
                    <a href="#" class="button round">
                        <i class="glyphicon glyphicon-log-out" style="margin-top: 10px; margin-left: 10px"></i>
                        <div style="margin-top: 6px; margin-left: 15px;">
                            <b style="font-size: 15px">Logout</b>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="w3-container">
                <form action="edit/info-update" method="post">
                    <div class="card">
                        <div class="info">
                            <span>Profile</span>
                            <button type="button" id="savebutton">edit</button>
                        </div>
                        <div class="forms">
                            <div class="inputs">
                                <span>Full Name</span>
                                <input type="text" readonly name="fullName" value="${user.fullName}">
                            </div>
                            <div class="inputs">
                                <span>Phone Number</span>
                                <input type="text" readonly name="mobile" value="${user.mobile}">
                            </div>
                            <div class="inputs">
                                <span>Gender</span>
                                <input type="text" readonly name="gender" value="${user.gender}">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <script>
                var savebutton = document.getElementById('savebutton');
                var readonly = true;
                var inputs = document.querySelectorAll('input[type="text"]');
                savebutton.addEventListener('click', function () {
                    if (savebutton.innerHTML == "edit") {
                        for (var i = 0; i < inputs.length; i++) {
                            inputs[i].toggleAttribute('readonly');
                        }
                        savebutton.innerHTML = "save";
                    } else {
                        savebutton.setAttribute("type", "submit");
                        savebutton.click();
                    }
                });
            </script>
        </div>
        <div class="right"></div>
    </div>
</div>
</body>
</html>
