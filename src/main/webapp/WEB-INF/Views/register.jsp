<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>OLS | FPT Online Learning System</title>

    <link rel="shortcut icon" href="/img/hopeUI/favicon.ico"/>

    <link rel="stylesheet" href="/css/hopeUI/core/libs.min.css"/>


    <link rel="stylesheet" href="/css/hopeUI/hope-ui.min.css?v=2.0.0"/>

    <link rel="stylesheet" href="/css/hopeUI/custom.min.css?v=2.0.0"/>

    <link rel="stylesheet" href="/css/hopeUI/dark.min.css"/>

    <link rel="stylesheet" href="/css/hopeUI/customizer.min.css"/>

    <link rel="stylesheet" href="/css/hopeUI/rtl.min.css"/>

    <link rel="stylesheet" href="/css/mainPage.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class=" " data-bs-spy="scroll" data-bs-target="#elements-section" data-bs-offset="0" tabindex="0">
<!-- loader Start -->
<div id="loading">
    <div class="loader simple-loader">
        <div class="loader-body"></div>
    </div>
</div>
<!-- loader END -->

<div class="wrapper">
    <section class="login-content">
        <div class="row m-0 align-items-center bg-white vh-100">
            <div class="col-md-6 d-md-block d-none bg-primary p-0 mt-n1 vh-100 overflow-hidden">
                <img src="../../../img/hopeUI/auth/02.png" class="img-fluid gradient-main animated-scaleX" alt="images">
            </div>
            <div class="col-md-6 p-0">
                <div class="card card-transparent auth-card shadow-none d-flex justify-content-center mb-0">
                    <div class="card-body">
                        <a href="/index" class="navbar-brand d-flex align-items-center mb-3">
                            <!--Logo start-->
                            <!--logo End-->

                            <!--Logo start-->
                            <div class="logo-main">
                                <div class="logo-normal">
                                    <svg class="text-primary icon-30" viewBox="0 0 30 30" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <rect x="-0.757324" y="19.2427" width="28" height="4" rx="2"
                                              transform="rotate(-45 -0.757324 19.2427)" fill="currentColor"/>
                                        <rect x="7.72803" y="27.728" width="28" height="4" rx="2"
                                              transform="rotate(-45 7.72803 27.728)" fill="currentColor"/>
                                        <rect x="10.5366" y="16.3945" width="16" height="4" rx="2"
                                              transform="rotate(45 10.5366 16.3945)" fill="currentColor"/>
                                        <rect x="10.5562" y="-0.556152" width="28" height="4" rx="2"
                                              transform="rotate(45 10.5562 -0.556152)" fill="currentColor"/>
                                    </svg>
                                </div>
                                <div class="logo-mini">
                                    <svg class="text-primary icon-30" viewBox="0 0 30 30" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <rect x="-0.757324" y="19.2427" width="28" height="4" rx="2"
                                              transform="rotate(-45 -0.757324 19.2427)" fill="currentColor"/>
                                        <rect x="7.72803" y="27.728" width="28" height="4" rx="2"
                                              transform="rotate(-45 7.72803 27.728)" fill="currentColor"/>
                                        <rect x="10.5366" y="16.3945" width="16" height="4" rx="2"
                                              transform="rotate(45 10.5366 16.3945)" fill="currentColor"/>
                                        <rect x="10.5562" y="-0.556152" width="28" height="4" rx="2"
                                              transform="rotate(45 10.5562 -0.556152)" fill="currentColor"/>
                                    </svg>
                                </div>
                            </div>
                            <!--logo End-->


                            <h4 class="logo-title ms-3">OLS</h4>
                        </a>
                        <h2 class="mb-2">Register</h2>
                        <div>
                            <c:if test="${message != null}">
                                <p class="text-danger">${message}</p>
                            </c:if>
                        </div>
                        <p>Please enter the required information.</p>
                        <form class="login100-form validate-form" action="/register" method="post">
                            <div class="col-lg-6">
                                <div class="form-group wrap-input100 validate-input"
                                     data-validate="Valid email is required: ex@abc.xyz">
                                    <label style="color: black; font-weight: bolder" for="email" class="form-label">Email</label>
                                    <input type="email" name="email" class="form-control" id="email"
                                           placeholder="Enter Email" required autofocus>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group wrap-input100 validate-input"
                                     data-validate="Password is required">
                                    <label style="color: black; font-weight: bolder" for="password" class="form-label">Password</label>
                                    <input type="password" name="password" class="form-control" id="password"
                                           placeholder="Enter Password" required autofocus>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="color: black; font-weight: bolder" for="confirm-password" class="form-label">Confirm
                                        Password</label>
                                    <input type="password" name="confirmPassword" class="form-control"
                                           id="confirm-password"
                                           placeholder="Confirm Password" required oninput="checkPasswordMatch(this);">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group wrap-input100 validate-input">
                                    <label style="color: black; font-weight: bolder" class="form-label" for="fullName">Full Name</label>
                                    <input type="text" name="fullName" class="form-control" id="fullName"
                                           placeholder="Enter Name" required autofocus>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group wrap-input100 validate-input">
                                    <label style="color: black; font-weight: bolder" class="form-label" for="gender">Gender</label>
                                    <select type="gender" name="gender" id="gender"
                                            class="form-select mb-3 shadow-none">
                                        <option value="" disabled selected>Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group wrap-input100 validate-input">
                                    <label style="color: black; font-weight: bolder" class="form-label" for="mobile">Mobile</label>
                                    <input type="tel" name="mobile" class="form-control" id="mobile"
                                           placeholder="Enter Phone Number">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary w-25" style="margin-top: 20px;">Register
                            </button>
                        </form>

                        <div class="card-body">
                            <div class="p-t-136 text-start"
                                 style="margin-top: 20px; display: inline-block; margin-right: 100px;">
                                <a class="txt2" href="/forgot">
                                    <i class="fa fa-long-arrow-left m-l-5" aria-hidden="true"></i>
                                    Forgot Password?
                                </a>
                            </div>
                            <div class="p-t-136 text-end"
                                 style="margin-top: 20px; display: inline-block; margin-left: 100px;">
                                <a class="txt2" href="/login">
                                    Already Have An Account?
                                    <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sign-bg sign-bg-right">
                    <svg width="280" height="230" viewBox="0 0 431 398" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g opacity="0.05">
                            <rect x="-157.085" y="193.773" width="543" height="77.5714" rx="38.7857"
                                  transform="rotate(-45 -157.085 193.773)" fill="#3B8AFF"/>
                            <rect x="7.46875" y="358.327" width="543" height="77.5714" rx="38.7857"
                                  transform="rotate(-45 7.46875 358.327)" fill="#3B8AFF"/>
                            <rect x="61.9355" y="138.545" width="310.286" height="77.5714" rx="38.7857"
                                  transform="rotate(45 61.9355 138.545)" fill="#3B8AFF"/>
                            <rect x="62.3154" y="-190.173" width="543" height="77.5714" rx="38.7857"
                                  transform="rotate(45 62.3154 -190.173)" fill="#3B8AFF"/>
                        </g>
                    </svg>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Library Bundle Script -->
<script src="../../../js/hopeUIjs/core/libs.min.js"></script>

<!-- External Library Bundle Script -->
<script src="../../../js/hopeUIjs/core/external.min.js"></script>

<!-- Widgetchart Script -->
<script src="../../../js/hopeUIjs/charts/widgetcharts.js"></script>

<!-- mapchart Script -->
<script src="../../../js/hopeUIjs/charts/vectore-chart.js"></script>
<script src="../../../js/hopeUIjs/charts/dashboard.js"></script>

<!-- fslightbox Script -->
<script src="../../../js/hopeUIjs/plugins/fslightbox.js"></script>

<!-- Settings Script -->
<script src="../../../js/hopeUIjs/plugins/setting.js"></script>

<!-- Slider-tab Script -->
<script src="../../../js/hopeUIjs/plugins/slider-tabs.js"></script>

<!-- Form Wizard Script -->
<script src="../../../js/hopeUIjs/plugins/form-wizard.js"></script>

<!-- AOS Animation Plugin-->

<!-- App Script -->
<script src="../../../js/hopeUIjs/hope-ui.js" defer></script>
<script>
    function checkPasswordMatch(fieldConfirmPassword) {
        if (fieldConfirmPassword.value !== $("#password").val()) {
            fieldConfirmPassword.setCustomValidity("Passwords do not match!");
        } else {
            fieldConfirmPassword.setCustomValidity("");
        }
    }
</script>
</body>
</html>